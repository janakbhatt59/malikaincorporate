@extends('frontLayout.main')
@section('content')

<style>
.content-title{
        font-style:normal;
        font-weight:400;
        font-size:24px;
        color:#3379b0;
        text-transform:none;
        line-height:Error:auto;
        margin:0;
        text-align:left;
        font-weight:600;}
.content-sub-title{
        font-style:normal;
        font-weight:400;
        font-size:20px;
        color:#3379b0;
        text-transform:none;
        line-height:30px;
        margin:0 0 20px 0;
}
</style>
<section class="hero-banner hero-banner--sm">
    <div class="hero-banner__content text-center">
      <h1>Product Inquiry Form</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('homePage')}}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="#">Product-Form</a></li>
        </ol>
      </nav>
    </div>
  </section>

        <div class="container mt-5">
         <div class="row">
                <div class="col-md-12 offset-md-12 col-sm-12 col-xs-12">
                    <h2 class="content-title">Our sales, technical service and customer service teams are prepared to serve customers 24x7.</h2>
                    <h2 class="content-sub-title">Contact our Malika team for more information about our products or to obtain a price quote.</h2>
                </div>
            </div>
{{Form::open(['route'=>'contactUsPost','novalidate'=>'novalidate','id'=>'contactForm','class'=>'form-contact contact_form','method'=>'post','enctype'=>'multipart/form-data','files' => true])}}
          <!-- <form action="#/" class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate"> -->
            <div class="row">
              <div class="col-lg-5">
                <div class="form-group">
                  <input class="form-control" name="name" id="name" type="text" placeholder="Full Name*">
                </div>
                <div class="form-group">
                  <input class="form-control" name="email" id="email" type="text" placeholder="Company*">
                </div>
                <div class="form-group">
                  <input class="form-control" name="email" id="email" type="email" placeholder="Enter Email*">
                </div>
                <div class="form-group">
                  <input class="form-control" name="subject" id="subject" type="number" placeholder="Enter Phone No*">
                </div>
              </div>
              <div class="col-lg-7">
                <div class="form-group">
                    <textarea class="form-control different-control w-100" name="message" id="message" cols="30" rows="7.6"
                    placeholder="Requirement(Minimum 30 Character)
Kindly mention the product name and the quantity here."></textarea>
                </div>
              </div>
            </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

<script type="text/javascript">
$(function () {$('#ec138ad9a1dd4c49b0d3b9bd997e1f90').show();});
function ______e091ac2c10bf4162b285cf1a33aa7596________() { $('#ec138ad9a1dd4c49b0d3b9bd997e1f90').hide(); $.post("/DefaultCaptcha/Refresh", { t: $('#CaptchaDeText').val() }, function(){$('#ec138ad9a1dd4c49b0d3b9bd997e1f90').show();}); return false; }</script>
<br>
<!-- <img id="CaptchaImage" src="/DefaultCaptcha/Generate?t=315479d0fba547a1b80991ffd7e41e81"><input name="CaptchaDeText" id="CaptchaDeText" type="hidden" value="315479d0fba547a1b80991ffd7e41e81"> <br><a id="ec138ad9a1dd4c49b0d3b9bd997e1f90" onclick="______e091ac2c10bf4162b285cf1a33aa7596________()" href="#CaptchaImage">Refresh</a><br><label>Please enter Captcha<span>*</span></label><br><input name="CaptchaInputText" id="CaptchaInputText" type="text" value="" data-val-required="The Captcha is required" data-val="true" autocorrect="off" autocomplete="off"><br><span class="field-validation-valid" data-valmsg-replace="true" data-valmsg-for="CaptchaInputText"></span>
                                    <p style="color: red;">   </p>
                                    <p id="CaptchReq" style="color: red;" hidden="">Captcha is required.</p>
                                </div> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>
                                        <input name="emailflag" id="emailflag" type="checkbox" checked="checked" value="true">
                                        <span class="checkmark"></span>
                                        Yes, I wish to receive email communications from Mantra.
                                    </label>
                                </div>
                            </div>
            <div class="form-group text-center text-md-right ml-5" style="color:white;">
              <a  class="button button-contactForm" >Send Message</a>
            </div>
    {{Form:: close()}}

          <!-- </form> -->
        </div>
      </div>
    </div>
@endsection
