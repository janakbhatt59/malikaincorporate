@extends('frontLayout.main')
@section('content')

 <link rel="stylesheet" href="{{asset('css/css/aos.css')}}">
@section('custom_css')
  <style>
      *{
      box-sizing: border-box;
     }
    .promote {
      transition: transform .2s;
     }

    .promote:hover {
      -ms-transform: scale(1.2); /* IE 9 */
      -webkit-transform: scale(1.2); /* Safari 3-8 */
      transform: scale(1.2);
    }
</style>
@endsection

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
    @foreach($homeSlider->take(1) as $slider)
    <div class="carousel-item active">
        <img class="d-block w-100" src="{{asset($slider->img_path)}}"  alt="First slide" width="100%" height="100%">
          <div class="carousel-caption d-none d-md-block text-left">
            <div class="row container" >
              <div class="col-md-12 col-lg-12">
                <h1 style="color:white;" data-aos="fade-left">{{ $slider->img_caption }}</h1>
                <p data-aos="fade-up" data-aos-delay="80">{{ $slider->img_caption }}</p>
                <a class="button mt-xl-3" data-aos="fade-left" data-aos-delay="100" href="#">{{ $slider->call_action }}</a>
            </div>
            </div>
          </div>
      </div>
      @endforeach
      @foreach($homeSlider as $key =>$slider)
        @if($key > 0)
        <div class="carousel-item ">
            <img class="d-block w-100" src="{{asset($slider->img_path)}}"  alt="First slide" width="100%" height="100%">
              <div class="carousel-caption d-none d-md-block text-left">
                <div class="row container" >
                  <div class="col-md-12 col-lg-12">
                    <h1 style="color:white;" data-aos="fade-left">{{ $slider->img_caption }}</h1>
                    <p data-aos="fade-up" data-aos-delay="80">{{ $slider->img_caption }}</p>
                    <a class="button mt-xl-3" data-aos="fade-left" data-aos-delay="100" href="#">{{ $slider->call_action }}</a>
                </div>
                </div>
              </div>
          </div>
      @endif
      @endforeach
        <!--/* <div class="carousel-item active">
        <img class="d-block w-100" src="{{asset('img/Slider/mantra-zebra.jpg')}}"  alt="First slide" width="100%" height="100%">
          <div class="carousel-caption d-none d-md-block text-left">
            <div class="row container" >
              <div class="col-md-12 col-lg-12">
                <h1 style="color:white;" data-aos="fade-left">Malika Incorporate <br>Pvt Ltd</h1>
                <p data-aos="fade-up" data-aos-delay="80">We are Happy to provide our Services.</p>
                         <a class="button mt-xl-3" data-aos="fade-left" data-aos-delay="100" href="#">Get in touch</a>
            </div>
            </div>
          </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{asset('img/Slider/Zebra-Family-of-Card-Printers.jpg')}}" alt="Second slide" width="100%" height="100%">
        <div class="carousel-caption d-none d-md-block text-left">
            <div class="row container">
              <div class="col-md-12 col-lg-12">
                   <h1 style="color:white;" data-aos="fade-left">Malika Incorporate <br>Pvt Ltd</h1>
                <p data-aos="fade-up" data-aos-delay="80">We are Happy to provide our Services.</p>
                         <a class="button mt-xl-3" data-aos="fade-left" data-aos-delay="100" href="#">Get in touch</a>
               </div>
            </div>
          </div>
      </div>
              <div class="carousel-item">
        <img class="d-block w-100" src="{{asset('img/Slider/wristband-Printer.png')}}" alt="Third slide" width="100%" height="100%">
          <div class="carousel-caption d-none d-md-block text-left">
            <div class="row container">
              <div class="col-md-12 col-lg-12 "  >
                  <h1 style="color:white;" data-aos="fade-left">Malika Incorporate <br>Pvt Ltd</h1>
                <p data-aos="fade-up" data-aos-delay="80">We are Happy to provide our Services.</p>
                         <a class="button mt-xl-3" data-aos="fade-left" data-aos-delay="100" href="#">Get in touch</a>
             </div>
            </div>
          </div>
      </div> */-->
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>



     <!--================ About section start =================-->
     <div class="bg-light container-fluid">
     <div class="col " id="aboutUs">
     <section class="d-lg-flex align-items-center section-margin--large">
       <div class="about__videoWrapper">
         <div class="about__video">
           <div class="about__video-image">
             <img class="card-imgVideo" src="{{ asset('img/home/video-bg.png') }}" alt="">
           </div>
           <div class="about__videoContent">
             <a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?v=vParh5wE-tM">
               <span></span>
             </a>
             <h4>Watch our video</h4>
           </div>
         </div>
       </div>
       <div class="container">
         <div class="about__content">
           <h3>About Us</h3>
           <h4>We Properly Understand Your Purpose</h4>
           <p>Malika incorporate Pvt.Ltd. is the leading distributor of solutions-based, RFID, Digital Signage,
   and ID & Security technology. Malika incorporate works exclusively with government smart city project,
    smart school project, healthcare sector , hospitality sector providing them with complete solutions.</p>
           <a class="button mt-xl-3" href="#">Learn More</a>
         </div>

       </div>
     </section>





     <!--================ Subject section start =================-->
     <section class="section-margin">
       <div class="container">
         <div class="row">
             <div class="col-sm-6 col-lg-4 col-xl-4 mb-4 mb-xl-0">
             <div class="card card-subject">
               <div class="card-subject__body">
                 <div class="card-subject__img">
                 <img class="card-img rounded-0" style=" position: relative; top: 5px; left: 96px;"  src="img/icon1.png" alt="icon1">
                 <br><br>
                 <div class="card-subject__imgOverlay">
                   <img src="img/home/hover-icon.png" alt="">
                 </div>
               </div>
                 <h3 class="text-center"><a href="#">Mission & Vision</a></h3><hr>
                 <p>Our vision and mission is to meet the requirements of the clients by delivering innovative products which ultimately
                        help us in becoming the leading Smart card supplier in the market. We always believe in offering quick and reliable customer
               service and strive to stick to that in any situation whatsoever. Working along with the changing technology, our aim is to deliver
               advanced products to our clients at reasonable prices.
               Our ultimate goal has always remained to attain maximum client satisfaction and we leave no stone unturned to achieve it.<br><br><br></p>
               </div>
             </div>
           </div>

           <div class="col-sm-6 col-lg-4 col-xl-4 mb-4 mb-xl-0">
             <div class="card card-subject">
               <div class="card-subject__body">
                 <div class="card-subject__img">
                 <img class="card-img rounded-0" style=" position: relative; top: 5px; left: 96px;" src="img/icon2.png" alt="icon2">
                 <br><br>
                 <div class="card-subject__imgOverlay">
                   <img src="img/home/hover-icon.png" alt="">
                 </div>
               </div>
                 <h3 class="text-center"><a href="#">Quality</a></h3><hr>
                 <p>Ever since we have forayed into this business, we have always worked hard to provide quality products and solutions to our customers. We have created our own niche as a quality manufacturer of smart cards, ID cards, RFID products, etc. Everything is planned right from the beginning and the processes are followed in a systematic manner to achieve 100 per cent customer satisfaction. Nothing is left to chance and everything is tested and ascertained to make sure that the end products stand true to the standards outlined in ISO certification.<br><br><br></p>
               </div>
             </div>
           </div>
               <div class="col-sm-6 col-lg-4 col-xl-4 mb-4 mb-xl-0">
             <div class="card card-subject">
               <div class="card-subject__body">
                 <div class="card-subject__img">
                 <img class="card-img rounded-0" style=" position: relative; top: 5px; left: 96px;" src="img/icon3.png" alt="icon3">
                 <br><br>
                 <div class="card-subject__imgOverlay">
                   <img src="img/home/hover-icon.png" alt="">
                 </div>
               </div>
                 <h3 class="text-center"><a href="#">Customer Satisfaction</a></h3><hr>
                 <p>The main focus of our company is to offer our clients with a quality proven range of Smart card printers ,Smart cards, PVC cards & consumables, access control equipment & ID Card Printers. We make no compromises on the quality front, which help us to attain maximum client satisfaction. For the convenience and specific demands of our customers, we also offer customized solutions based particular demands of clients. Some of the factors on which we emphasize in our bid to earn total customer satisfaction are: Pricing, Availability of products, Application specific design, Packaging, Delivery schedule.
   </p>
               </div>
             </div>
             </div>
           </div>
         </div>
         <div class="col-12 text-center mt-3 pt-xl-4">
           <a class="button" href="#">View All Practices <span class="align-middle"><i class="ti-arrow-right"></i></span></a>
         </div>
       </div>
     </section>
     <!--================ Subject section end =================-->

   <!-- -----------------------------carousel start------------------------------------------------- -->
<!--
  <div class="container-fluid bg-light">
   <div class="container-fluid">
   <br>
   <h2 class="text-center" id="solution">Solution</h2>
   <hr width="100px">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
     <ol class="carousel-indicators">
       <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
       <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
       <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
       <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
       <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
     </ol>
     .container-
     <div class="carousel-inner">
       <div class="carousel-item active">
         <img class=" site-section-cover overlay" src="img/solCon.jpg" alt="First slide"  height="650px">
       </div>
         <div class="carousel-item">

         <img class="d-block w-100" src="img/solution2.jpg" alt="Second slide" width="500" height="650px">
       </div>
        <div class="carousel-item ">

         <img class="d-block w-100" src="img/solution5.jpg" alt="Third slide" width="500" height="650px">
       </div>
       <div class="carousel-item ">

         <img class="d-block w-100" src="img/solution3.jpg" alt="fourth slide" width="500" height="650px">
       </div>
       <div class="carousel-item ">

         <img class="d-block w-100" src="img/solution4.jpg" alt="Third slide" width="500" height="400px">
       </div>
     </div>
     <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
       <span class="carousel-control-prev-icon" aria-hidden="true"></span>
       <span class="sr-only">Previous</span>
     </a>
     <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
       <span class="carousel-control-next-icon" aria-hidden="true"></span>
       <span class="sr-only">Next</span>
     </a>
   </div>
   </div>
   <br> -->



   <div class="three-cols container">
           <div class="row text-center">

           <div class="col-sm-6 col-lg-4 col-xl-4 mb-4 mb-xl-0 text-center">
                                       <div class="icon-holder">
                               <img src="https://malikaincorporate.com/wp-content/uploads/2018/04/icon5.png" height="44" width="28" alt="Services">
                           </div>
                                       <div class="text-holder">
                 <h2 class="title">Services</h2>
                 <p>Service solutions, certified support, and repair options for the equipment that is essential to keeping your business productive, profitable, on schedule and customers happy!</p>
               </div>
             </div>
           <div class="col-sm-6 col-lg-4 col-xl-4 mb-4 mb-xl-0">
                                       <div class="icon-holder">
                               <img src="https://malikaincorporate.com/wp-content/uploads/2018/04/icon4.png" height="44" width="28" alt="Products">
                           </div>
                                       <div class="text-holder">
                 <h2 class="title">Products</h2>
                 <p>Buy the asset tracking, inventory tracking, and data capture equipment needed for efficient supply chain operations. RFID and barcode scanners, printers, labels, and supplies.</p>
               </div>
             </div>
                                           <div class="col" style="height: 191px;">
                                       <div class="icon-holder">
                               <img src="https://malikaincorporate.com/wp-content/uploads/2018/04/icon9.png" height="44" width="28" alt="Government">
                           </div>
                                       <div class="text-holder">
                 <h2 class="title">Government</h2>
                 <p>Solutions and products for Government – from state and local to federal and military needs; Malika Incorporate has the expertise, knowledge, and skill to get the job done!</p>
               </div>
             </div>
        </div>
       </div>

   </div>
   </div>
   <br>
   <br><br>


     <!--================ Our Services =================-->

         <div class="container align-center">
         <div class="row">
            <div class="col-sm-4 col-lg-4 mb-4 mb-lg-0">
               <div class="categories_post">
                   <img class="card-img rounded-0 mx-auto" src="img/home/blog1.png" alt="Smart City">
                   <div class="categories_details">
                       <div class="categories_text">

                           <a href="single-blog.html">
                               <h5>Government-issued driver's Lisenses</h5>
                           </a>
                           <div class="border_line"></div>
                           <h6>SMART DRIVING LICENSE CARD </h6>
                           <P>Specimen of National Driving License Card of Nepal ...</p>
                           <a class="button mt-xl-3" href="#">Learn More</a>
                       </div>
                   </div>
               </div>
               <br>
               <h3><a href="#">Computer models to investors Short</a></h3>
                 <ul class="card-blog__info">
                   <li><a href="#"><i class="ti-notepad"></i>Feb 25 2018</a></li>
                   <li><a href="#"><i class="ti-themify-favicon"></i>1 Comment</a></li>
                 </ul>
                 <p>Varius conubia a mauris litora lacus. Cubilia proin on  ornare et nunc dapibus vel rutrum augue facilisis malesuada et porta impe.</p>
                 <br><br>
                 <hr>
                 <div class="card-blog__footer">
                 <a href="#">Read More<i class="ti-arrow-right"></i></a>
               </div>
           </div>
           <div class="col-sm-4 col-lg-4 mb-4 mb-lg-0">
               <div class="categories_post">
                   <img class="card-img rounded-0 mx-auto" src="img/home/blog2.png" alt="Smart City">
                   <div class="categories_details">
                       <div class="categories_text">

                           <a href="single-blog.html">
                               <h5>Government-issued driver's Lisenses</h5>
                           </a>
                           <div class="border_line"></div>
                           <h6>SMART DRIVING LICENSE CARD </h6>
                           <P>Specimen of National Driving License Card of Nepal ...</p>
                           <a class="button mt-xl-3" href="#">Learn More</a>
                       </div>
                   </div>
               </div>
               <br>
               <h3><a href="#">Computer models to investors Short</a></h3>
                    <ul class="card-blog__info">
                        <li><a href="#"><i class="ti-notepad"></i>Feb 25 2018</a></li>
                        <li><a href="#"><i class="ti-themify-favicon"></i>1 Comment</a></li>
                    </ul>
                <p>Varius conubia a mauris litora lacus. Cubilia proin on  ornare et nunc dapibus vel rutrum augue facilisis malesuada et porta impe.</p>
                      <br><br>
                      <hr>
               <div class="card-blog__footer">
                 <a href="#">Read More<i class="ti-arrow-right"></i></a>
             </div>
           </div>
           <div class="col-sm-4 col-lg-4 mb-4 mb-lg-0">
               <div class="categories_post">
               <img class="card-img rounded-0 mx-auto" src="img/home/blog3.png" alt="">
                   <div class="categories_details">
                       <div class="categories_text">

                           <a href="single-blog.html">
                               <h5>Government-issued driver's Lisenses</h5>
                           </a>
                           <div class="border_line"></div>
                            <h6>SMART DRIVING LICENSE CARD </h6>
                            <P>Specimen of National Driving License Card of Nepal ...</p>
                           <a class="button mt-xl-3" href="#">Learn More</a>
                       </div>
                   </div>
               </div>
               <br>
               <h3><a href="#">Computer models to investors Short</a></h3>
               <ul class="card-blog__info">
                 <li><a href="#"><i class="ti-notepad"></i>Feb 25 2018</a></li>
                 <li><a href="#"><i class="ti-themify-favicon"></i>1 Comment</a></li>
               </ul>
               <p>Varius conubia a mauris litora lacus. Cubilia proin on  ornare et nunc dapibus vel rutrum augue facilisis malesuada et porta impe.</p>
                  <br><br>
                   <hr>
               <div class="card-blog__footer">
                <a href="#">Read More<i class="ti-arrow-right"></i></a>
             </div>
           </div>
   </div>
   </div>
   <br>
   <br>



    <br>
    <br>
    {{-- Brands we promote --}}
    <div class="container align-center">
      <h2 style="text-align:center; color:#000000;">Brands We Promote</h2>
      <hr>
      <div class="row">
          <div class="col-sm-4 promote">
              <img style="width:300px; padding:20px;" src={{asset("brands/zebra.png")}}      alt="Zebra Logo" >
          </div>
          <div class="col-sm-4 promote">
              <img style="width:300px; padding:20px; " src={{asset("brands/paycraft.png")}}   alt="Mantra Logo">
          </div>
          <div class="col-sm-4 promote">
              <img style="width:300px; height:150px; padding:20px; " src={{asset("brands/ciso.png")}}           alt="Cisco Logo">
          </div>
          <div class="col-sm-4 promote">
              <img style="width:300px; padding:20px; " src={{asset("brands/mantra.png")}}      alt="Mantra Logo">
          </div>
          <div class="col-sm-4 promote">
              <img style="width:300px; padding:20px; " src={{asset("brands/shristhi.png")}}     alt="Mantra Logo">
          </div>
          <div class="col-sm-4 promote">
            <img style="width:300px; height:200px; padding:10px 0px 20px 0px;" src={{asset("brands/logo_suprema.png")}}   alt="Mantra Logo">
        </div>
          <div class="col-sm-4 promote">
            <img style="width:300px; height:200px; padding:5px 30px 30px 10px;" src={{asset("brands/logo_get.png")}}        alt="Mantra Logo">
        </div>
          <div class="col-sm-4 promote">
              <img style="width:300px; padding:10px 20px 20px 20px ; " src={{asset("brands/logo_marg.png")}}       alt="Marg Logo">
          </div>
        </div>
        </div>
        </div>
     </div>
  </section>

  {{-- Happy Customers --}}
    @include('page.advertise')
  {{-- Happy Customers --}}
  @endsection

