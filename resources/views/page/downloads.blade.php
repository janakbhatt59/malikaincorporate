@extends('frontLayout.main')
@section('content')
<section class="hero-banner hero-banner--sm">
 <div class="hero-banner__content text-center">
    <h1>Downloads</h1>
    <nav aria-label="breadcrumb" class="banner-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('homePage')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Downloads</li>
    </ol>
    </nav>
  </div>
 </section>


@endsection
