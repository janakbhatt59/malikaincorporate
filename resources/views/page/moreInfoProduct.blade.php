@extends('frontLayout.main')
@section('content')
<div style="background-color:#3379b0">
<section class="products-sub-head-section" style="padding: 100px 0; color:white">
 <div class="link_pages">
  <div class="container">
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xl-12 " style="position:absolute">
   <p>
   <a style="color:white" href="{{route('homePage')}}">Home &nbsp;</a>/ <a style="color:white"
            href="{{route('homePage')}}">Product &nbsp;</a>/<a href="javascript:void(0)" class="active" style="color:#ff9907">{{($productInfo->heading)}}</a>
    </p>
   </div></div></div></div><div class="container">
    <div class="row"> <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 fw-480">
     <img src="{{asset($productInfo->image)}}" alt="product img" title="product" width="100%" height="100%">
      </div><div class="col-lg-7 col-md-7 col-sm-5 col-xs-12 fw-480 ml-5" style="float:left">
       <div class="content_service">
        <h3 class="text-light" style="font-size:35;">{{($productInfo->heading)}}</h3>
        <hr>
         <p style="font-size:px">{{($productInfo->description)}}</p>
          <a href="{{route('getInTouch')}}" class="btn btn-outline-light" style="transition: all .3s ease-in-out; display: inline-block; padding: 5px 30px;   ">
          <i class="fa fa-hand-o-right"></i>Get In Touch</a>
          <br>
          <br>
            </div>
          </div>
        </div>
      </div>
    </section>


</div>
<div class="container">
  @if($productInfo->heading=='Bio Face MSD 1K')
     @include('Specifications.msd1')
  @endif
</div>

<div class="container">
    @if($productInfo->heading=='Bio Face MSD 150')
       @include('Specifications.msd150')
    @endif
  </div>

  <div class="container">
      @if($productInfo->heading=='M BIO G3')
         @include('Specifications.g3M')
      @endif
    </div>

   <div class="container">
     @if($productInfo->heading=='MBIO-M18')
        @include('Specifications.m18')
     @endif
   </div>

   <div class="container">
      @if($productInfo->heading=='M BIO S 18')
         @include('Specifications.bioS18')
      @endif
    </div>

    {{-- new --}}

    <div class="container">
      @if($productInfo->heading=='M BIO 5 N')
         @include('Specifications.bio5N')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='MFACE FA 300')
         @include('Specifications.FA300')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='MFACE FA 200')
         @include('Specifications.FA 200')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='M BIO ST1')
         @include('Specifications.bioST1')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='M BIO ST2')
         @include('Specifications.bioST2')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='SEM-01')
         @include('Specifications.sem01')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='BIOGPRS-01')
         @include('Specifications.Gprs1')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='Bioweb RC1')
         @include('Specifications.webRC1')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='BIOWEB B3')
         @include('Specifications.webB3')
      @endif
    </div>

    <div class="container">
      @if($productInfo->heading=='MFS100')
         @include('Specifications.MFS100')
      @endif
    </div>


    {{-- finish --}}
<br>

<div class="container">
    {{-- Specifications --}}
    @if($productInfo->heading==  'ZXP Series 3')
      @include('Specifications.zxp-series-3')
    @endif
    @if($productInfo->heading==  'ZXP Series 7')
      @include('Specifications.zxp-series-7')
    @endif
    @if($productInfo->heading==  'ZXP Series 9')
      @include('Specifications.zxp-series-9')
    @endif
    @if($productInfo->heading=='DS2208 2D' || $productInfo->heading==  'DS2278 Wireless' )
      @include('Specifications.ds2200')
    @endif
    @if($productInfo->heading=='LS2208 1D')
      @include('Specifications.ls2208')
    @endif
    @if($productInfo->heading=='ZT410')
      @include('Specifications.zt410')
    @endif
    @if($productInfo->heading== 'imZ320')
     @include('Specifications.iMZ320')
    @endif
    @if($productInfo->heading== 'EZ320')
     @include('Specifications.ez320')
    @endif
    @if($productInfo->heading== 'TC 20')
      @include('Specifications.tc20')
    @endif
    @if($productInfo->heading== 'TC 25')
      @include('Specifications.tc25')
    @endif
    @if($productInfo->heading== 'TC 56')
      @include('Specifications.tc56')
    @endif
    @if($productInfo->heading== 'GT 800')
      @include('Specifications.gt800')
    @endif
    @if($productInfo->heading== 'GC 420')
      @include('Specifications.gc420')
    @endif
    @if($productInfo->heading== 'HC 100')
      @include('Specifications.hc100')
    @endif
    @if($productInfo->heading== 'Bio Mini Finger')
      @include('Specifications.biomini')
    @endif
  </div>
<style>


.link_pages {
    position: absolute;
    top: 175px;
    left: 0;
    right: 0;
    z-index: 100;
    font-size: 16px;
    font-weight: 500;
    line-height: 26px;
    font-size: 14px;
    font-weight: 400;
    line-height: 26px;
}
.link_pages a{
        font-size: 15px;
        font-weight: 500;
        line-height: 26px;
}
</style>
@endsection
