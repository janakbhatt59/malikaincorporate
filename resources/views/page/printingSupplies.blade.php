@extends('frontLayout.main')
@section('content')
<section class="hero-banner hero-banner--sm">
    <div class="hero-banner__content text-center">
      <h1>Printing Supplies</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('homePage')}}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Printing Supplies</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================ Hero sm Banner end =================-->


 
  <!-- ================ Printing Supplies section start ================= -->
  <section class="section-margin--large">
    <div class="row" style="margin-left:170px;">
      @foreach($printing as $row)
        <div class="col-sm-12 col-lg-4 col-md-12">
         <br>
          <img src = "{{asset($row->photo)}}">
          <br> 
          <div class="col-sm-12 col-lg-8 col-md-12">
            @if($row->name == "Card Printer Ribbons")
              <a href = "{{route('viewPrintingSupplies')}}">{{$row->name}}</a>
            @endif
          <a href = "">{{$row->name}}</a>
          <br>
            {{$row->desc}}
          </div>
        </div>
 
        @endforeach
      </div>

  </section>
@endsection






