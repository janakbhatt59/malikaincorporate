@extends('frontLayout.main')
@section('content')
@foreach($productList as $row)

          <section class="products-all-parts mt-5 mb-5"> <div class="container">
   <div class="row">
 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <a href="" class="about-col-img clearfix products-list-img">
   <img src="{{asset($productList->image)}}" alt="product img" title="product" class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0s"
   style="visibility: visible; animation-duration: 1s; animation-delay: 0s; animation-name: fadeInLeft; position:absolute; right:90px;">
 </a>
</div>
<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
 <div class="about-col-two">
 <h3 class="sub_contain_part_title fingerprint-pro-title">{{$productList->heading}}m<span>Brix</span></h3>
  <p class="sub_contain_part_contain">{{$productList->description}}
 </p>
   <p class="list"><span class="li"></span>3.5 Inch Touch Screen TFT Color LCD with 320x240 Virtual Keypad on LCD</p>
   <p class="list"><span class="li"></span>Linux OS (RTOS)</p>
   <p class="list"><span class="li"></span>512 MB RAM , 256MB Flash</p>
   <a href=" {{route('moreinfo')}} " class="btn btn-outline-primary"><i class="fa fa-info"></i>&nbsp;More Info</a>
   <a href="" class="btn btn-outline-primary" download=""><i class="fa fa-download"></i>
   Download Brochure
   </a>
    </div></div></div></div></section>
    @endforeach
   <style>
.list .li{
      background-color: #ee1d23;
    height: 10px;
    width: 10px;
    display: inline-block;
    position: absolute;
    left: 0;
    top: 7px;
    bottom: 0;
    border-radius: 100%;
    margin: 0;

}
.products-all-parts .list, .products-all-parts .ticket-creation-about-col .list {
    padding-left: 30px;
    position: relative;
    margin-bottom: 5px;
</style>


@endsection
