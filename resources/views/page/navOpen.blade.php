@extends('frontLayout.main')
@section('content')
 <link rel="stylesheet" href="{{asset('css/css/aos.css')}}">

  <!--================ Hero sm Banner start =================-->
  <section style="background:url('/img/Slider/zebraMantra-sm.jpg') no-repeat;" class="hero-banner hero-banner--sm" >
    <div  class="hero-banner__content text-center">
      <h1 data-aos="fade-up">{{$navOpen->heading1}}</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb" data-aos="fade-up" data-aos-delay="100">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{$navOpen->heading1}}</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================ Hero sm Banner end =================-->


  @if ($navOpen->id==4)
  @include('page.aboutUs')
  @endif
  @if ($navOpen->id==2)
  @include('page.product')
  @endif

@endsection

