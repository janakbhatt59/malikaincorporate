@extends('frontLayout.main')
@section('content')

<a href="">
      <div class="fs-card-img-container">
        <img class="fs-card-img" alt="image" src="">
      </div>

      <div class="fs-card-text">
        <div class="fs-card-title">Mini Air Cooler Fan With Water And Ice Compartment</div>
        <div class="fs-card-price">
          <span class="currency">Rs.</span><span class="price">439</span>
        </div>
        <div class="fs-card-origin-price">
          <span class="fs-origin-price">
            <span class="currency">Rs.</span><span class="price">599</span>
          </span>+


          0
          <span class="fs-discount">
           -27%
          </span>
        </div>
        <div class="fs-card-sold">

        </div>
      </div>
    </a>

@endsection
