@extends('frontLayout.main')
@section('content')

<section class="hero-banner hero-banner--sm">
    <div class="hero-banner__content text-center">
      <h1>Our Partners</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('homePage')}}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Our Partners</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================ Hero sm Banner end =================-->



  <!-- ================ Section section start ================= -->
  <section class="section-margin--large">
    <div class="container col-lg-8 col-sm-12">
      <div class="d-none d-sm-block mb-5 pb-4">  
            <div class="container" style="margin-top:-100px">
                <div class="container row text-center">
                        <div class=" "><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/zebraLogo.jpeg')}}"      alt="Zebra Logo" ></div>
                        <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/logo_marg.png')}}"        alt="Marg Logo" ></div>
                        <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/mantraLogo.jpg')}}"      alt="Mantra Logo" ></div>
                        <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/logo_edigital.png')}}"      alt="digital Logo" ></div>
                        <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/cisco.png')}}"            alt="Cisco Logo" ></div>
                        <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/logo_get.png')}}"         alt="get Logo" ></div>
                        <div class="col-3"><img class=" mb-2" style=" width:200px; height:180px " src="{{URL::asset('/img/logo_srishti.png')}}"      alt="Srishti Logo" ></div>
                        <div class="col-3"><img class=" mr-2" style=" width:200px; height:180px " src="{{URL::asset('/img/logo_paycraft.png')}}"      alt="PayCraft Logo" ></div>
                </div>
            </div>
     </div>
    </div>
  </section>
@endsection





