@extends('backend.layout')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="row">
        <!-- @if(Session::has('message'))
            <div class="row errMsg" style="margin:1em;">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('message') !!}
                </div>
            </div>
        @endif -->
  <div class="col-sm-12 well">
        <div class="row">
          <p class="text-right">
             <button class="btn btn-primary btnDownload">Add new Product</button>
          </p>
        </div>
		<div role="content" style="display: none;" class="addDownloadForm">
			<div class="widget-body">
                <div class="row" style="border-bottom: 1em; background: #2196F3; padding: 1em;">
                <div class="col-sm-12" style="background:#FFF; ">
                    <fieldset>
                    <legend>Upload to Download</legend>
                    {{ Form::open(['route'=>'downloadStore', 'method' => 'post', 'class'=>'form-inline', 'files'=>true, 'name'=>'frmDownload']) }}
                     @include('page.download.form')
                        <div class="container">
                        <p class="text-right">
                      {{ Form::submit('Submit', ['class'=>'btn btn-primary']) }}
                        </p>
                        </div>
                      {{ Form::close() }}
                    </fieldset>
               	   </div>
                </div>
			</div>
		</div>
        <!-- update edit -->
		<div role="content" style="display: none;" class="updateDownloadForm">
			<div class="widget-body">
                <div class="row" style="border-bottom: 1em; background: #2196F3; padding: 1em;">
                <div class="col-sm-12" style="background:#FFF; ">
                    <fieldset>
                    <legend>Upload to Download</legend>
                    {{ Form::model(['route'=>'downloadUpdate', 'method' => 'post', 'class'=>'form-inline', 'files'=>true, 'name'=>'frmDownload']) }}
                    @include('page.download.form')
                        <div class="container">
                        <p class="text-right">
                      {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
                        </p>
                        </div>
                      {{ Form::close() }}
                    </fieldset>
               	   </div>
                </div>
			</div>
		</div>
        <p class="statusInfo" style="display: none;"></p>
        <div class="pannel-footer">
            <div role="content">
            <div class="widget-body">
                    <table id="dt_basic" class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="dt_basic_info" style="width: 100%;" width="100%">
                    <thead>
                        <tr role="row">
                        <th>sn</th>
                        <th>Name</th>
                        <th>Downloads Link</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php  $product_count = 1;   ?>
                        @if($downloads)
                        @foreach($downloads as $row)
                        <tr role="row" class="odd">
                        <td class="sorting_1"><?php echo  $product_count ++;   ?></td>
                        <td>{{$row->name}}</td>
                        <td><a href="{!! asset($row->download) !!}">{!! asset($row->download) !!}</a></td>
                        <td>
                            <span><a href="{!! route('downloadEdit', $row->id) !!}"  title="Edit" data-url="{!! route('downloadEdit', $row->id) !!}">
                                <i class="fa fa-fw fa-lg fa-pencil-square-o editBtn"></i></a>
                            </span>
                            <span><a href="{!! route('downloadDelete', $row->id ) !!}" class="txt-color-red deleteMe" title="delete Product" >
                          <i class="fa fa-fw fa-lg fa-trash-o deletable"> </i> </a>
                            </span>
                        @endforeach
                        {{ $downloads->links() }}
                        @endif
                    </tbody>
                </table>
        </div>
		</div>
	</div>
</div>
</div>
@endsection
@section('custom_script')
<script type="text/javascript">
	var btnDownload        = $('.btnDownload');
	var addDownloadForm    = $('.addDownloadForm');
	btnDownload.on('click', function(){
    addDownloadForm.toggle();
	});
	var editBtn        = $('.editBtn');
	var updateDownloadForm    = $('.updateDownloadForm');
	editBtn.on('click', function(){
    updateDownloadForm.toggle();
	});

    var btnEditSlider = $('.btnEditSlider');
        btnEditSlider.on('click', function(e){
            e.preventDefault();
            var url = $(this).data('url');
            alert(url);
            $.ajax({
                'type': 'GET',
                'url': url,
                success: function (response) {
                    console.log(response);
                    $('.homeSliderForm').css('display', 'block');
                    $('.sliderId').val(response.id);
                    $('.slider_img_caption').val(response.img_caption);
                    $('.img_caption').val(response.img_caption);
                    $('.call_action').val(response.call_action);

                }
            })
        });

</script>
@endsection
