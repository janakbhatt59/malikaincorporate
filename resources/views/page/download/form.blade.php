<div class="alert alert-success col-12">Add Files</div>
  <div class="container row">
  <div class="form-group col-6">
      {{ Form::label('name','File Name:') }}
      {{ Form::text('name', null, array('placeholder'=>'Enter File Name', 'class'=>'form-control', 'title'=>'file name')) }}
  </div>
      <div class="form-group col-6">
      {{ Form::file('download', null, array('placeholder'=>'Enter File Name', 'class'=>'form-control', 'title'=>'file name')) }}
  </div>
</div>
