@extends('frontLayout.main')
@section('content')
@foreach($productsList as $row)

          <section class="products-all-parts mt-5 mb-5"> <div class="container">
   <div class="row">
 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <a href="" class="about-col-img clearfix products-list-img">
   <img src="{{asset($row->image)}}" alt="product img" title="product"  class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0s"
   style="visibility: visible; animation-duration: 1s; animation-delay: 0s; animation-name: fadeInLeft; width:500px; height:200px; margin-left:-80px;">
 </a>
</div>
<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
 <div class="about-col-two">
 <h3 class="sub_contain_part_title fingerprint-pro-title" style="color:#ee1d23">{{$row['heading']}}</h3>
  <p class="sub_contain_part_contain">{{$row['description']}}</p>
   <p class="list"><span class="li"></span>{{$row['bullet1']}}</p>
   <p class="list"><span class="li"></span>{{$row['bullet2']}}</p>
   <p class="list"><span class="li"></span>{{$row['bullet3']}}</p>
   <a href="{{route('moreinfo',$row->id)}}" class="btn btn-outline-primary"><i class="fa fa-info"></i>&nbsp;More Info</a>
   <a href="" class="btn btn-outline-primary" download=""><i class="fa fa-download"></i>
   Download Brochure
   </a>
    </div></div></div></div></section>
    @endforeach
    <div class="row bg-light">
<div class="container">
<div class="row">
<div class="col-lg-7 col-sm-6 col-xl-7"><span class="content-title"
style="font-size:20px; color:#3379b0; line-height:1.1; font-weight:500; text-transform:uppercase;">
TALK TO <span style="color:#ee1d23">MALIKA'S</span> PROFESSIONAL FOR BULK ORDER.</span>
<br>Feel free to give us a call with any questions; we are waiting to hear from you. For Your bulk requirement just ask for quote</div>
<div class="col-lg-3 col-sm-5 col-xl-3 mt-2 ml-5"> <div class="form-group text-center text-md-right">
              <button type="submit" class="button button-contactForm">Schedule a Consultation</button>
            </div></div>
            </div>
</div></div>
   <style>
.list .li{
      background-color: #ee1d23;
    height: 10px;
    width: 10px;
    display: inline-block;
    position: absolute;
    left: 0;
    top: 7px;
    bottom: 0;
    border-radius: 100%;
    margin: 0;

}
.products-all-parts .list, .products-all-parts .ticket-creation-about-col .list {
    padding-left: 30px;
    position: relative;
    margin-bottom: 5px;
</style>


@endsection
