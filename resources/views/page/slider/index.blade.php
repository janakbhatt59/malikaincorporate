@extends('backend.layout')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="row">
	<div class="col-sm-12 well">
        <div class="panel">
        	  <div class="panel-headiing">
        	  	 <h5>Create Home Slider</h5>
            @if(Session::has('message'))
                <div class="row errMsg" style="margin:1em;">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! Session::get('message') !!}
                    </div>
                </div>
            @endif
               <p class="pull-right">
                <button class="btn btn-success btnAddhomeSlider">Add slider image</button>
               </p>
            </div>
        	  <div class="panel-body">
        	  	   <div class="row homeSliderForm" style="display: none;">
        	  	    <div class="col-sm-12">
        	  	    {!! Form::open(['route'=>'slidersave', 'class'=>'smart-form', 'files'=>true]) !!}
            <hr>
             <div class="widget-body no-padding">
			<fieldset>
				<div class="row">
					<section class="col col-4">
					    <label class="input">
					    {!! Form::text('img_caption', null, ['placeholder'=>'Slider name caption','name'=>'img_caption' ,
                        'class'=>'slider_img_caption', 'required']) !!}
					    </label>
					</section>

					<section class="col col-4">
					    <label class="input">
					    {!! Form::text('call_action', null, ['placeholder'=>'give to redirect action','name'=>'call_action' , 'class'=>'slider_call_action']) !!}
					    </label>
					</section>

                    <section class="col col-4">
                    	{!! Form::file('img_path',null, ['name'=>'img_path']) !!}

                    </section>
				</div>
			</fieldset>
	</div>

                           <footer>
               	        	<button type="submit" class="btn btn-primary">Save...</button>
               	        </footer>
                     {{ Form::hidden('sliderId', null, ['class' => 'sliderId']) }}
               	     {!! Form::close() !!}
                     </div>

        	  	   </div>
        	  </div>
            <p class="infoDiv"></p>
        	  <div class="panel-footer">
        	  	<table id="dt_basic" class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="dt_basic_info" style="width: 100%;" width="100%">
        	  		<caption>List of slider</caption>
        	  		<thead>
        	  			<tr>
        	  				<th>sn</th>
                            <th>Image</th>
                            <th>Caption</th>
        	  				<th>Call Action</th>
                            <th>Action</th>
        	  			</tr>
        	  		</thead>
        	  		<tbody>
                <?php  $count = 1;   ?>
                @if($homeSlider)
                    @foreach($homeSlider as $homeSlider)
                          <tr>
                            <td><?php echo $count ++;   ?></td>
                            <td><img src="{!! asset($homeSlider->img_path) !!}"  width="100" height="50"></td>
                            <td>{{ $homeSlider->img_caption }}</td>
                            <td>{{ $homeSlider->call_action }}</td>
                            <td>
                              <a href="" class="btnEditSlider btn btn-primary btn-xs" title="Edit" id="" data-url="{{ route('slideredit',$homeSlider->id)}}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                              <span>
                                <a href="{!! route('sliderdelete', $homeSlider->id ) !!}" class="txt-color-red deleteMe"
                                  data-url="{!! route('sliderdelete', $homeSlider->id ) !!}" title="delete Slider" data-name="{{ $homeSlider->img_caption }}" data-id = "{{ $homeSlider->id }}">
                                  <i class="fa fa-fw fa-lg fa-trash-o deletable"> </i></a>
                              </span>
                            </td>
                          </tr>
                    @endforeach
                    @else
                    <h4>Oops data is not found. Insert it first</h4>
                @endif
        	  		</tbody>
        	  	</table>
        	  </div>
        </div>
	</div>
<!-- modal -->
<!-- edit leavetype modal -->
<div class="modal" id="editSliderModel">
    <div class="modal-dialog editSlider">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit">&nbsp;Edit Slider </i></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                {{ Form::open(['name'=>'frmEditSlider', 'id' => 'frmUpdateSlider']) }}
                <!-- include('page.slider._form') -->
                <div class="widget-body no-padding">
			<fieldset>
				<div class="row">
					<section class="col col-4">
					    <label class="input">
					    {!! Form::text('img_caption', null, ['placeholder'=>'Slider name caption','name'=>'img_caption' ,'class'=>'slider_img_caption', 'required']) !!}
					    </label>
					</section>
					<section class="col col-4">
					    <label class="input">
					    {!! Form::text('call_action', null, ['placeholder'=>'give to redirect action','name'=>'call_action' , 'class'=>'slider_call_action']) !!}
					    </label>
					</section>
                    <section class="col col-4">
                    	{!! Form::file('img_path',null, ['name'=>'img_path']) !!}
                    </section>
				</div>
			</fieldset>
	</div>

                {{ Form::hidden('sliderId', null, ['class' => 'sliderId']) }}
                <div class="col-lg-12 p-t-20 text-right">
                {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
            {{ Form::close() }}
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">

        </div>
     </div>
    </div>
</div>

<!--  -->
</div>
@endsection
@section('custom_script')
<script type="text/javascript">

	var btnAddhomeSlider = $('.btnAddhomeSlider');
	var homeSliderForm   = $('.homeSliderForm');
	btnAddhomeSlider.on('click', function(){
    homeSliderForm.toggle();
	});

    var btnEditSlider = $('.btnEditSlider');
        btnEditSlider.on('click', function(e){
            e.preventDefault();
            var url = $(this).data('url');
            alert(url);
            $.ajax({
                'type': 'GET',
                'url': url,
                success: function (response) {
                    console.log(response);
                    $('.homeSliderForm').css('display', 'block');
                    $('.sliderId').val(response.id);
                    $('.slider_img_caption').val(response.img_caption);
                    $('.img_caption').val(response.img_caption);
                    $('.call_action').val(response.call_action);

                }
            })
        });

</script>
@endsection
