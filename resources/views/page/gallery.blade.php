@extends('frontLayout.main')
@section('content')
<style>
    .zoom {
        padding: 50px;
        transition: transform .2s;
        width: 600px;
        height: 500px;
        margin: 0 auto;
    }
        
    .zoom:hover {
        -ms-transform: scale(1.5); /* IE 9 */
        -webkit-transform: scale(1.5); /* Safari 3-8 */
        transform: scale(1.5); 
    } 
        
    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 300px) {
            .prev, .next,.text {font-size: 11px}
        }
</style>

<section class="hero-banner hero-banner--sm">
    <div class="hero-banner__content text-center">
      <h1>Gallery</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('homePage')}}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Gallery</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================ Hero sm Banner end =================-->



  <!-- ================ Gallery section start ================= -->
  <section class="section-margin--large">
    <div class="container col-lg-8 col-sm-12">
      <div class="d-none d-sm-block mb-5 pb-4">
          @foreach($gallery as $row)
              <img class = "zoom img-responsive" src="{{asset($row->ImageName)}}"  data-toggle= "modal" href=".modal-slider" style="width:400px; height:300px">              
          @endforeach 
              <div class="modal fade modal-slider" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">     
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($gallery->take(1) as $row)
                              <div class="carousel-item active">
                                <img class="d-block w-100" src="{{asset($row->ImageName)}}" alt="First slide">
                              </div>
                              @endforeach 
                              @foreach($gallery as $key =>$row)
                              @if($key > 0)
                              <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset($row->ImageName)}}" alt="Second slide">                              
                              </div>
                              @endif
                              @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                              <span class="carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                          </div>
                        
                    </div>
                  </div>
              </div>
             
                
        
     </div>
    </div>
  </section>
@endsection






