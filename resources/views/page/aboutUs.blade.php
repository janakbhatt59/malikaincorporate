 <section class="section-margin bg-light" style="margin-top:20px">
         <div class="section-intro text-center pb-65px">
           <h2 class="section-intro__title">{{$navOpen->heading1}}</h2><hr>
           <p>{{$navOpen->description1}}</p>
         </div>
</section>
<div class="container" style="margin-top:-100px">
         <h2 class="text-center">{{$navOpen->heading2}}</h2><hr>
<div class="container row text-center">

         <div class=" "><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/zebraLogo.jpeg')}}"      alt="Zebra Logo" ></div>
         <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/logo_marg.png')}}"        alt="Marg Logo" ></div>
         <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/mantraLogo.jpg')}}"      alt="Mantra Logo" ></div>
         <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/logo_edigital.png')}}"      alt="digital Logo" ></div>
         <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/cisco.png')}}"            alt="Cisco Logo" ></div>
         <div class="col-3"><img class=" mr-2 mb-2" style=" width:200px; height:150px " src="{{URL::asset('/img/logo_get.png')}}"         alt="get Logo" ></div>
         <div class="col-3"><img class=" mb-2" style=" width:200px; height:180px " src="{{URL::asset('/img/logo_srishti.png')}}"      alt="Srishti Logo" ></div>
         <div class="col-3"><img class=" mr-2" style=" width:200px; height:180px " src="{{URL::asset('/img/logo_paycraft.png')}}"      alt="PayCraft Logo" ></div>
</div>
</div>
<div class=" bg-light">
<div class="container">
         <h3 class="text-center col-2 bg-info" style="line-height:50px; color:white; margin-top:-12px">{{$navOpen->heading3}}</h3>
 <div class="row">
       

        <div class="col-md-6 col-lg-4 mb-4 mb-md-0 " >
         <h6 class=" font-weight-bold text-info">Head Office:</h4> <hr>

          <div class="media contact-info ">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <h3>Sahayoginager, Koteshor, Nepal</h3>
              <p>Koteshwor, Om Shanti Marg</p>

            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-headphone"></i></span>
            <div class="media-body">


              <h3><a href="tel:454545654">01-4154316</a></h3>
              <p>Sun to Fri 9:30am to 5:30pm</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <h3><a href="mailto:support@colorlib.com">info@malikaincorporate.com</a></h3>
              <p>Send us your query anytime!</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-4 mb-md-0">
         <h6 class=" font-weight-bold text-info">Head Office:</h4> <hr>

          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <h3>ओम् शान्ति मार्ग, Kathmandu 44600, Nepal</h3>
              <p>Koteshwor, Om Shanti Marg</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-headphone"></i></span>
            <div class="media-body">


              <h3><a href="tel:454545654">091-410398</a></h3>
              <p>Sun to Fri 9:30am to 5:30pm</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <h3><a href="mailto:support@colorlib.com">sandeep@malikaincorporate.com</a></h3>
              <p>Send us your query anytime!</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-4 mb-md-0">
         <h6 class=" font-weight-bold text-info" style="margin-top:-25px;">Partner Channel:<br>D.G Digital Solution</h4> <hr>

          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <h3>Tintolya, Biratnagar, Nepal</h3>
              <p>Koteshwor, Om Shanti Marg</p>

            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-headphone"></i></span>
            <div class="media-body">


              <h3><a href="tel:454545654">+977 9852020142</a></h3>
              <p>Sun to Fri 9:30am to 5:30pm</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <h3><a href="mailto:support@colorlib.com">dgds@malikaincorporate.com</a></h3>
              <p>Send us your query anytime!</p>
            </div>
          </div>
        </div>
</div>
</div>
