@extends('frontLayout.main')
@section('content')
  <!-- ================ Ribbons section start ================= -->

 <section class="section-margin--large">
        <div class="row" style="margin-left:170px;">
        <h2 style="color:#3379B0;">ZXP 3 Card Printer Ribbons</h2>
        </div>
        <div class="row" style="margin-left:170px;">
      
          @foreach($zxp3ribbons as $row)
            <div class="col-sm-12 col-lg-4 col-md-12">
             <br>
              <img src = "{{asset($row->image)}}">
              <br> 
              <div class="col-sm-12 col-lg-8 col-md-12">
                    <strong style = "color:#3379B0;">Brand:</strong> {{$row->brand}}
                    <br>
                    <strong style = "color:#3379B0;">Model:</strong> {{$row->model}}
                    <br>
                    <strong style = "color:#3379B0;">Print Capacity:</strong> {{$row->printCapacity}}
                    <br>
                    <strong style = "color:#3379B0;">Panel Type:</strong> {{$row->panelType}}<br>
              </div>
            </div>
            @endforeach
        </div>
        <br>
        <div class="row" style="margin-left:170px;">
            <h2 style="color:#3379B0;">ZXP 7 Card Printer Ribbons</h2>
        </div>
        <div class="row" style="margin-left:170px;">
                @foreach($zxp7ribbons as $row)
                  <div class="col-sm-12 col-lg-4 col-md-12 ">
                   <br>
                    <img src = "{{asset($row->image)}}">
                    <br> 
                    <div class="col-sm-12 col-lg-8 col-md-12">
                          <strong style = "color:#3379B0;">Brand:</strong> {{$row->brand}}
                          <br>
                          <strong style = "color:#3379B0;">Model:</strong> {{$row->model}}
                          <br>
                          <strong style = "color:#3379B0;">Print Capacity:</strong> {{$row->printCapacity}}
                          <br>
                          <strong style = "color:#3379B0;">Panel Type:</strong> {{$row->panelType}}<br>
                    </div>
                  </div>
                  @endforeach
              </div>
              <br>
              <div class="row" style="margin-left:170px;">
                  <h2 style="color:#3379B0;">ZXP 9 Card Printer Ribbons</h2>
              </div>
              <div class="row" style="margin-left:170px;">
                    @foreach($zxp9ribbons as $row)
                    <div class="col-sm-12 col-lg-4 col-md-12  ">
                        <br>
                        <img src = "{{asset($row->image)}}">
                        <br> 
                        <div class="col-sm-12 col-lg-8 col-md-12">
                            <strong style = "color:#3379B0;">Brand:</strong> {{$row->brand}}
                            <br>
                            <strong style = "color:#3379B0;">Model:</strong> {{$row->model}}
                            <br>
                            <strong style = "color:#3379B0;">Print Capacity:</strong> {{$row->printCapacity}}
                            <br>
                            <strong style = "color:#3379B0;">Panel Type:</strong> {{$row->panelType}}<br>
                        </div>
                    </div>
                    @endforeach
                </div>
      </section>
@endsection






