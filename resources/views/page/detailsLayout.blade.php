@extends('frontLayout.main')
@section('content')

<section class="hero-banner hero-banner--sm">
    <div class="hero-banner__content text-center">
      <h1>Company Details</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Company Details</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================ Hero sm Banner end =================-->



  <!-- ================ Gallery section start ================= -->
  <section class="section-margin--large">
    <div class="container col-lg-8 col-sm-12">
      <div class="d-none d-sm-block mb-5 pb-4">
          @foreach($details as $row)
          <div class="container"><h2>Company Details</h2>
          <div><h3>Name of the Company:{{$row->CompanyName}}</h3></div>
          <div><h3>Contact No:</h3>{{$row->phon}}</div>
          <div><h3>Email:</h3>{{$row->email}}</div>
          <div><h3>Logo:</h3>{{$row->logo}}</div>
            
          </div> 
          @endforeach 
              
             
                
        
     </div>
    </div>
  </section>
@endsection






