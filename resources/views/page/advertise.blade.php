<div class="container">
    <h2 style="text-align:center;">Our Happy Customer</h2>
    <hr>
</div>
    <div id="brands" class="carousel slide container" data-ride="carousel" style="margin-top:30px; padding-left:20px; padding-top:50px; height:300px; min-height:100px;">
       <div class="carousel-inner">
         <div class="carousel-item active">
           <div class="row">
               <div class="col">
               <img class="img-thumbnail" src={{asset("happycustomer/everest.jpg")}}   alt="First slide"  class="img-fluid">
               </div>
               <div class="col">
               <img class="img-thumbnail" src={{asset("happycustomer/national.jpg")}}   alt="First slide"  class="img-fluid">
               </div>
               <div class="col">
               <img class="img-thumbnail" src={{asset("happycustomer/license.png")}}   alt="First slide"  class="img-fluid">
               </div>
               <div class="col">
                <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/kathmandu.png")}}  alt="First slide"  class="img-fluid">
                </div>
                <div class="col">
                <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/tribhuvanLOGO.png")}}  alt="First slide"  class="img-fluid">
                </div>
                   
           </div>
         </div>
         <div class="carousel-item">
             <div class="row">
                    <div class="col">
                    <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/nmb.png")}}  alt="First slide"  class="img-fluid">
                    </div>
                    <div class="col">
                    <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/premier.jpg")}}  alt="First slide"  class="img-fluid">
                    </div>
                    <div class="col">
                    <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/sidHos.jpg")}}  alt="First slide"  class="img-fluid">
                    </div>
                    <div class="col">
                    <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/smartnagarpalika.png")}}  alt="First slide"  class="img-fluid">
                    </div>
                    <div class="col">
                    <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/vayodha.png")}}  alt="First slide"  class="img-fluid">
                    </div>
                   
             </div>
         </div>
  <div class="carousel-item">
    <div class="row">
        <div class="col">
        <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/kailali.png")}}  alt="First slide"  class="img-fluid">
        </div>
        <div class="col">
        <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/united.jpg")}}  alt="First slide"  class="img-fluid">
        </div>
        <div class="col">
        <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/water.jpg")}}  alt="First slide"  class="img-fluid">
        </div>
        <div class="col">
        <img class="img-thumbnail" src={{asset("happycustomer/NBB.png")}}   alt="First slide"  class="img-fluid">
        </div>
        <div class="col">
        <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/pasang.jpg")}}  alt="First slide"  class="img-fluid">
        </div>
    
    </div>
</div>
<div class="carousel-item">
        <div class="row">
               
               <div class="col">
               <img class="img-thumbnail" src={{asset("happycustomer/hospital.png")}}   alt="First slide"  class="img-fluid">
               </div>
               <div class="col">
               <img class="img-thumbnail" src={{asset("happycustomer/metlife.png")}}  alt="First slide"  class="img-fluid">
               </div>
               <div class="col">
               <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/prabhuBank.gif")}}  alt="First slide"  class="img-fluid">
               </div>
               <div class="col">
               <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/sunrise.png")}}  alt="First slide"  class="img-fluid">
               </div>
        </div>
    </div>
<div class="carousel-item">
        <div class="row">
            <div class="col">
            <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/greenlandschool.png")}}  alt="First slide"  class="img-fluid">
            </div>
            <div class="col">
            <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/images.png")}}  alt="First slide"  class="img-fluid">
            </div>
            <div class="col">
            <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/manmohan.png")}}  alt="First slide"  class="img-fluid">
            </div>
            <div class="col">
            <img  class="img-thumbnail rounded mx-auto d-block" src={{asset("happycustomer/samyak.png")}}  alt="First slide"  class="img-fluid">
            </div>
        </div>
    </div>
       </div>
       <a class="carousel-control-prev" href="#brands" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
       </a>
       <a class="carousel-control-next" href="#brands" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
       </a>
     </div>
