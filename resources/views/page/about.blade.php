@extends('frontLayout.main')
@section('content')

<section class="hero-banner hero-banner--sm">
    <div class="hero-banner__content text-center">
      <h1>About Us</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('homePage')}}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">About Us</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================ Hero sm Banner end =================-->



  <!-- ================ Section section start ================= -->
  <section class="section-margin--large">
    <div class="container col-lg-8 col-sm-12">
      <div class="d-none d-sm-block mb-5 pb-4">  
          @foreach ($about as $row )
            <div class="container">
              <strong style = "color:#1C88D0;">{{$row->heading}}</strong>
              <div class="clear"></div>
                {{$row->description}}
              </div>
              <br>     
          @endforeach
     </div>
    </div>
  </section>
@endsection







