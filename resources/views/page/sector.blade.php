@extends('frontLayout.main')
@section('content')

<section class="hero-banner hero-banner--sm">
    <div class="hero-banner__content text-center">
      <h1>Sectors We Serve</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('homePage')}}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Sector We Serve</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================ Hero sm Banner end =================-->



  <!-- ================ Section section start ================= -->
  <section class="section-margin--large">
    <div class="container col-lg-8 col-sm-12">
      <div class="d-none d-sm-block mb-5 pb-4">
          @foreach ($sector as $row )
            <div class="container">
              <ol>
             <strong style = "color:#1C88D0;"> {{$row->id}}.</strong>
              <strong style = "color:#1C88D0;">{{$row->heading}}</strong>
              <div class="container">
                {{$row->description}}
                <div class="clear"></div>
                @if($row->id<14)
                <Strong style = "color:#1C88D0;">Products: &nbsp;</Strong>{{ $row->product }}
                @endif
              </div>
              </ol>  
            </div>           
          @endforeach
     </div>
    </div>
  </section>
@endsection







