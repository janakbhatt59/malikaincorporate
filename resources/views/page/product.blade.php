<section class="section-margin bg-light" style="margin-top:20px">
         <div class="section-intro text-center pb-65px">
           <h2 class="section-intro__title">{{$navOpen->heading1}}</h2><hr>
           <h6 class="section-intro__title">{{$navOpen->description1}}</h6>
           <p>{{$navOpen->description1}}<br><br>Using decades of innovation, we design every product with you in mind. Your day-to-day tasks. Your work conditions. Your pressures. Discover your performance edge with scanners, mobile computers, tablets and printers with features and form factors made for you.</p>
         </div>
</section>

<div class="col-12">
<div class="columncontrol section mb-5" style="margin-top:-90px">
 <div class="column-control row">
  <div class="col-md-5">
    <div><div class="imagecomponent section">
     <div class="hero-bkg-img d-align-left m-align-center" data-d-height="300" data-m-height="250">
     &nbsp;&nbsp;&nbsp;    <a href="">&nbsp;&nbsp;&nbsp; <img class="ml-3"src="{{asset($navOpen->image1)}}" alt="worker using mobile computer" style="height: 300px; width: auto;"></a>
        <br>
    </div>
</div>
</div>
</div>
    <div class="col-md-7">
        <div class="text section mt-5">
        <h3>{{$navOpen->heading2}}<hr></h3>
       <p>{{$navOpen->description2}}<b>That’s why Malika is the indisputable global leader in enterprise mobile computing.</b><br>
     </p>
</div>
<div class="calltoaction section">
 <a class="btn btn-primary" style="border-radius:0px 0px 0px 0px" href="javascript:void(0);">Explore Mobile Computer And Tablets</a>
</div>
</div>
</div>
</div>
<div class="col-12">
<div class="columncontrol section mt-5 mb-5">
 <div class="column-control row">
    <div class="col-md-7 ">
        <div class="text section mt-5">
        <h3>{{$navOpen->heading3}}<hr></h3>
       <p>{{$navOpen->description3}}<b>That’s why Malika is the indisputable global leader in enterprise mobile computing.</b><br>
     </p>
</div>
<div class="calltoaction section">
 <a class="btn btn-primary" style="border-radius:0px 0px 0px 0px" href="javascript:void(0);">Explore Barcode Scanners And Images </a>
</div>
</div>
  <div class="col-md-5">
    <div><div class="imagecomponent section">
     <div class="hero-bkg-img d-align-left m-align-center" data-d-height="300" data-m-height="250">
     &nbsp;&nbsp;&nbsp;    <a href="">&nbsp;&nbsp;&nbsp; <img class="ml-3" src="{{asset($navOpen->image2)}}" alt="worker using mobile computer" style="height: 300px; width: auto;"></a>
        <br>
    </div>
</div>
</div>
</div>
</div></div>
</div>
<div class="col-12 mt-5">
<div class="columncontrol section mb-5" >
 <div class="column-control row">
  <div class="col-md-5">
    <div><div class="imagecomponent section">
     <div class="hero-bkg-img d-align-left m-align-center" data-d-height="300" data-m-height="250">
       <a href=""> <img class="ml-3"src="{{asset($navOpen->image3)}}" alt="worker using mobile computer" style="height: 300px; width: auto;"></a>
        <br>
    </div>
</div>
</div>
</div>
    <div class="col-md-7">
        <div class="text section">
        <h3>PRINTERS<hr></h3>
       <p>People. Products. Processes. No matter what you’re tracking or tagging, with barcodes or RFID, Malika has the most
        advanced solutions. This legacy of innovation continues with best-of-breed desktop, industrial, card and mobile printers.
         And they’re embedded with software for remote management, easy integration and
       peak performance. Why choose any printer when you can have a complete printing solution? <b>For that, the industry standard is Malika.</b><br>
     </p>
</div>
<div class="calltoaction section">
 <a class="btn btn-primary" style="border-radius:0px 0px 0px 0px" href="javascript:void(0);">Exlore Printers</a>
</div>
</div>
</div>
</div>

