  <style>
   @media (min-width: 576px)
{label {
    display: -ms-flexbox;
    display: block !important;
    -ms-flex-align: centera;
}}
  </style>
<div class="alert alert-success">Product Detail Form</div>
  <div class="container row">
  <hr>
  <div class=" col-sm-6">
  <div class="form-group col-sm-12 mt-2">
     <div class="col-5">
      {{Form::label('name','Product Name:')}}
     </div>
      {{ Form::text('name', null, array('placeholder'=>'Enter product name', 'class'=>'form-control', 'title'=>'product name')) }}
  </div>
  <div class="form-group col-sm-12 mt-2">
      <div class="col-5">
      {{Form::label('price','Price:')}}
      </div>
      {{ Form::text('price', null, array('placeholder'=>'Price', 'class'=>'form-control', 'title'=>'price')) }}
  </div>
  <div class="form-group col-sm-12 mt-2">
  <div class="col-5">
      {{Form::label('vido_link','Video Link:')}}
     </div>
      {{ Form::text('vedio_link', null, array('placeholder'=>'Vedio link', 'class'=>'form-control', 'title'=>'product vedio link')) }}
  </div>
  <div class="form-group col-sm-12 mt-2">
  <div class="col-5">
      {{Form::label('author_manufactural_name','Manufactural:')}}
     </div>
      {{ Form::text('author_manufactural_name', null, array('placeholder'=>'Enter manufactural name', 'class'=>'form-control', 'title'=>'manufactural_name')) }}
  </div>
  <div class="form-group col-sm-12 mt-2">
  <div class="col-5">
      {{Form::label('discount','Discount :')}}
     </div><br><br>
        <select name="discount" class="form-control" title="product discount">
            <option value="<?php echo isset($products->discount) ? $products->discount : '0';  ?>">
            <?php echo isset($products->discount) ? 'Discount '. $products->discount : 'Discount 0 %';?>
            </option>
            <option value="5">Discount 5%</option>
            <option value="10">Discount 10%</option>
            <option value="15">Discount 15%</option>
            <option value="20">Discount 20%</option>
        </select>
<br></div>
        <select class="form-control catVal" name="categories_id" title="category name">
            <option value="{{ isset($products->categories_id) ? $products->categories_id : '' }}">
              {{ isset($products->categories_id) ? $products->categories->name : 'select categories id' }}
            </option>
            @if(isset($catId))
            @foreach($catId as $cat)
               <option value="{{$cat->id}}">{{ $cat->name }}</option>
            @endforeach
            @endif
         </select>
    <div class="form-group col-sm-12 mt-2">
     <div class="col-5">
         {{Form::label('description','Description:')}}
     </div>
        {{ Form::textarea('description', null, array('placeholder'=>'Enter product description','rows'=>'5', 'cols'=>'30','class'=>'summernote form-control', 'title'=>'product description')) }}
    </div>
</div>

<div class="form-group col-sm-6">
<div class="col-12">
    @if(isset($products))
    <img src="{{ asset($products->featured_img) }}" width="100" height="100">
    @endif
</div>
<div class="col-12">
    <img src="" width="100" height="100" class="img img-thumbnail" id="productImg_1">
</div>   <label>Select Feature image</label>
    <input name="featured_img" type='file' onchange="displayImage(this, 'productImg_1');" title="select event featured image" />
    @if(isset($products))
    <img src="{{ asset($products->images) }}" width="150" height="150">
    @endif
    <input name="image[]" multiple="multiple" type='file' onchange="displayImage(this, 'productImg_3');" title="select images" />

    <select class="form-control" name="brands_id" title="brand name">
        <option value="<?php echo isset($products->brands->name) ? $products->brands_id : 'Select brand name';    ?>">
            <?php
                echo isset($products->brands->name) ? $products->brands->name : 'select brand name';
            ?>
        </option>
        @if(isset($brand))
            @foreach($brand as $brands)
                <option value="{{ $brands->id }}">{{ $brands->name }}</option>
            @endforeach

            @else
            <h4>Oops there is no band name</h4>
        @endif
    </select>
   </div>
  </div>
