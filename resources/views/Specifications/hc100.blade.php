<div class="container row">
        <h2 style = "color:#3379B0;">HC100 SPECIFICATIONS</h2>
        <div class="row">
        <div class="container col-sm-6 col-md-6 col-xl-6">
            <h3>STANDARD FEATURES</h3>
                <li>Quick & easy wristband cartridge loading</li>
                <li>Automatic print speed setting—up to 4 ips depending on installed cartridge</li>
                <li>Direct thermal printing of barcodes, text and graphics</li>
                <li>Dual-wall frame, impact-resistant plastic</li>
                <li>ZPL® or ZPL II® programming language</li>
                <li>32 bit RISC processor</li>
                <li>8 MB (2 MB user-available) Flash memory (standard)</li>
                <li>16 MB (4 MB user-available) SDRAM memory (standard)</li>
                <li>Element Energy Equalizer™ (E3™) for superior print quality</li>
                <li>Dual communications interface: serial and USB</li>
                <li>Odometer for print-length tracking</li>
                <li>Illuminated media low/media out indicator</li>
                <li>Tool-less printhead and platen replacement</li>
                <li>Unicode™-compliant for multi-language printing</li>
                <li>Instant media calibration—no wasted media</li>
            <br>
        </div>
        <div class="container col-sm-4 col-md-4 col-xl-4">
            <li>16 resident expandable bitmap fonts</li>
            <li>One resident scalable font</li>
            <li>Head-up sensor</li>
            <li>Standard tear-off mode feature</li>
            <li>XML-enabled printing—allows XML communications from information systems</li>
            <li>ENERGY STAR® qualified</li>
            <strong>Maximum Print Length</strong>
                <li>22"/558 mm</li>
            <strong>Minimum Print Length</strong>
                <li>3"/76 mm</li>
            <Strong>Print widths</Strong>
                <li>0.75"/19.05 mm, 1"/25.4 mm, 1.1875"/30.16 mm</li>
            <Strong>Resolution</Strong>
                <li>300 dpi/12 dots per mm</li>
            <Strong>Maximum Print Speed</Strong>
                <li>2"/51 mm per second (Healthcare)</li>
                <li>4"/102 mm per second (Z-Band Fun & Z-Band Splash)</li>
            <br>
        </div>
        <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/hc100.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
        </div>
</div>
