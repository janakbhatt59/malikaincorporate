<div class="container row">

    <h2 style = "color:#3379B0;">ZXP-SERIES-9 SPECIFICATIONS</h2>
    &nbsp; &nbsp;
    <a href = "{{asset('assests/img/SpecsSheet/zxp9.pdf')}}" style ="padding-top:10px; color:#3379B0;">
       <i class="fa-file-pdf-o" aria-hidden="true"> View Full Specifications</i>
    </a>
    <div class="row">
        <div class="container col-sm-6 col-md-6 col-xl-6">
        <h3>STANDARD FEATURES</h3>
            <li>USB and Ethernet connnectivity</li>
            <li>Single-card feed capability</li>
            <li>150 card capacity feeder(30 mil)</li>
            <li>15 card capacity reject hopper(30 mil)</li>
            <li>100 card capacity output hopper(30mil)</li>
            <li>i Series intelligent media technology</li>
            <li>About-calibration of media</li>
            <li>21-character, 6line LCD operator display</li>
            <li>304 dpi(12.0 dots/mm) print resolution</li>
            <li>64 MB memory standard</li>
            <li>Lifetime warranty on printhead</li>
            <li>Two-year limited warranty on printer</li>
            <li>Microsoft Windows certified drivers</li>
            <li>Kensington® physical lock capable</li>
            <br>
            <h3>CARD SPECIFICATIONS</h3>
            <li>Card Thickness: 30-40 mil</li>
            <li>Card Size: CR-80, ISO 7810 format, Type ID-1</li>
            <li>Card Material: PVC and PVC composite, ABS cards, PET, PET-G, and Teslin composite</li>
            <li>Specialty cards: transparent(IR-blocked) or translucent card 30 mil(clear and colored)</li>
            <li>Technology cards: contact and contactless smart cards, UHF cards</li>
            <br>
        </div>
    <div class="container col-sm-4 col-xl-4 col-md-4">
        <h3>LAMINATOR SPECIFICATIONS</h3>
        <li>Single-or dual-sided lamination option(with dual-sided printer only)</li>
        <li>180 cards per hour dual-sided printing and dual-sided lamination</li>
        <li>GSA FIPS 201 approved(dual-sided lamination)</li>
        <li>Uses Zebra True Secure i Series laminates only</li>
        <li>Laminate for the top and bottom sold separately</li>
        <li>Custom-coded laminates available</li>
        <li>Custom holographic laminates available</li>
        <br>
        <h3>ENCODING OPTIONS  & SPECIFICATIONS</h3>
        <li>iso 7816 Smart Card Contact Station for third-party external contact encoders</li>
        <li>Magnetic stripe encoder-AAMVA and ISO 7811(new and pre-encoded; tracks 1,2 and 3; high and low coercivity)</li>
        <li>Combined MIFARE ISO 14443 A & B (13.65 MHz) contactless, ISO 7816 Contact Encoder PC/SC compliance(supported over USB and Ethernet)</li>
        <li>Encoding over Ethernet</li>
        <br>
    </div>
    <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/zxp9.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
</div>
</div>
