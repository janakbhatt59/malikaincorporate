<div class="container">
        <h2 style = "color:#3379B0;">GC420 SPECIFICATIONS</h2><hr>
        &nbsp; &nbsp;
        <div class="row">
            <div class="col-xl-6 col-sm-6 col-md-6">
                <h3>STANDARD FEATURES</h3>
                    <li>Programming languages: EZPL (ZPL, ZPL II and EPL2)</li>
                    <li>32 bit RISC processor</li>
                    <li>Triple connectivity: USB, parallel and serial</li>
                    <li>Print methods: Direct thermal and thermal transfer, printing of barcodes, text and graphics.</li>
                    <li>OpenACCESS™ for easy media and ribbon loading</li>
                    <li>Microsoft® Windows® drivers</li>
                    <li>ENERGY STAR® qualified</li>

                     <Strong>Maximum media roll size</Strong>
                        <li>5”/127 mm O.D. on a 1.00”/25.4 mm, 1.5”/38 mm I.D. core</li>
                     <Strong>Media thickness</Strong>
                        <li>0.003”/0.075 mm to 0.007”/0.18 mm</li>
                     <Strong>Media types</Strong>
                        <li>Roll-fed or fan-fold</li>
                        <li>Label stock (die cut or continuous, direct thermal or thermal transfer)</li>
                        <li>Tag stock (die cut or continuous, direct thermal or thermal transfer)</li>
                        <li>Receipt paper (continous, direct thermal)</li>
                        <li>Wristband stock (direct thermal or thermal transfer)</li>
                <br>
            </div>
            <div class="col-xl-4 col-sm-4 col-md-4">
                <strong>Resolution</strong>
                    <li>203dpi/8 dots per mm</li>
                <strong>Memory</strong>
                    <li>8 MB Flash, 8 MB SDRAM (standard)</li>
                <Strong>Print width</Strong>
                    <li>4.09”/104 mm </li>
                <Strong>Print length</Strong>
                    <li>39”/990 mm</li>
                <Strong>Print speed</Strong>
                    <li>4”/102 mm per second maximum</li>
                <Strong>Media Sensors</Strong>
                    <li>Reflective and transmissive sensors</li>
                <Strong>Media width</Strong>
                    <li>1.00”/25.4 mm to 4.25”/108 mm</li>
                 <Strong>Media length</Strong>
                    <li>0.38”/9.6 mm to 39”/990 mm</li>
                <br>
            </div>
            <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/gc420.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
        </div>
    </div>


