
<div class="container">
    <br>
    <h2 style = "color:#3379B0; font-size:26px">STANDARD FEATURES</h2>
    <div class="container">
            <li>Color: Black & Silver</li>
            <li>Fingerprint capacity: 1000</li>
            <li>card capacity: 1000</li>
            <li>LCD Display: 2.4inch TFT Color LCD</li>
            <li>Transaction capacity: 100000 logs</li>
            <li>Authentication Method: Fingerprints, card, Password</li>
            <li>Identification Mode: Fingerprints, card, Pin</li>
            <li>Verification Mode: 1:N,1:1</li>
            <li>Communation: USB drive/TCP</li>
            <li>Auto Push Technology: Yes</li>
            <li>Real-time transmission: Yes</li>
            <li>Voice Prompt: High-definition Pronunciation</li>
            <li>Operation temperature: 0℃-60℃</li>
            <li>Battery Backup: Yes</li>
    <br> 
</div>
</div>