
<div class="container">
        <br>
        <h2 style = "color:#3379B0; font-size:26px">STANDARD FEATURES</h2>
        <div class="container">
        <li>TFT ColorLCD screen: 2.8" Color Screen</li>
        <li>Fingerprint capacity: 1,000</li>
        <li>Records capacity: 1,00,000 logs</li>
        <li>Identification Mode:  1:N & 1:1</li>
        <li>Identification Method: Fingerprint, Card, PIN</li>
        <li>FAR: 0.0001% </li>
        <li>FRR: 0.1 </li>
        <li>Communation method: USB Drive, TCP/IP</li>
        <li>Push Data Technology: Yes</li>
        <li>Door access control: Weigand in & out, Relay</li>
        <li>Current voltage: DC 12v 1A,</li>
        <li>Operation temprature: 0°C — 50°C</li>
        <br> 
   </div>
    </div>