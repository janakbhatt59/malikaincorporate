<div class="container row">
    <h2 style = "color:#3379B0;">TC 20 SPECIFICATIONS</h2>
    <div class="row">
        <div class="container col-sm-6 col-md-6 col-xl-6">
            <h3>Physical Characteristics</h3>
            <strong>Dimensions</strong>
            <li><strong>TC20 ALL-TOUCH</strong></li>
                <li>5.27 in. L x 2.82 in. W x .63 in. D</li>
                <li>134 mm L x 73.1 mm W x 16 mm D</li>
            <li><strong>TC20 WITH KEYBOARD</strong></li>
                <li>6.45 in. L x 2.82 in W x .63 in. D</li>
                <li>164 mm L x 73.1 mm W x 16 mm D</li>

            <strong>Weight</strong>
            <li><strong>TC20 ALL-TOUCH:</strong>6.88 oz./195 g</li>
            <li><strong>TC20 WITH KEYBOARD:</strong>7.58 oz./215 g</li>

            <strong>Display</strong>
            <li>4.3 in. color WVGA (800x480); LED backlight; Corning Gorilla Glass</li>

            <strong>Imager Window</strong>
            <li><strong>Corning Gorilla Glass</strong></li>

            <strong>Touch Panel</strong>
            <li>Capacitive Touch Panel; multi-touch</li>

            <strong>Power</strong>
            <li>Non-removable</li>
            <li>Rechargeable Li-Ion: standard capacity, 3000mAh</li>
            <li>Charge in under 4 hours (10hrs = 1 Shift) RTC backup</li>

            <strong>Network Connections</strong>
            <li>One USB OTG - host/client (Type C connector)</li>
            <br>
        </div>
        <div class="container col-sm-4 col-md-4 col-xl-4">
            <h3>Performance Characteristics</h3>
            <strong>CPU</strong>
                <li>QCOM MSM8937® 64-bit 8-Core, ARM® Cortex A53 1.4GHz, 512KB L2 cache, power-optimization</li>
            <strong>Operation System</strong>
                <li>Android Oreo 8.1</li>
            <strong>Memory</strong>
                <li>2GB RAM/16 GB Flash memory</li>
            <strong>Security</strong>
                <li>Verified Boot</li>
            <br>
            <h3>Warranty</h3>
            <p>
                Subject to the terms of Zebra’s hardware warranty statement, the TC20 is warranted against defects in workmanship and materials for a period of 1 (one) year from the date of shipment. For complete warranty statement, please visit: www.zebra.com/warranty
            </p>
            <br>
        </div>
         <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/tc20.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
    </div>
</div>
