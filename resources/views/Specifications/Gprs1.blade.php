
<div class="container">
    <br>
    <h2 style = "color:#3379B0; font-size:26px">STANDARD FEATURES</h2>
    <div class="container">
    <li>TFT Color LCD screen: 2.8"TFT Color Screen LCD</li>
    <li>FAR: 0.0001%</li>
    <li>FRR: 0.1</li>
    <li>Identification speed: 0.5 sec</li>
    <li>Record Capacity: 1,00,000 logs</li>
    <li>Finger Capacity: 3,000 user</li>
    <li>Fingerprint sensor resolution: 500dpi</li>
    <li>Identify mode: Card, Password 1:N & 1:1</li>
    <li>Maximum employee ID length: Numeric, 6  digit</li>
    <li>Keypad Type: Numeric Backlight keys</li>
    <li>Wiegand/RS485 Interface: Wiegand Input & Output 26/34 bit</li>
    <li>Waterproof: Conforms to IP68</li>
    <li>Operation temprature: Operations: 0°C ~ 50°C</li>
    <li>Wiring Connections: External Card Reader, Electric Lock, Exit Button, Alarm, Bell</li>
    <li>Power: 9v DC 1A, AC Adapter included</li>
    <li>Communication: GPRS/RS485/TCP/IP/USB Line/U Disk</li>
    <br> 
</div>
</div>
