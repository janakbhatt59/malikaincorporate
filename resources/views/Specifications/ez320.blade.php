<div class="container row">
        <h2 style = "color:#3379B0;">EZ320 SPECIFICATIONS</h2>
        <div class="row">
        <div class="container col-md-6 col-sm-6 col-xl-6">
            <h3>STANDARD FEATURES</h3>
                <li>Direct thermal</li>
                <li>USB 2.0</li>
                <li>Power Supply (AC Adaptor — charges battery while in printer)</li>
                <li>8 MB Flash / 16 MB SDRAM</li>
                <li>IP42 rating</li>
                <li>1.2 m / 4' drop test design</li>
                <li>USB 2.0</li>
                <li>Bluetooth®</li>
                <li>Regional specific feature: Simplified Chinese SimSun GB18030 fonts in 2 sizes (24 x 24, 16 x 16), Traditional Chinese (Big5) 24 x 24, Latin 1 & 9</li>
            <br>
            <h3>Warranty</h3>
            <p>
                    The EZ320 is warranted against defects in workmanship and materials for
                    a period of 1 (one) year from the date of shipment. For the complete warranty
                    statement, please visit: www.zebra.com/warranty.
            </p>
            <br>
        </div>
        <div class="container col-sm-4 col-md-4 col-xl-4">
            <strong>Resolution</strong>
                <li>203 dpi/8 dots per mm</li>
            <strong>Memory</strong>
                <li>8 MB Flash/16 MB SDRAM</li>
            <Strong>Print Width</Strong>
                <li>Fixed - 74mm — adjustable media option with Spacers - 60mm</li>
            <Strong>Maximum Print Speed</Strong>
                <li>50 mm/2" per second</li>
            <Strong>Media Sensors</Strong>
                <li>Out of paper; media door open</li>
            <Strong>Power</Strong>
                <li>7.4V Li-Ion, 1200 mAh battery; rechargeable</li>
            <Strong>Maximum Media Roll Size</Strong>
                <li>40 mm/1.57" O.D</li>
            <Strong>Media Width</Strong>
                <li>Fixed - 80mm (+/- 1mm), with Spacers - 60mm (+/- 1mm)</li>
            <Strong>Thickness</Strong>
                <li>2.5 mil/64 μm to 4.5 mil/114 μm</li>
            <br>
        </div>
        <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/ez320.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
        </div>
</div>
