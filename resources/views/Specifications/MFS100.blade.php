
<div class="container">
    <br>
    <h2 style = "color:#3379B0; font-size:26px">STANDARD FEATURES</h2>
    <div class="container">
    <li>Fingerprint Sensor Optical (scratch free sensor surface)</li>
    <li>Resolution 500 DPI / 256 gray</li>
    <li>Sensing Area 15 x 17 mm</li>
    <li>Operation Temperature 0~50°C</li>
    <li>Standards ANSI-378, ISO19794-2</li>
    <li>Supports Encryption algorithms AES256, RSA2048</li>
    <li>Supports HASHing algorithms MD5, SHA256</li>
    <li>Physical Unique Device ID</li>
    <li>Interface USB 2.0 High speed/Full speed, Plug & Play</li>
    <li>CE, FCC, RoHS, IEC60950 Certified</li> 
    <br> 
</div>
</div>

