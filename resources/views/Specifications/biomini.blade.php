<div class="container row">
        <h2 style = "color:#3379B0;">BIO MINI(SUPREMA) Specifications</h2>
        &nbsp; &nbsp;
        <a  class="badge badge-secondary" href = "{{asset('assests/img/SpecsSheet/biomini.pdf')}}" style ="padding-top:10px; color:#3379B0;">
           <i class="fa-file-pdf-o " aria-hidden="true"> View Full Specifications</i>
        </a>
        <div class="row">
        <div class="container col-sm-6 col-md-6 col-xl-6">
            <strong>Sensor Type</strong>
            <li>Optical</li>
            <br>
            <strong>Resolution / Gray Scale</strong>
            <li>500 ppi / 256 levels</li>
            <br>
            <strong>Platen Size</strong>
            <li>16.0 x 18.0 mm</li>

            <strong>Sensing Area</strong>
            <li>14.6 x 16.2 mm</li>

            <strong>Image Size</strong>
            <li>288 x 320 pixels</li>

            <strong>Template Format</strong>
            <li>Suprema, ISO 19794-2, ANSI 378</li>

            <strong>Image Format</strong>
            <li>RAW, BMP, WSQ, ISO 19794-4</li>

            <strong>USB</strong>
            <li>2.0 High Speed</li>

            <br>
        </div>
        <div class="container col-6">
                <strong>Operating Temperature</strong>
                <li>-10 ~ 50°C</li>

                <strong>Operation Humidity</strong>
                <li>10 ~ 90% non-condensing</li>

                <strong>Certification</strong>
                <li>CE, FCC, KC, WHQL</li>

                <strong>Dimensions(W x L x H)</strong>
                <li>66 x 90 x 58 mm</li>

                <strong>Weight</strong>
                <li>120g</li>

                <strong>Operating System</strong>
                <li>Windows XP, Vista, 7, 8, 8.1, 10 32/64bit</li>
                <li>Ubuntu, Debian, Fedora, OpenSUSE, CentOS 32/64bit</li>
                <li>Android 4.1(Jelly Bean) and Above</li>
            <br>
        </div>
        </div>
          <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/biomini.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
        </div>
