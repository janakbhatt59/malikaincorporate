<div class="container row">
    <h2 style = "color:#3379B0;">ZXP-SERIES-3 SPECIFICATIONS</h2>
    <div class="row">
    <div class="container col-sm-6 col-md-6 col-xl-6">
    <h3>STANDARD FEATURES</h3>
        <li>True Colours® ix Series™ ZXP 3 high-performance ribbons with intelligent media technology</li>
        <li>High capacity, eco-friendly Load-N-Go™ drop-in ribbon cartridges</li>
        <li>ZRaster™ host-based image processing</li>
        <li>Auto calibration of ribbon</li>
        <li>USB connectivity</li>
        <li>Microsoft® Certified Windows® drivers</li>
        <li>Print Touch NFC tag for online printer documentation and tools</li>
        <li>100 card covered feeder (30 mil)</li>
        <li>45 card capacity output hopper (30 mil)</li>
        <li>16-character LCD operator control display</li>
        <li>300 dpi (11.8 dots/mm) print resolution</li>
        <li>Two-year warranty on printer</li>
        <li>Two-year (unlimited passes) original printhead</li>
        <li>One-year hot swap (U.S. only)</li>
        <li>Kensington® secure lock slot</li>
        <br>
        <h3>Card Compatibility</h3>
        <li>Card Thickness: 10-40 mil</li>
        <li>Card Size: CR-80, ISO 7810 format, Type ID-1</li>
        <li>Card Material: PVC and PVC composite</li>
        <li>Smart Card Contact — ISO 7816-1,2,3,4</li>
        <li>Smart Card Contactless MIFARE® ISO 14443A/B</li>
        <li>Spot color or monochrome print only on < 20 mil card thickness</li>
    <br>
    <br>
    </div>

<div class="container col-sm-4 col-md-4 col-xl-4">
        <h3>MEDIA SPECIFICATIONS</h3>
        <li>Zebra intelligent technology automatically detects and authenticates ribbon</li>
        <li>Integrated cleaning roller included with each ribbon</li>
        <li>Media Starter Kit (single-sided): 1 YMCKO ribbon, 200 30-mil PVC cards</li>
        <li>Specially designed cleaning supplies simplify preventive maintenance</li>

        <br>

        <h3>PRINTING SPECIFICATIONS</h3>
        <li>Dye-sublimation thermal transfer direct to card</li>
        <li>Full color or monochrome printing</li>
        <li>Single- or dual-sided printing</li>
        <li>700 cards / hour monochrome single-sided</li>
        <li>195 cards / hour single-sided YMCKO</li>
        <li>140 cards / hour dual-sided YMCKOK </li>
        <li>Edge-to-edge printing on standard CR-80 media</li>

        <br>

        <h3>True Colours ix Series ZXP 3 Ribbons</h3>
        <li>YMCKO: 280 images/roll</li>
        <li>½ YMCKO: 400 images/roll</li>
        <li>YMCKOK: 230 images/roll</li>
        <li>KdO: 700 images/roll</li>
        <li>KrO: 700 images/roll</li>
        <li>Black Monochrome: 2000 images/roll</li>
        <li>Red Monochrome: 1700 images/roll</li>
        <li>Blue Monochrome: 1700 images/roll</li>
        <li>Gold Monochrome: 1600 images/roll</li>
        <li>Silver Monochrome: 1600 images/roll</li>
        <li>White Monochrome: 1400 images/roll</li>

        <br>


</div>
    <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/zxp3.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
</div>
</div>
