
<div class="container">
    <br>
    <h2 style = "color:#3379B0; font-size:26px">STANDARD FEATURES</h2>
    <div class="container">
            <li>Battery: NA</li>
            <li>Body: Strong Zinc Alloy Electroplated anti-vandal case</li>
            <li>Dimension: 12*56*20 mm</li>
            <li>ID Card Capacity: 2000</li>
            <li>Record Capacity: NA</li>
            <li>Card Type: Proximity card, Mifare (optional)</li>
            <li>Identify mode: Card, Password</li>
            <li>LED Indicator: 3 LED Status (Green, Yellow, Red)</li>
            <li>Keypad Type: Numeric Backlight keys</li>
            <li>Wiegand/RS485 Interface: Wiegand Input & Output 26/34 bit</li>
            <li>Waterproof: Conforms to IP68</li>
            <li>Operation temprature: 0°C — 60°C</li>
            <li>Wiring Connections: External Card Reader, Electric Lock, Exit Button, Alarm, Bell</li>
            <li>Operation Humidity: 10% ~ 90%</li>
            <li>Power: DC 12V 3A</li>
        
    <br> 
</div>
</div>