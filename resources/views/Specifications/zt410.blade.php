<div class="container row">
        <h2 style = "color:#3379B0;">ZT410 SPECIFICATIONS</h2>
        <div class="row">
        <div class="container col-sm-6 col-md-6 col-xl-6">
            <h3>STANDARD FEATURES</h3>
                <li>Print Methods: Thermal transfer and direct thermal printing</li>
                <li>Construction: Metal frame and bi-fold metal media cover with enlarged clear viewing window </li>
                <li>Side-loading supplies path for simplified media and ribbon loading</li>
                <li>Thin film printhead with E3™ Element Energy™ Equalizer for superior print quality </li>
                <li>Communications: USB 2.0, high-speed, RS-232 Serial, 10/100 Ethernet, Bluetooth 2.1, USB Host</li>
                <li>Back-lit, multi-line graphic LCD display with intuitive menu and easy-to-use keypad for quick operation</li>
                <li>Bi-colored status LEDs for quick printer status</li>
                <li>ENERGY STAR qualified</li>
            <br>
            <h3>Warranty</h3>
            <p>
                Subject to the terms of Zebra's hardware warranty statement, the DS2200 Series is warranted against defects in workmanship and materials for the following period from the date of shipment:
            </p>
                <li>DS2208 Corded Scanner: Sixty(60) months</li>
                <li>DS2278 Cordless Scanner: Thirty-six(36) months</li>
                <li>CR2278 Presentation Cradle: Thirty-six(36) months</li>
                <li>Battery: Tweleve(12) months</li>
            <br>
        </div>
        <div class="container col-md-4 col-sm-4 col-xl-4">
            <strong>Dimensions (closed)</strong>
                <li>19.5 in. L x 10.6 in. W x 12.75 in. H</li>
                <li>495 mm L x 269 mm W x 324 mm H</li>
            <strong>Weight</strong>
                <li>36 lbs./16.33 kg</li>
            <Strong>Resolution</Strong>
                <li>203 dpi/8 dots per mm</li>
                <li>300 dpi/12 dots per mm (optional)</li>
                <li>600 dpi/24 dots per mm (optional for ZT410 only</li>
            <Strong>Memory</Strong>
                <li>256 MB SDRAM memory</li>
                <li>512 MB on-board linear Flash memory</li>
            <Strong>Maximum Print Width</Strong>
                <li>4.09 in./104 mm</li>
            <Strong>Maximum Print Speed</Strong>
                <li>14 ips/356 mm per second</li>
            <Strong>Media Sensors</Strong>
                <li>Dual media sensors: transmissive and reflective</li>
            <Strong>Print Length</Strong>
                <li>203 dpi: 157 in./3988 mm</li>
                <li>300 dpi: 73 in./1854 mm</li>
                <li>600 dpi: 39 in./991 mm</li>
            <Strong>Thickness</Strong>
                <li>0.0023 in./0.058 mm to 0.010 in./0.25 mm</li>
            <Strong>Media Types</Strong>
                <li>Continuous, die-cut, notch, black-mark</li>
            <Strong>Operating Temperature</Strong>
                 <li>Thermal transfer: 40º F to 104º F/5º C to 40º C</li>
                <li>Direct thermal: 32º F to 104º F/0º C to 40º C</li>
            <br>
        </div>
             <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/zt410.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
        </div>
</div>
