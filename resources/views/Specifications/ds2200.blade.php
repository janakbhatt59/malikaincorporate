<div class="container row">
    <h2 style = "color:#3379B0;">DS2200 Series Specifications</h2>
    <div class="row">
    <div class="col-sm-6 col-md-6 col-xl-6">
        <h4>Dimensions</h4>
        <strong>Corded DS2208</strong>
        <li>6.5 in. Hx2.6 in. Wx3.9 in. D</li>
        <li>16.5 cm. Hx6.6 cm. Wx9.9 cm. D<</li>
        <br>
        <strong>Cordless DS2278</strong>
        <li>6.9 in. Hx2.6 in. Wx3.5 in. D</li>
        <li>17.5 cm. Hx6.6 cm. Wx9.0 cm. D<</li>
        <br>
        <strong>Presentation Cradle</strong>
        <li>2.8 in. Hx3.7 in. Wx4.8 in. D</li>
        <li>7.2 cm. Hx9.4 cm. Wx12.2 cm. D<</li>
        <br>
        <h4>Warranty</h4>
        <p>
            Subject to the terms of Zebra's hardware warranty statement, the DS2200 Series is warranted against defects in workmanship and materials for the following period from the date of shipment:</p>
            <li>DS2208 Corded Scanner: Sixty(60) months</li>
            <li>DS2278 Cordless Scanner: Thirty-six(36) months</li>
            <li>CR2278 Presentation Cradle: Thirty-six(36) months</li>
            <li>Battery: Tweleve(12) months</li>
        <br>
    </div>
    <div class="col-sm-4 col-md-4 col-xl-4">
        <strong>Input Voltage Range</strong>
            <li>4.5 to 5.5 VDC Host Powered; 4.5 to 5.5 VDC External Power Supply</li>
        <strong>Suppoorted Host Interfaces</strong>
            <li>USB, RS232, Keyboard Wedge, TGCS(IBM) 46XX over RS485</li>
        <Strong>Image Sensor</Strong>
            <li>640 x 480 pixels</li>
        <Strong>Minimum Element Resolution</Strong>
            <li>Code 39 - 4.0 mil; Code 128 - 4.0 mil; Data Matrix - 6.0 mil; QR Code - 6.7 mil </li>
        <Strong>Bluetooth Radio</Strong>
            <li>Standard Bluetooth Version 4.0 with BLE: Class 2 33 ft. (10.0m), Serial Port and HID Profiles; output power adjustable down from 2.0 dBm in 8 Steps </li>
        <Strong>Battery Capacity/Battery Type</Strong>
            <li>2,400 mAh Li-Ion Battery</li>
        <Strong>Scans Per Battery Charge</Strong>
            <li>110,000 scans at 60 Scans per Minute or 50,000 scans at 10 Scans per Minute</li>
        <Strong>Operating Time Per Full Charge</Strong>
            <li>84.0 Hours</li>
        <br>
    </div>
         <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/ds2200.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
    </div>

    </div>
