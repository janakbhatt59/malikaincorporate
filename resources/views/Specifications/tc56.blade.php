<div class="container row">
        <h2 style = "color:#3379B0;">TC 56 SPECIFICATIONS</h2>
        <div class="row">
            <div class="container col-sm-6 col-md-6 col-xl-6">
                <h3>Physical Characteristics</h3>
                <strong>Dimensions</strong>
                    <li>6.1 in. L x 2.9 in. W x 0.73 in. H</li>
                    <li>155 mm L x 75.5 mm W x 18.6 mm H</li>

                <strong>Weight</strong>
                <li>8.8 oz./249 g with battery</li>

                <strong>Display</strong>
                <li>5.0 in. High Definition (1280 x 720); exceptionally bright, outdoor viewable; optically bonded to touch panel, 600 NITs</li>

                <strong>Imager Window</strong>
                <li><strong>Corning Gorilla Glass</strong></li>

                <strong>Touch Panel</strong>
                <li>Dual mode capacitive touch with stylus or bare or gloved fingertip input (conductive stylus sold separately); Corning Gorilla Glass 5</li>

                <strong>Power</strong>
                <li>Rechargeable Li-Ion, Power Precision + Standard Capacity, 4300 mAh typical/4030 mAh minimum; improved battery technology for longer cycle times and real-time visibility into battery metrics for better battery management; fast USB charging (500mA)</li>

                <strong>Network Connections</strong>
                <li>WWAN, WLAN, WPAN (Bluetooth)</li>
                <br>
            </div>
            <div class="container col-sm-4 col-md-4 col-xl-4">
                <h3>Performance Characteristics</h3>
                <strong>CPU</strong>
                    <li>Snapdragon 650 64-bit hexa-core 1.8GHz </li>
                    <li>ARM</li>
                    <li>Cortex A72, power optimization</li>

                <strong>Operation System</strong>
                    <li>Android Oreo 8.1</li>


                <strong>Memory</strong>
                    <li><strong>Standard</strong>2GB RAM/16 GB Flash memory</li>
                    <li><strong>Optional</strong>4GB RAM/32GB Flash</li>
                <br>
                <h3>Warranty</h3>
                <p>

Subject to the terms of Zebra’s hardware warranty statement, the TC51/56Series is warranted against defects in workmanship and materials for a period of 1 (one) year from the date of shipment. For complete warranty statement, please visit: www.zebra.com/warranty
                </p>
                <br>
            </div>
                <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/tc56.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
        </div>
    </div>
