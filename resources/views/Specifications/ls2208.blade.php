<div class="container row">
    <h2 style = "color:#3379B0;">LS2208 Series Specifications</h2>
        <div class="container col-sm-6 col-md-6 col-xl-6">
            <strong>Corded DS2208 Dimensions</strong>
            <li>6.0 in. Hx2.5 in. Wx3.3 in. D</li>
            <li>15.2 cm. Hx6.4 cm. Wx8.5 cm. D<</li>
            <strong>Input Voltage Range</strong>
            <li>4.5 to 5.5 VDC Host Powered; 4.5 to 5.5 VDC External Power Supply</li>
            <strong>Suppoorted Host Interfaces</strong>
                <li>USB, RS232, Keyboard Wedge, TGCS(IBM) 46XX over RS485</li>
            <Strong>Operating Current at Nominal Voltage (5.0V)</Strong>
                <li>175 mA (typical)</li>
            <Strong>Standby Current (idle) at Nominal Voltage (5.0V) </Strong>
                <li>90 mA (typical)</li>
            <Strong>Scan Pattern</Strong>
                <li>Single line</li>
            <Strong>Scan Speed</Strong>
                <li>100 scans per second</li>
            <Strong>Operating Temperature</Strong>
                <li>32° to 122° F/0° to 50° C</li>
            <br>
        </div>
        <div class="container col-sm-4 col-md-4 col-xl-4">
            <Strong>Drop Specification</Strong>
                <li>Designed to withstand multiple drops at 5.0 ft./1.5 m to concrete</li>
            <Strong>Scanning Technology</Strong>
                <li>Code 39, Code 128, Code 93, Codabar/NW7, Code 11, MSI Plessey, UPC/EAN, I 2 of 5, Korean 3 of 5, GS1 DataBar, Base 32 (Italian Pharma). Refer to Product Reference Guide for complete list of symbologies.</li>
            <Strong>Minimum Element Resolution</Strong>
                <li>Code 39: 3.0 mil</li>
            <br>

            <h4>Warranty</h4>
            <p>
                Subject to the terms of Zebra’s hardware warranty statement, the LS2208, is warranted against defects in workmanship and materials for a period of five years from the date of shipment. Limited lifetime warranty on the scan element. Complete Zebra hardware product warranty statement: www.zebra.com/warranty
            </p>
        </div>
         <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/ls2208.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
    </div>
</div>
