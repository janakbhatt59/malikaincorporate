
<div class="container">
    <br>
    <h2 style = "color:#3379B0; font-size:26px">STANDARD FEATURES</h2>
    <div class="container">
            <li>LCD Display: 4.3 inches(Touch Screen)</li>
            <li>Face image capacity: 3,000</li>
            <li>Fingerprint capacity: 5,000</li>
            <li>RFID Card capacity: 5,000</li>
            <li>Log capacity: 3,00,000</li>
            <li>Fingerprint Scanner: Optical fingerprint Scanner</li>
            <li>Autheentication Method: Face, Card, Password</li>
            <li>Identification Mode: Face,RF-Card, PIN</li>
            <li>Verification Mode: 1:N & 1:1</li>
            <li>Language: English</li>
            <li>FAR: 0.0001% </li>
            <li>FRR: 0.1 </li>
            <li>Identification Time: 0.5sec</li>
            <li>Communation method: USB Drive, TCP/IP, Wifi,GPRS</li>
            <li>Push Data Technology: Yes</li>
            <li>Real Time Transmission: Yes</li>
            <li>Access Control: Able to control single door access through wiegand</li>
            <li>Voice Promt: High-Defination Pronounciation</li>
            <li>Operation temprature: 10℃-50℃</li>
            <li>Power Adapter: DC 12V</li>
            <li>Battery Backup: Optional</li>
    <br> 
</div>
</div>