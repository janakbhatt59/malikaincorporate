
<div class="container">
        <br>
        <h2 style = "color:#3379B0; font-size:26px">STANDARD FEATURES</h2>
        <div class="container">
        <li>LCD screen: 2.8" TFT Color Screen</li>
        <li>Authentication Method: Fingerprint, Card, Password</li>
        <li>Fingerprint capacity: 3,000</li>
        <li>Log capacity: 1,00,000</li>
        <li>Identification Mode: Fingerprint, Card, PIN</li>
        <li>Verification Mode: 1:N.1:1</li>
        <li>FRR: 0.01% </li>
        <li>FAR: 0.0001% </li>
        <li>Communation method: USB Drive, TCP/IP</li>
        <li>Push Data Technology: Yes</li>
        <li>Voice Promp: High-definition Pronunciation</li>
        <li>Battery backup: Yes</li>
        <li>Current voltage: DC 9V</li>
        <li>Operation humidity: 20-60%</li>
        <li>Operation temprature: 0°C — 60°C</li>
        <br> 
   </div>
    </div>