<div class="container row">
    <h2 style = "color:#3379B0;">ZXP-SERIES-7 SPECIFICATIONS</h2>
    <div class="row">
        <div class="container col-sm-6 col-md-6 col-xl-6">
        <h3>STANDARD FEATURES</h3>
            <li>300 dpi/11.8 dots per mm print resolution</li>
            <li>USB 2.0 and Ethernet 10/100 connectivity</li>
            <li>Magnetic stripe encoder — AAMVA and ISO 7811 (new and pre-encoded, tracks 1, 2, and 3, high and low coercivity)</li>
            <li>Microsoft® Windows Certified drivers</li>
            <li>250-card input capacity feeder (30 mil)</li>
            <li>15-card reject hopper (30 mil)</li>
            <li>250 First-in, First-out mechanical output hopper</li>
            <li>Single-card feed capability</li>
            <li>ix Series intelligent media technology</li>
            <li>21-character x 6-line LCD operator display with configurable soft keys</li>
            <li>Kensington® physical lock slot</li>
            <li>License key for Zebra Virtual PrintWare™ PrintMonitor™ software for monitoring of Network Attached Card Printers</li>
            <li>Two-year unlimited warranty on printer and printhead</li>

            <br>

            <h3>OPTIONAL FEATURES</h3>
            <li>FIPS 201 compliant dual-sided laminator</li>
            <li>Optional contact/contactless SmartCard encoding option module</li>
            <li>Lockable enclosures and card feeder</li>
            <li>Extended product warranty programs</li>
            <li>802.11b/g wireless connectivity</li>
            <li>1D barcode scanner</li>
            <br>
        </div>
        <div class="container col-sm-4 col-md-4 col-xl-4">
                <h3>PRINTING AND LAMINATING SPECIFICATIONS</h3>
                <li>Color dye sublimation or monochrome thermal-transfer printing</li>
                <li>Near photographic print quality</li>
                <li>Edge-to-edge printing on standard CR-80 media</li>
                <li>Simultaneous printing, encoding, and laminating capability</li>
                <li>One-pass single- and dual-sided wasteless lamination</li>
                <li>1375 cards/hour monochrome single-sided or 555 cards/hour dual-sided printing</li>
                <li>300 cards/hour full-color YMCKO single-sided printing</li>
                <li>225 cards/hour full-color YMCKOK dual-sided printing</li>
                <li>270 cards/hour full-color YMCKO single-sided printing with lamination</li>
                <li>200 cards/hour full-color YMCKOKO dual-sided printing with lamination</li>

                <br>

                <h3>SUPPLIES SPECIFICATIONS</h3>
                <li>Zebra’s intelligent technology RFID tags authenticate and automate ix Series ribbons and i Series laminates</li>
                <li>Card cleaning rollers included with each ribbon carton</li>
                <li>Specially designed cleaning supplies simplify preventative maintenance</li>

                <br>

                <h3>ENCODING OPTIONS AND SPECIFICATIONS</h3>
                <li>ISO 7816 Smart Card Contact Station for third-party external contact encoders</li>
                <li>Combined MIFARE® ISO 14443 A & B (13.56MHz) contactless and ISO 7816 Contact Encoder with EMV level 1 certification, and PC/SC compliance</li>
                <li>EPCglobal®Gen 2 UHF RFID encoder</li>
                <br>
        </div>
           <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/zxp7.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
    </div>
-</div>
