<div class="container row">
    <h2 style = "color:#3379B0;">GT800 SPECIFICATIONS</h2>
    <div class="row">
        <div class="container col-sm-6 col-md-6 col-xl-6">
            <h3>STANDARD FEATURES</h3>
                <li>300 meter ribbon capacity</li>
                <li>32 bit RISC processor</li>
                <li>Co-resident EPL2 and ZPL II programming languages </li>
                <li>Triple connectivity: Serial, USB and parallel</li>
                <li>Print methods: Thermal transfer and direct thermal modes; printing of bar codes, text and graphics</li>
                <li>OpenACCESS™ for easy media and ribbon loading</li>
                <li>Microsoft® Windows® drivers</li>

                 <Strong>Maximum media roll size</Strong>
                    <li>5”/127 mm O.D. on a 1.00”/25.4 mm, 1.5”/38 mm I.D. core</li>
                 <Strong>Media thickness</Strong>
                    <li>0.003"/0.08 mm to 0.007"/0.18 mm</li>
                 <Strong>Media types</Strong>
                    <li>Roll-fed or fan-fold</li>
                    <li>Label stock (die cut or continuous, direct thermal or thermal transfer)</li>
                    <li>Tag stock (die cut or continuous, direct thermal or thermal transfer)</li>
            <br>
        </div>
        <div class="container col-sm-4 col-md-4 col-xl-4">
            <strong>Resolution</strong>
                <li>203dpi/8 dots per mm/li>
            <strong>Memory</strong>
                <li>8 MB Flash, 8 MB SDRAM (standard)</li>
            <Strong>Print width</Strong>
                <li>4.09"/104 mm </li>
            <Strong>Print length</Strong>
                <li>39"/991 mm</li>
            <Strong>Print speed</Strong>
                <li>5"/127 mm per second</li>
            <Strong>Media Sensors</Strong>
                <li>Reflective and transmissive sensors </li>
            <Strong>Media width</Strong>
                <li>.75”/19.5 mm to 4.49”/114 mm</li>
             <Strong>Media length</Strong>
                <li>0.25”/6.5 mm to 39”/991 mm</li>
            <br>
        </div>
         <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/gt800.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
        </div>
    </div>
</div>
