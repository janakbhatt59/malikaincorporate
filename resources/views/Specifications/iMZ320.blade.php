<div class="container row">
        <h2 style = "color:#3379B0;">iMZ320 SPECIFICATIONS</h2>
        <div class="row">
        <div class="container col-sm-6 col-md-6 col-xl-6">
            <h3>STANDARD FEATURES</h3>
                <li>Direct-thermal printing of barcodes, text and graphics</li>
                <li>203dpi resolution (8 dots/mm)</li>
                <li>Print speed: up to 102mm/s (4 ips)</li>
                <li>CPCL and ZPL™ programming languages</li>
                <li>Resident fixed and scalable fonts</li>
                <li>400MHz 32-bit ARM® processor with 128MB RAM</li>
                <li>128MB flash supports downloadable programs, receipt formats, fonts, and graphics </li>
                <li>USB port</li>
                <li>1.6Ahr Li-Ion battery</li>
                <li>Built-in battery charger/li>
                <li>Optical out-of-media sensing using fixed centre-position sensor</li>
                <li>Tear bar for easy receipt dispensing</li>
                <li>Supports vertical and horizontal printing</li>
                <li>Printer can be used in any orientation</li>
                <li>Fixed media-roll width</li>
                <li>“Clamshell” design for easy media loading</li>
                <li>Management: Mirror, SNMP, Web server (with customisable HTML pages), Wavelink Avalanche® [Future firmware update]</li>
                <li>Four LEDs as user interface indicators/li>
                <li>1.2m (4') drop to concrete (multiple times) </li>
                <li>Label odometer</li>
            <br>
        </div>
        <div class="container col-sm-4 col-md-4 col-xl-4">
            <li>Bluetooth 2.1</li>
            <li>Security modes 1–4</li>
            <li>Bluetooth printing from Apple devices (iOS5, iOS6)</li>
            <li>IP42-rated for dust- and water-resistance (IP54 with durability-enhacing case)</li>
            <li>Print Touch via NFC</li>
            <strong>Resolution</strong>
                <li>203dpi/8 dots per mm/li>
            <strong>Memory</strong>
                <li>128MB RAM; 128MB flash</li>
            <Strong>Print width</Strong>
                <li>iMZ320: 73.7mm/2.9"</li>
            <Strong>Print length</Strong>
                <li>Maximum: Continuous</li>
            <Strong>Print speed</Strong>
                <li>Up to 102mm/4" per second</li>
            <br>
            <h3>OPTIONAL FEATURES</h3>
            <li>Linerless printing (with silicone platen roller only)
                    through CAG (Custom Application Group) request</li>
            <li>ZBI 2.x: Powerful programming language that lets
                    printers run standalone applications, connect to
                    peripherals, and much more</li>
        </div>
         <div class="col-xl-2 col-sm-2 col-md-2">
                 <a href="{{asset('assests/img/SpecsSheet/imz320.pdf')}}" class="btn btn-outline-primary" style="transition: all .3s ease-in-out; display: inline-block; padding-top:10px;">
          <i class="fa fa-hand-o-right"></i> View Full Specifications</a>
            </div>
        </div>
</div>
