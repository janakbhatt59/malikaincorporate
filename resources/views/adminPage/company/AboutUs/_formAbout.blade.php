<div class="container col-10">

    <div class="clear"></div>
    {{Form::label('Heading','Heading')}}
    {{Form::text('heading',null, ['class'=>'form-control form-inline','name'=>'heading','placeholder'=>'Heading','title'=>'Heading','required'=>true])}}

    <div class="clear"></div>
    {{Form::label('Description','Description')}}
    {{Form::textarea('description',null, ['class'=>'form-control','id'=>'description','name'=>'description','title'=>'Description','placeholder'=>'Description','required'=>true ])}}

</div>
