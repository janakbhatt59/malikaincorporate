@extends('backend.layout')
@section('content')

<div class="container col-10">
    {{ Form::open(['route'=>'storeabout','id'=>'store','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
        @include('adminPage.company.AboutUs._formAbout')
        <br>
    {{Form::submit('save',['class'=>'btn btn-primary  mt-10 float-right'])}}
    {{ Form::close() }}
</div>

@endsection
