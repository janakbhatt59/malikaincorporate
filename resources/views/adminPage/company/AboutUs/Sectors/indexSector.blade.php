@extends('backend.layout')
@section('content')

<div class="content iframe">
<div class="animated fadeIn">
    <div class="row">
    <div class="col-md-12">
    <a href = "{{  route('addsector') }}"><button class = "btn btn-primary">Add more content to Sectors</button></a>
    <br>
    <br>
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Sectors we Serve</strong>
            </div>
            <div class="card-body">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
                
            <tr>
                <th>Sn.</th>
                <th>Heading</th>
                <th width="205">Description</th>
                <th width="205">Product</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach($sector as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{$row->heading}}</td>
                        <td>{{$row->description}}</td>
                        <td>{{$row->product}}</td>

                        <td>
                            <a href={{ route('editsector',$row->id) }}><i class="fa fa-edit" title="Edit"></i></a>
                            <a href={{ route('deletesector',$row->id) }}><i class="fa fa-trash-o" title="Delete"></i></a>
                        </td>
                    </tr>
                 @endforeach
            </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
@endsection
