<div class="container col-10">

    <div class="clear"></div>
    {{Form::label('Heading','Heading')}}
    {{Form::text('heading',null, ['class'=>'form-control form-inline','name'=>'heading','placeholder'=>'Heading','title'=>'Heading','required'=>true])}}

    <div class="clear"></div>
    {{Form::label('Description','Description')}}
    {{Form::text('description',null, ['class'=>'form-control','id'=>'description','name'=>'description','title'=>'Description','placeholder'=>'Description','required'=>true ])}}

    <div class="clear"></div>
    <div class="clear"></div>
    {{Form::label('Product','Product')}}
    {{Form::textarea('product',null, ['class'=>'form-control','id'=>'product','name'=>'product','title'=>'Products','placeholder'=>'Products','required'=>true ])}}


</div>
