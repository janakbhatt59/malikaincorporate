@extends('backend.layout')
@section('content')
 <div class="container col-10">
        {{ Form::model($zxp3ribbons,['route'=>['updatezxp3ribbon',$zxp3ribbons->id],'id'=>'update','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
           @include('adminPage.company.Ribbons._formRibbon')
        <br>
        {{Form::submit('update',['class'=>'btn btn-primary  mt-10 float-right'])}}
        {{ Form::close() }}
    </div>
@endsection