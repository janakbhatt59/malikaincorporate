
{{Form::label('Brand','Brand')}}
{{Form::text('brand',null, ['class'=>'form-control','name'=>'brand','placeholder'=>'Brand','title'=>'Brand','required'=>true])}}
  <br>
{{Form::label('Model','Model')}}
{{Form::text('model',null, ['class'=>'form-control','name'=>'model','placeholder'=>'Model','title'=>'Model','required'=>true])}}
  <br>
{{Form::label('Print Capacity','Print Capacity')}}
{{Form::text('printCapacity',null, ['class'=>'form-control','name'=>'printCapacity','placeholder'=>'Print Capacity','title'=>'Print Capacity','required'=>true])}}
  <br>
{{Form::label('Panel Type','Panel Type')}}
{{Form::text('panelType',null,['class'=>'form-control','id'=>'panelType','name'=>'panelType','placeholder'=>'Panel Type','title'=>'Panel Type','required'=>true])}}
  <br>
{{Form::label('Image','Image')}}
{{Form::file('image',null, ['class'=>'form-control','name'=>'image','title'=>'Image','placeholder'=>'Image','required'=>true])}}

