@extends('backend.layout')
@section('content')


<div class="content iframe">
    <div class="animated fadeIn">
        <div class="row">
        <div class="col-md-12">
                <a href = "{{  route('addzxp7ribbon') }}"><button class = "btn btn-primary">Add more Ribbons</button></a>
                <br>
                <br>
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">List of Ribbons</strong>
                </div>
                <div class="card-body">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                                    <th width="50">Sn.</th>
                                    <th>Brand</th>
                                    <th width="300">Model</th>
                                    <th width="205">Print Capacity</th>
                                    <th width="205">Panel Type</th>
                                    <th width="205">Photo</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($zxp7ribbons as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->brand}}</td>
                                        <td>{{$row->model}}</td>
                                        <td>{{$row->printCapacity}}</td>
                                        <td>{{$row->panelType}}</td>
                                        <td class="avatar">
                                            <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->image)}}"></div>
                                        </td>
                                        <td>
                                            <a href={{ route('editzxp7ribbon',$row->id) }}><i class="fa fa-edit" title="Edit"></i></a>
                                            <a href={{ route('deletezxp7ribbon',$row->id) }}><i class="fa fa-trash-o" title="Delete"></i></a>
                                        </td>
                                    </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
    
        </div>
    </div><!-- .animated -->
    </div>
@endsection
