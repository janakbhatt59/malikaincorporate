@extends('backend.layout')
@section('content')
 <div class="container col-10">
        {{ Form::model($zxp7ribbons,['route'=>['updatezxp7ribbon',$zxp7ribbons->id],'id'=>'update','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
           @include('adminPage.company.Ribbons._formRibbon')
        <br>
        {{Form::submit('update',['class'=>'btn btn-primary  mt-10 float-right'])}}
        {{ Form::close() }}
    </div>
@endsection