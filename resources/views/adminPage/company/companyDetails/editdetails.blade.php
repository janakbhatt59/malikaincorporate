@extends('backend.layout')
@section('content')
 <div class="container col-10">
        {{ Form::model($details,['route'=>['updatedetails',$details->id],'id'=>'update','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
           @include('adminPage.company.companyDetails._formdetails')
        <br>
        {{Form::submit('update',['class'=>'btn btn-primary  mt-10 float-right'])}}
        {{ Form::close() }}
    </div>
@endsection