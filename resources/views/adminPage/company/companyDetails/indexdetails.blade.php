@extends('backend.layout')
@section('content')


<div class="content iframe">
    <div class="animated fadeIn">
        <div class="row">
        <div class="col-md-12">
                <a href = "{{  route('adddetails') }}"><button class = "btn btn-primary">Add more CompanyDetails</button></a>
                <br>
                <br>
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">List of CompanyDetails</strong>
                </div>
                <div class="card-body">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                                    <th width="50">Sn.</th>
                                    <th width="205">Company Name</th>
                                    <th width="300">Phone Number</th>
                                    <th width="300">Email</th>
                                    <th width="205">Address</th>
                                    <th width="300">logo</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($details as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->CompanyName}}</td>
                                        <td>{{$row->phon}}</td>
                                        <td>{{$row->email}}</td>
                                        <td>{{$row->address}}</td>
                                        <td class="avatar">
                                            <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->logo)}}" alt=""></div>
                                        </td>
                                        <td>
                                            <a href={{ route('editdetails',$row->id) }}><i class="fa fa-edit" title="Edit Company Details"></i></a>
                                            <a href={{ route('deletedetails',$row->id) }}><i class="fa fa-trash-o" title="Delete Company Details"></i></a>
                                        </td>
                                    </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
    
        </div>
    </div><!-- .animated -->
    </div>
@endsection
