@extends('backend.layout')
@section('content')


<div class="content iframe">
    <div class="animated fadeIn">
        <div class="row">
        <div class="col-md-12">
                <a href = "{{  route('addCareers') }}"><button class = "btn btn-primary">Add Careers</button></a>
                <br>
                <br>
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">List of Ribbons</strong>
                </div>
                <div class="card-body">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                                    <th width="50">Sn.</th>
                                    <th>Job Summery</th>
                                    <th width="300">Job Description1</th>
                                    <th width="300">Job Description2</th>
                                    <th width="300">Job Description3</th>
                                    <th width="300">Job Description4</th>
                                    <th width="300">Job Description5</th>
                                    <th width="300">Job Description6</th>
                                    <th width="300">Job Description7</th>
                                    <th width="300">Job Description8</th>
                                    <th width="300">Job Description9</th>
                                    <th width="300">Job Description10</th>
                                    <th width="300">Job Specification1</th>
                                    <th width="300">Job Specification2</th>
                                    <th width="300">Job Specification3</th>
                                    <th width="300">Job Specification4</th>
                                    <th width="300">Job Specification5</th>
                                    <th width="300">Job Specification6</th>
                                    <th width="300">Job Specification7</th>
                                    <th width="300">Job Specification8</th>
                                    <th width="300">Job Specification9</th>
                                    <th width="300">Job Specification10</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($careers as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->jobSummary}}</td>
                                        <td>{{$row->jobDescription1}}</td>
                                        <td>{{$row->jobDescription2}}</td>
                                        <td>{{$row->jobDescription3}}</td>
                                        <td>{{$row->jobDescription4}}</td>
                                        <td>{{$row->jobDescription5}}</td>
                                        <td>{{$row->jobDescription6}}</td>
                                        <td>{{$row->jobDescription7}}</td>
                                        <td>{{$row->jobDescription8}}</td>
                                        <td>{{$row->jobDescription9}}</td>
                                        <td>{{$row->jobDescription10}}</td>
                                        <td>{{$row->jobSpecification1}}</td>
                                        <td>{{$row->jobSpecification2}}</td>
                                        <td>{{$row->jobSpecification3}}</td>
                                        <td>{{$row->jobSpecification4}}</td>
                                        <td>{{$row->jobSpecification5}}</td>
                                        <td>{{$row->jobSpecification6}}</td>
                                        <td>{{$row->jobSpecification7}}</td>
                                        <td>{{$row->jobSpecification8}}</td>
                                        <td>{{$row->jobSpecification9}}</td>
                                        <td>{{$row->jobSpecification10}}</td>
                                        <td>
                                            <a href={{ route('editCareers',$row->id) }}><i class="fa fa-edit" title="Edit"></i></a>
                                            <a href={{ route('deleteCareers',$row->id) }}><i class="fa fa-trash-o" title="Delete"></i></a>
                                        </td>
                                    </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
    </div>
@endsection



