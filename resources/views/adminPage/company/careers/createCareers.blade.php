@extends('backend.layout')
@section('content')
    {{Form::open(['route'=>'storeCareers','id'=>'store','method'=>'post','enctype'=>'multipart/form-data','files' => true])}}
        @include('adminPage.company.careers.formCareers')
        {{Form::submit('save',['class'=>'btn btn-danger  mt-10 float-right', 'onMouseOver'=>"this.style.color='#0F0'"])}}
    {{Form:: close()}}

@endsection
