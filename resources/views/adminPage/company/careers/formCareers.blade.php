
          <h3 class="alert alert-primary">Job Summary</h3>
<div class="container-fluid">
        {{Form::label('jobSummary','Job Summary:')}}
        {{Form::text('jobSummary',null, ['class'=>'form-control','name'=>'jobSummary','placeholder'=>'Job summery for careers'])}}<br>
</div>
<div class="row">
<div class="col-sm-12 col-lg-6 col-xl-6">
<div class="container">
          <h3 class="alert alert-primary">Job Descriptions</h3>
        {{Form::label('jobDescription1','Job Description1:')}}
        {{Form::text('jobDescription1',null, ['class'=>'form-control','name'=>'jobDescription1','title'=>'Job description1'])}}
        {{Form::label('jobDescription2','Job Description2:')}}
        {{Form::text('jobDescription2',null, ['class'=>'form-control','name'=>'jobDescription2','title'=>'Job description2'])}}
        {{Form::label('jobDescription3','Job Description3:')}}
        {{Form::text('jobDescription3',null, ['class'=>'form-control','name'=>'jobDescription3','title'=>'Job description3'])}}
        {{Form::label('jobDescription4','Job Description4:')}}
        {{Form::text('jobDescription4',null, ['class'=>'form-control','name'=>'jobDescription4','title'=>'Job description4'])}}
        {{Form::label('jobDescription5','Job Description5:')}}
        {{Form::text('jobDescription5',null, ['class'=>'form-control','name'=>'jobDescription5','title'=>'Job description5'])}}
        {{Form::label('jobDescription6','Job Description6:')}}
        {{Form::text('jobDescription6',null, ['class'=>'form-control','name'=>'jobDescription6','title'=>'Job description6'])}}
        {{Form::label('jobDescription7','Job Description7:')}}
        {{Form::text('jobDescription7',null, ['class'=>'form-control','name'=>'jobDescription7','title'=>'Job description7'])}}
        {{Form::label('jobDescription8','Job Description8:')}}
        {{Form::text('jobDescription8',null, ['class'=>'form-control','name'=>'jobDescription8','title'=>'Job description8'])}}
        {{Form::label('jobDescription9','Job Description9:')}}
        {{Form::text('jobDescription9',null, ['class'=>'form-control','name'=>'jobDescription9','title'=>'Job description9'])}}
        {{Form::label('jobDescription10','Job Description10:')}}
        {{Form::text('jobDescription10',null, ['class'=>'form-control','name'=>'jobDescription10','title'=>'Job description10'])}}
        </div></div>
        <div class="col-sm-12 col-lg-6 col-xl-6">
        <div class="container">
        <h3 class="alert alert-primary">Job Specifications</h3>
        {{Form::label('jobSpecification1','Job Specification1:')}}
        {{Form::text('jobSpecification1',null, ['class'=>'form-control','name'=>'jobSpecification1','title'=>'Job Specification1'])}}
        {{Form::label('jobSpecification2','Job Specification2:')}}
        {{Form::text('jobSpecification2',null, ['class'=>'form-control','name'=>'jobSpecification2','title'=>'Job Specification2'])}}
        {{Form::label('jobSpecification3','Job Specification3:')}}
        {{Form::text('jobSpecification3',null, ['class'=>'form-control','name'=>'jobSpecification3','title'=>'Job Specification3'])}}
        {{Form::label('jobSpecification4','Job Specification4:')}}
        {{Form::text('jobSpecification4',null, ['class'=>'form-control','name'=>'jobSpecification4','title'=>'Job Specification4'])}}
        {{Form::label('jobSpecification5','Job Specification5:')}}
        {{Form::text('jobSpecification5',null, ['class'=>'form-control','name'=>'jobSpecification5','title'=>'Job Specification5'])}}
        {{Form::label('jobSpecification6','Job Specification6:')}}
        {{Form::text('jobSpecification6',null, ['class'=>'form-control','name'=>'jobSpecification6','title'=>'Job Specification6'])}}
        {{Form::label('jobSpecification7','Job Specification7:')}}
        {{Form::text('jobSpecification7',null, ['class'=>'form-control','name'=>'jobSpecification7','title'=>'Job Specification7'])}}
        {{Form::label('jobSpecification8','Job Specification8:')}}
        {{Form::text('jobSpecification8',null, ['class'=>'form-control','name'=>'jobSpecification8','title'=>'Job Specification8'])}}
        {{Form::label('jobSpecification9','Job Specification9:')}}
        {{Form::text('jobSpecification9',null, ['class'=>'form-control','name'=>'jobSpecification9','title'=>'Job Specification9'])}}
        {{Form::label('jobSpecification10','Job Specification10:')}}
        {{Form::text('jobSpecification10',null, ['class'=>'form-control','name'=>'jobSpecification10','title'=>'Job Specification10',])}}
</div>
</div>
</div>
