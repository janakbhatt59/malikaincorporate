@extends('backend.layout')
@section('content')

<div class="container col-10">
    {{ Form::open(['route'=>'storePhoto','id'=>'store','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
        @include('adminPage.company.Gallery._galleryForm')
        <br>
    {{Form::submit('save',['class'=>'btn btn-primary  mt-10 float-right'])}}
    {{ Form::close() }}
</div>

@endsection
