@extends('backend.layout')
@section('content')

<div class="content iframe">
<div class="animated fadeIn">
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Gallery</strong>
            </div>
            <div class="card-body">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead> 
            <tr>
                <th>Sn.</th>
                <th>File Name</th>
                <th width="205">Image</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach($gallery as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{$row->fileName}}</td>
                        <td class="avatar">
                            <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->ImageName)}}" alt=""></div>
                        </td>
                        <td>
                            <a href={{ route('editPhoto',$row->id) }}><i class="fa fa-edit" title="Edit Photo"></i></a>
                            <a href={{ route('deletePhoto',$row->id) }}><i class="fa fa-trash-o" title="Delete Photo"></i></a>
                        </td>
                    </tr>
                 @endforeach
            </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
@endsection
