@extends('backend.layout')
@section('content')

<div class="content iframe">
<div class="animated fadeIn">
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">List of Blog Details</strong>
            </div>
            <div class="card-body">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
            <tr>
                           <th width="50">Sn.</th>
                                <th width="205">Image1</th>
                                <th width="300">Heading1</th>
                                <th width="300">Description1</th>
                                <th width="205">Image2</th>
                                <th width="300">Heading2</th>
                                <th width="300">Description2</th>
                                <th width="205">Image3</th>
                                <th width="300">Heading3</th>
                                <th width="300">Description3</th>
                                <th width="110">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        {{ $count = 1}}
                            @foreach($navOpen as $row)
                    <tr><td>{{$row['id']}}</td>

    <td class="avatar">
        <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->image1)}}" alt=""></div>
    </td>

        <td>{{$row['heading1']}}</td>
        <td>{{$row['description1']}}</td>

    <td class="avatar">
        <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->image2)}}" alt=""></div>
    </td>

        <td>{{$row['heading2']}}</td>
        <td>{{$row['description2']}}</td>

    <td class="avatar">
        <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->image3)}}" alt=""></div>
    </td>

        <td>{{$row['heading3']}}</td>
        <td>{{$row['description3']}}</td>
        <td>
            <a href={{ route('editNavOpen',$row->id) }}><i class="fa fa-edit" title="Edit Content"></i></a>
            <a href={{ route('deleteNavOpen',$row->id) }}><i class="fa fa-trash-o" title="Delete Content"></i></a>
    </td>
    </tr>
@endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div><!-- .animated -->
</div>
@endsection
