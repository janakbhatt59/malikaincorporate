@extends('backend.layout')
@section('content')
 <div class="container col-10">
        {{ Form::model($navOpen,['route'=>['updateNavOpen',$navOpen->id],'id'=>'update','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
           @include('adminPage.company.navOpen.formNavOpen')
        <br>
        {{Form::submit('update',['class'=>'btn btn-danger  mt-10 float-right', 'onMouseOver'=>"this.style.color='#0F0'"])}}
        {{ Form::close() }}
    </div>
@endsection
