
          <h3 class="alert alert-primary">Description First</h3>
        {{Form::label('image1','Insert Image1:')}}
        {{Form::file('image1',null,['class'=>'form-control img','id'=>'image1','name'=>'image1','title'=>'Insert photo1','value'=>'Insert photo of content 1','required'=>true])}}
        <br>
        {{Form::label('heading1','Heading1:')}}
        {{Form::text('heading1',null, ['class'=>'form-control','id'=>'heading1','name'=>'heading1','placeholder'=>'Enter heading of content 1','title'=>'Heading of content 1','required'=>true])}}
        {{Form::label('description1','Description1:')}}
        {{Form::textarea('description1',null, ['class'=>'form-control','id'=>'desc1','name'=>'desc1','bind'=>'desc','title'=>'Description of content 1','placeholder'=>'Enter description about content 1','required'=>true])}}

          <br> <h3 class="alert alert-primary">Description Second</h3>
        {{Form::label('image2','Insert Image2:')}}
        {{Form::file('image2',null,['class'=>'form-control img','id'=>'image2','name'=>'image2','title'=>'Insert photo2','value'=>'Insert photo of content 2','required'=>true])}}
        <br>
        {{Form::label('heading2','Heading2:')}}
        {{Form::text('heading2',null, ['class'=>'form-control','id'=>'heading2','name'=>'heading2','placeholder'=>'Enter heading of content 2','title'=>'Heading of content 2','required'=>true])}}
        {{Form::label('description2','Description2:')}}
        {{Form::textarea('description2',null, ['class'=>'form-control','id'=>'desc2','name'=>'desc2','bind'=>'desc','title'=>'Description of content 2','placeholder'=>'Enter description about content 2','required'=>true])}}

         <br> <h3 class="alert alert-primary">Description Third</h3>
        {{Form::label('image3','Insert Image3:')}}
        {{Form::file('image3',null,['class'=>'form-control img','id'=>'image3','name'=>'image3','title'=>'Insert photo3','value'=>'Insert photo of content 3','required'=>true])}}
        <br>
        {{Form::label('heading3','Heading3:')}}
        {{Form::text('heading3',null, ['class'=>'form-control','id'=>'heading3','name'=>'heading3','placeholder'=>'Enter heading of content 3','title'=>'Heading of content 3','required'=>true])}}
        {{Form::label('description3','Description3:')}}
        {{Form::textarea('description3',null, ['class'=>'form-control','id'=>'desc3','name'=>'desc3','bind'=>'desc','title'=>'Description of content 3','placeholder'=>'Enter description about content 3','required'=>true])}}
