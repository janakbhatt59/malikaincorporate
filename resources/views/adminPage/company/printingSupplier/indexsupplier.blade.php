@extends('backend.layout')
@section('content')


<div class="content iframe">
    <div class="animated fadeIn">
        <div class="row">
        <div class="col-md-12">
                <a href = "{{  route('addsupplier') }}"><button class = "btn btn-primary">Add more CompanySupplier</button></a>
                <br>
                <br>
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">List of CompanySupplier</strong>
                </div>
                <div class="card-body">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                                    <th width="50">Sn.</th>
                                    <th>Name</th>
                                    <th width="300">Description</th>
                                    <th width="205">Photo</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($supplier as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->desc}}</td>
                                        <td class="avatar">
                                            <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->photo)}}" alt=""></div>
                                        </td>
                                        <td>
                                            <a href={{ route('editsupplier',$row->id) }}><i class="fa fa-edit" title="Edit Company Supplier"></i></a>
                                            <a href={{ route('deletesupplier',$row->id) }}><i class="fa fa-trash-o" title="Delete Company Supplier"></i></a>
                                        </td>
                                    </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
    
        </div>
    </div><!-- .animated -->
    </div>
@endsection
