@extends('backend.layout')
@section('content')


<div class="content iframe">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">List of Blog Details</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="50">Sn.</th>
                                            <th width="205">Image</th>
                                            <th width="230">Title</th>
                                            <th width="146">Date From</th>
                                            <th width="146">Date To</th>
                                            <th width="250">Description</th>
                                            <th width="110">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
   	    		                   {{ $count = 1}}
                                     @foreach($blogs as $row)
                 <tr><td>{{ $count ++ }}</td>

                <td class="avatar">
                  <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->image)}}" alt=""></div>
                </td>

                    <td>{{$row['title']}}</td>
                    <td>{{$row['date_from']}}</td>
                    <td>{{$row['date_to']}}</td>
                    <td>{{$row['description']}}</td>
                   <td>
                     <a href={{ route('editBlog',$row->id) }}><i class="fa fa-edit" title="Edit blog"></i></a>
                     <a href={{ route('deleteBlog',$row->id) }}><i class="fa fa-trash-o" title="Delete blog"></i></a>
                </td>
                </tr>
@endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
    </div>
@endsection
