 @extends('backend.layout')
@section('content')
 <div class="container col-10">
        {{ Form::open(['route'=>'storeBlog','id'=>'store','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
           @include('adminPage.company.formBlog')
        <br>
        {{Form::submit('save',['class'=>'btn btn-danger  mt-10 float-right', 'onMouseOver'=>"this.style.color='#0F0'"])}}
        {{ Form::close() }}
    </div>
@endsection
