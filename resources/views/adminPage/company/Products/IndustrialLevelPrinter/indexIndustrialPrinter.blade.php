@extends('backend.layout')
@section('content')

<div class="content iframe">
<div class="animated fadeIn">
    <div class="row">
    
    <div class="col-md-12">
    <a href = "{{  route('addindustrialLevelPrinter') }}"><button class = "btn btn-primary">Add more Industrial Level Printers Products</button></a>
    <br>
    <br>
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Industrial Level Printer</strong>
            </div>
            <div class="card-body">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Sn.</th>
                <th>Heading</th>
                <th width="205">Description</th>
                <th width="205">Bullet 1</th>
                <th width="205">Bullet 2</th>
                <th width="205">Bullet 3</th>
                <th width="205">Image</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach($industrialprinter as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{$row->heading}}</td>
                        <td>{{$row->description}}</td>
                        <td>{{$row->bullet1}}</td>
                        <td>{{$row->bullet2}}</td>
                        <td>{{$row->bullet3}}</td>
                        <td class="avatar">
                            <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->image)}}" alt=""></div>
                        </td>
                        <td>
                            <a href={{ route('editindustrialLevelPrinter',$row->id) }}><i class="fa fa-edit" title="Edit"></i></a>
                            <a href={{ route('deleteindustrialLevelPrinter',$row->id) }}><i class="fa fa-trash-o" title="Delete"></i></a>
                        </td>
                    </tr>
                 @endforeach
            </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
@endsection
