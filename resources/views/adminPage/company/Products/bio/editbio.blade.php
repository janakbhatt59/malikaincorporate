@extends('backend.layout')
@section('content')
 <div class="container col-10">
        {{ Form::model($bio,['route'=>['updatecardpinter',$bio->id],'id'=>'update','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
           @include('adminPage.company.Products._formProducts')
        <br>
        {{Form::submit('update',['class'=>'btn btn-primary  mt-10 float-right'])}}
        {{ Form::close() }}
    </div>
@endsection