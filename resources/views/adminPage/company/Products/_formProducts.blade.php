{{Form::label('Heading','Heading')}}
{{Form::text('heading',null, ['class'=>'form-control','name'=>'heading','placeholder'=>'Enter heading','title'=>'heading','required'=>true])}}

<br>
<br>

{{Form::label('Image','Insert Photo')}}
<br>
{{Form::file('image',null,['class'=>'form-control img','name'=>'image','set-to'=>'imageupa','title'=>'Insert photo','value'=>'Insert photo','required'=>true])}}
<br>
<br>
{{Form::label('Description','Description')}}
{{Form::text('description',null, ['class'=>'form-control','name'=>'description','placeholder'=>'description','title'=>'description','required'=>true])}}

<br>
<br>

{{Form::label('Bullet 1','Bullet 1')}}
{{Form::text('bullet1',null, ['class'=>'form-control','name'=>'bullet1','placeholder'=>'bullet1','title'=>'bullet1','required'=>true])}}

<br>
<br>

{{Form::label('Bullet 2','Bullet 2')}}
{{Form::text('bullet2',null, ['class'=>'form-control','name'=>'bullet2','placeholder'=>'bullet2','title'=>'bullet2','required'=>true])}}

<br>
<br>

{{Form::label('Bullet 3','Bullet 3')}}
{{Form::text('bullet3',null, ['class'=>'form-control','name'=>'bullet3','placeholder'=>'bullet3','title'=>'bullet3','required'=>true])}}
