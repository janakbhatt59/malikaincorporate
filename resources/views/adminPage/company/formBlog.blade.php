
        {{Form::label('title','Title:')}}
        {{Form::text('title',null, ['class'=>'form-control','name'=>'title','placeholder'=>'Enter title of blog','title'=>'Title of blog','required'=>true])}}

        {{Form::label('image','Insert Photo:')}}
        {{Form::file('image',null,['class'=>'form-control img','name'=>'image','set-to'=>'imageupa','title'=>'Insert photo','value'=>'Insert photo of student','required'=>true])}}

        <div class="row">
        <div class="col-6">
        {{Form::label('date_from','Date From:')}}
        {{Form::date('date_from',null, ['class'=>'form-control','id'=>'date_from','name'=>'date_from','title'=>'Start date of blog','placeholder'=>'Enter Start date of blog','required'=>true ])}}
        </div>
        <div class="col-6">
        {{Form::label('date_to','Date to:')}}
        {{Form::date('date_to',null, ['class'=>'form-control','id'=>'date_to','name'=>'date_to','title'=>'End date of blog','placeholder'=>'Enter end date of blog','required'=>true ])}}
        </div>
        </div>
        {{Form::label('description','Description:')}}
        {{Form::text('description',null, ['class'=>'form-control','name'=>'desc','bind'=>'desc','title'=>'Description of blog','placeholder'=>'Enter description about blog','required'=>true])}}
