@extends('backend.layout')
@section('content')
 <div class="container col-10">
        {{ Form::model($team,['route'=>['updateteam',$team->id],'id'=>'update','method'=>'post','enctype'=>'multipart/form-data','files' => true]) }}
           @include('adminPage.company.OurTeam._formteam')
        <br>
        {{Form::submit('update',['class'=>'btn btn-primary  mt-10 float-right'])}}
        {{ Form::close() }}
    </div>
@endsection