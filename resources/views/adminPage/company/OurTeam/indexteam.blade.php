@extends('backend.layout')
@section('content')


<div class="content iframe">
    <div class="animated fadeIn">
        <div class="row">
        <div class="col-md-12">
                <a href = "{{  route('addteam') }}"><button class = "btn btn-primary">Add more Teams</button></a>
                <br>
                <br>
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">List of Teams</strong>
                </div>
                <div class="card-body">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                                    <th width="50">Sn.</th>
                                    <th width="300">Name</th>
                                    <th width="300">Post</th>
                                    <th width="300">Photo</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($team as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->post}}</td>
                                        <td class="avatar">
                                            <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->photo)}}" alt=""></div>
                                        </td>
                                        <td>
                                            <a href={{ route('editteam',$row->id) }}><i class="fa fa-edit" title="Edit Teams"></i></a>
                                            <a href={{ route('deleteteam',$row->id) }}><i class="fa fa-trash-o" title="Delete Teams"></i></a>
                                        </td>
                                    </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
    
        </div>
    </div><!-- .animated -->
    </div>
@endsection
