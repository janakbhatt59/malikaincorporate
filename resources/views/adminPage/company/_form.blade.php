 <div class="container col-10">

        <div class="clear"></div>
        {{Form::label('roll_no','Roll No:')}}
        {{Form::number('roll_no',null, ['class'=>'form-control form-inline','name'=>'roll','bind'=>'roll','placeholder'=>'Roll No','title'=>'Enter roll number','required'=>true])}}

        <div class="clear"></div>
        {{Form::label('name','Name:')}}
        {{Form::text('name',null, ['class'=>'form-control','id'=>'name','name'=>'name','bind'=>'name','title'=>'Name of student','placeholder'=>'Enter name of student','required'=>true ])}}

        {{Form::label('image','Insert Photo:')}}
        {{Form::file('image',null,['class'=>'form-control img','name'=>'image','bind'=>'image','set-to'=>'imageupa','title'=>'Insert photo','value'=>'Insert photo of student','required'=>true])}}

        <div class="clear"></div>
        {{Form::label('faculty','Faculty:')}}
        {{Form::text('faculty',null, ['class'=>'form-control','name'=>'faculty','bind'=>'faculty','title'=>'Faculty of student','placeholder'=>'Enter faculty of student','required'=>true])}}

        <div class="clear"></div>
        {{Form::label('level','Level')}}
        {{Form::text('level',null,['class'=>'form-control','bind'=>'level','name'=>'level','title'=>'Level of student','placeholder'=>'Enter level of student','required'=>true])}}

        <div class="clear"></div>
        {{Form::label('phon_no','Phone Number:')}}
       {{Form::number('phon_no',null, ['class'=>'form-control','name'=>'phon','bind'=>'phon','title'=>'Phone number of student','placeholder'=>'Enter phon number of student','required'=>true])}}

        <div class="clear"></div>
       {{Form::label('email','E-mail:')}}
       {{Form::text('email',null, ['class'=>'form-control','name'=>'email','bind'=>'email','title'=>'email of student','placeholder'=>'Enter email of student','required'=>true])}}
        <div class="clear"></div>
       {{Form::label('address','Address:')}}
       {{Form::text('address',null, ['class'=>'form-control','name'=>'address','bind'=>'address','title'=>'Address of student','placeholder'=>'Enter address of student','required'=>true])}}

        <div class="clear"></div>
       {{Form::label('issued_date','Issued Date:')}}
       {{Form::date('issued_date',null, ['class'=>'form-control','name'=>'issued_date','bind'=>'issued_date','title'=>'card issued date of student','placeholder'=>'Enter card issued date  of student','required'=>true])}}

        <div class="clear"></div>
       {{Form::label('valid_upto','Valid-Upto:')}}
       {{Form::text('valid_upto',null, ['class'=>'form-control','name'=>'valid_upto','bind'=>'valid_upto','title'=>'card validation of student','placeholder'=>'Enter card validation  of student','required'=>true])}}

    </div>
