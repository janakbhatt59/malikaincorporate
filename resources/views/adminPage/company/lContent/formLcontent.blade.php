        {{Form::label('image','Insert Image:')}}
        {{Form::file('image',null,['class'=>'form-control img','id'=>'image','name'=>'image','set-to'=>'imageupa','title'=>'Insert photo','value'=>'Insert photo of student','required'=>true])}}
        <br>
        {{Form::label('heading','Heading:')}}
        {{Form::text('heading',null, ['class'=>'form-control','id'=>'heading','name'=>'heading','placeholder'=>'Enter heading of content','title'=>'Heading of content','required'=>true])}}

        {{Form::label('description','Description:')}}
        {{Form::text('description',null, ['class'=>'form-control','id'=>'desc','name'=>'desc','bind'=>'desc','title'=>'Description of blog','placeholder'=>'Enter description about blog','required'=>true])}}
