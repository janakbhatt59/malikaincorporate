@extends('backend.layout')
@section('content')

<div class="content iframe">
<div class="animated fadeIn">
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">List of Blog Details</strong>
            </div>
            <div class="card-body" >
            <table id="bootstrap-data-table table" class="table table-striped table-bordered" id="table" >
            <thead>
            <tr>
                           <th width="50">Sn.</th>
                                <th width="205">Image</th>
                                <th width="230">Heading</th>
                                <th width="250">Description</th>
                                <th width="110">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        {{ $count = 1}}
                            @foreach($content as $row)
                    <tr><td>{{ $count ++ }}</td>

    <td class="avatar">
        <div class="round-img"><img class="rounded-circle img-thumbnail" src="{{asset($row->image)}}" alt="" width="50%"></div>
    </td>

        <td>{{$row['heading']}}</td>
        <td>{{$row['description']}}</td>
        <td>
            <a href={{ route('editcontent',$row->id) }}><i class="fa fa-edit" title="Edit Content"></i></a>
            <a href={{ route('deletecontent',$row->id) }}><i class="fa fa-trash-o" title="Delete Content"></i></a>
    </td>
    </tr>
@endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div><!-- .animated -->
</div>
@section('custom_script')
<div style="margin-left: 100px">
<script type = "text/javascript">
$(document).ready(function() {
  $('#table').DataTable();
} );
</script>
</div>
@endsection
@endsection
