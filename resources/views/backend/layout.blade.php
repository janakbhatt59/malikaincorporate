
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta name="description" content="@yield('page_description')" />
  <meta name="author" content="Malika incorporate pvt ltd"/>
  <title>@yield('page_title')</title>
  @include('backend.styleBackend')
  @yield('custom_css')
</head>
<body>
	<div class="row">
       @include('backend.backendLeft')
	</div>
    <div id="right-panel" class="right-panel">
        @include('backend.header')<!-- header -->

        <div class="content">
                    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
                @endif
            <div class="animated fadeIn">
                @yield('content')
            </div>
        </div>
        <div class="clearfix"></div>
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; MALIKA INCO
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">Colorlib</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
   @include('backend.backendScript')
  @yield('custom_script')
</body>
</html>
