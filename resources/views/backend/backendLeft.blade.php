 <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{ route('adminDashboard') }}">
                            <i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                     <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-table"></i>Contents</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="{{ route('indexlContent') }}">View Content Lists</a></li>
                            <li><i class="fa fa-table"></i><a href="{{ route('createlcontent') }}">Create Content</a></li>
                        </ul>
                    </li>
                     <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-table"></i>Top Nav Contents</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="{{ route('indexNavOpen') }}">View Content Lists</a></li>
                            <li><i class="fa fa-table"></i><a href="{{ route('createNavOpen') }}">Create Content</a></li>
                        </ul>
                    </li>


                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="menu-icon fa fa-th"></i>Products</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-th"></i><a href="{{ route('viewcardpinter') }}">Card Printer(ZEBRA)</a></li>
                            <li><i class="fa fa-th"></i><a href="{{ route('viewmobile') }}">Mobile</a></li>
                            <li><i class="fa fa-th"></i><a href="{{ route('viewbarcodescanner') }}">Barcode Scanner</a></li>
                            <li><i class="fa fa-th"></i><a href="{{route('viewindustrialLevelPrinter')}}">Industrial Level Printer</a></li>
                            <li><i class="fa fa-th"></i><a href="{{route('viewmobtickprinter')}}">Mobile Printer/Ticket Printer</a></li>
                            <li><i class="fa fa-th"></i><a href="{{route('viewbarcodeprinter')}}">Barcode Level Printer</a></li>
                            <li><i class="fa fa-th"></i><a href="{{route('viewhospital')}}">Hospital Wristband Printer</a></li>
                            <li><i class="fa fa-th"></i><a href="{{route('viewbio')}}">Biometric Devices(Mantra)</a></li>
                            <li><i class="fa fa-th"></i><a href="{{route('viewfinger')}}">BIO Mini finger Print(SUPREMA)</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-table"></i>Blog</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="{{ route('viewBlog') }}">View Blog Lists</a></li>
                            <li><i class="fa fa-table"></i><a href="{{ route('createBlog') }}">Create Blog</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-table"></i>Slider</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="{{ route('sliderindex') }}">Slider List</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{ route('downloadIndex') }}"  aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-table"></i>Downloads</a>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-table"></i>Products</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="{{ route('sisadmin.products.index') }}">Product List</a></li>
                            <li><i class="fa fa-table"></i><a href="{{ route('sisadmin.brand.index') }}"> Brand List</a></li>
                            <li><i class="fa fa-table"></i><a href="{{ route('sisadmin.categories.index') }}">Categories List</a></li>
                            <li><i class="fa fa-table"></i><a href="{{ route('sisadmin.subcategory.index') }}">SubCategories List</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Gallery</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('addPhoto') }}">Add Photo</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewPhoto') }}">List Photo</a></li>
                        </ul>
                    </li>  <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>About Us</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewabout') }}">About Us</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewsector') }}">Sectors We Serve</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Company Details</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('adddetails') }}">Add Details</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewdetails') }}">List Details</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Printing Supplier</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ route('addsupplier') }}">Add Supplier</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewsupplier') }}">List Supplier</a></li>
                            </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Ribbons</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewzxp3ribbon') }}">ZXP 3 Ribbons</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewzxp7ribbon') }}">ZXP 7 Ribbons</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewzxp9ribbon') }}">ZXP 9 Ribbons</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Our Team</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ route('addteam') }}">Add Team</a></li>
                                <li><i class="menu-icon fa fa-th"></i><a href="{{ route('viewteam') }}">List Team</a></li>
                            </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </aside>
