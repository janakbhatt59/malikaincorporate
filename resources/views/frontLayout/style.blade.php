<link rel="icon" href="img/titleLogo.png" type="image/png">
<link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/fontawesome/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/themify-icons/themify-icons.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/linericon/style.css') }} ">
<link rel="stylesheet" href="{{ asset('vendors/owl-carousel/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/owl-carousel/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/Magnific-Popup/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

