<section class="header-top d-none d-sm-block " style="z-index:1000">
    <div class="container col-12">
      <div class="d-sm-flex justify-content-between">
        <ul class="header-top__info mb-2 mb-sm-0">
          <li><a href="tel:+9774154316"><span class="align-middle"><i class="ti-mobile"></i></span>+977 415 4316 </a></li>
          <li><a href="mailto:info@malikaincorporate.com"><span class="align-middle"><i class="ti-email"></i></span>info@malikaincorporate.com</a></li>
        </ul>
        <ul class="header-top__social">
          <li><a href="#"><i class="ti-facebook"></i></a></li>
          <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
          <li><a href="#"><i class="ti-instagram"></i></a></li>
          <li><a href="#"><i class="ti-skype"></i></a></li>
        </ul>
      </div>
    </div>
  </section>
<header class="header_area">
  <div class="main_menu" style="border-bottom:1px solid #ee1d23">
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container box_1620 col-11">
        <a class="navbar-brand logo_h" href="{{route('homePage')}}"><img src="{{URL::asset('/img/logo.png')}}   " alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
         <ul class="nav navbar-nav menu_nav justify-content-end">
           <li class="nav-item submenu dropdown">
             <a href="{{route('topNavOpen', ['id'=>1])}}" class="nav-link dropdown-toggle"  role="button"
             aria-haspopup="true"aria-expanded="false">Solution</a>
             <ul class="dropdown-menu nav navbar-nav menu_nav">
             <li class="nav-item submenu dropdown subsubMenu">
               <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false">Governmental Solution</a>
              <ul id="subsubMenuVis1" class="subsubMenuVis1 dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('smartCity', ['id'=>2]) }}">Municipality(Smart City Project)</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('smartCity', ['id'=>3]) }}">Airlines</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('smartCity', ['id'=>4]) }}">Nepal Electricity</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('smartCity', ['id'=>5]) }}">Provident Fund/EPS</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('smartCity', ['id'=>6]) }}">Poverty Card</a></li>
               </ul>
          </li>
             <li class="nav-item submenu dropdown subsubMenu"><a href="#solution" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="false"
                aria-expanded="false">Business Solution</a>
                 <ul id="subsubMenuVis1" class="dropdown-menu nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="rightPostCol.php">Bus Ticketing</a></li>
                <li class="nav-item"><a class="nav-link1" href="#">Banking</a></li>
                <li class="nav-item"><a class="nav-link1" href="#">Insurance</a></li>
                <li class="nav-item"><a class="nav-link1" href="#">Retail/Supermarket</a></li>
                <li class="nav-item"><a class="nav-link1" href="#">Manufacturing</a></li>
                <li class="nav-item"><a class="nav-link1" href="#">Transport</a></li>
                <li class="nav-item"><a class="nav-link1" href="#">Hospitality/Hotel</a></li>
                <li class="nav-item"><a class="nav-link1" href="#">Drinking Water Office</a></li>
                <li class="nav-item"><a class="nav-link1" href="#">E-commerce</a></li>
              </ul>
              </li>
           <li class="nav-item"><a class="nav-link" href="#">ID and access control cards</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Government-issued driver"s license</a></li>
          <li class="nav-item"><a class="nav-link" href="#">National ID and voter registration cards</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Smart cards in travel, gaming and entertainment</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Surveillance camera & Access control system</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Healthcare</a></li>
          <li class="nav-item"><a class="nav-link" href="#">School/Colleges</a></li>


              </ul>
             <li class="nav-item submenu dropdown1">
              <a href="{{route('topNavOpen', ['id'=>2])}}" class="nav-link dropdown-toggle"  data-toggle="dropdown1" role="button" aria-haspopup="true"
                aria-expanded="false">Product</a>
              <ul class="dropdown-menu nav navbar-nav menu_nav">
            <li class="nav-item submenu dropdown subsubMenu">
               <a href="{{ route('biometrics') }}" class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"
               aria-expanded="false">Biometric Devices(Mantra)</a>
                <ul id="subsubMenuVis1" class=" dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>1])  }}">Bio face MSD 1K</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>2])  }}">Bio face MSD 150</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>4])  }}">M BIO G3</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>5])  }}">M BIO M18</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>6])  }}">M BIO S 18</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>7])  }}">M BIO 5 N</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>8])  }}">MFACE FA 300</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>9])  }}">MFACE FA 200</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>10])  }}">MBIO ST1</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>11]) }}">MBIO ST2</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>12]) }}">SEM-01</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>13]) }}">BIOGPRS-01</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>14]) }}">BIOWEB RC1</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>15]) }}">BIOWEB B3</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('biometric',['id'=>16]) }}">MGS100</a></li>
               </ul>
         </li>
 <li class="nav-item submenu dropdown subsubMenu">
               <a href="{{ route('productsLists') }}" class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"
               aria-expanded="false">Card Printer(ZEBRA)</a>
              <ul id="subsubMenuVis1" class=" dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('productList', ['id'=>1]) }}">ZXP 3 card printers</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('productList', ['id'=>2]) }}">ZXP 7 card printers</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('productList', ['id'=>3]) }}">ZXP 9 ID Card Printers</a></li>
               </ul>
         </li>
 <li class="nav-item submenu dropdown subsubMenu">
               <a href="{{ route('barcodeScanners') }}" class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"
               aria-expanded="false">Barcode Scanner</a>
              <ul id="subsubMenuVis1" class=" dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('barcodeScanner', ['id'=>1]) }}">DS2208 2D</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('barcodeScanner', ['id'=>2]) }}">LS2208 1D</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('barcodeScanner', ['id'=>3]) }}">DS2278 wireless</a></li>
               </ul>
         </li>
 <li class="nav-item submenu dropdown subsubMenu">
               <a href="{{ route('IPrinters') }}" class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"
               aria-expanded="false">Industrial Level Printer</a>
              <ul id="subsubMenuVis1" class=" dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('IPrinter', ['id'=>1]) }}">ZT410</a></li>
               </ul>
         </li>
 <li class="nav-item submenu dropdown subsubMenu">
               <a href="{{ route('mobPrinters') }}" class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"
               aria-expanded="false">Mobile Printer/Ticket Printer</a>
              <ul id="subsubMenuVis1" class=" dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('mobPrinter', ['id'=>1]) }}">iMZ320</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('mobPrinter', ['id'=>2]) }}">EZ320</a></li>
               </ul>
         </li>
 <li class="nav-item submenu dropdown subsubMenu">
               <a href="{{ route('mobiles')}}" class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"
               aria-expanded="false">Mobile</a>
              <ul id="subsubMenuVis1" class=" dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('mobile', ['id'=>1]) }}">TC 20</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('mobile', ['id'=>2]) }}">TC 25</a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('mobile', ['id'=>3]) }}">TC 56</a></li>
               </ul>
         </li>
 <li class="nav-item submenu dropdown subsubMenu">
               <a href="{{ route('barcodeLevelPrinters') }}" class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"
               aria-expanded="false">Barcode Level Printer</a>
              <ul id="subsubMenuVis1" class=" dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('barcodeLevelPrinter', ['id'=>1]) }}">GT 800 </a></li>
                <li class="nav-item"><a class="nav-link1" href="{{ route('barcodeLevelPrinter', ['id'=>2]) }}">GC 420</a></li>
               </ul>
         </li>
 <li class="nav-item submenu dropdown subsubMenu">
               <a href="{{ route('hospitalWristbandPrinters') }}" class="nav-link dropdown-toggle" data-toggle="" role="button" aria-haspopup="true"
               aria-expanded="false">Hospital Wristband Printer</a>
              <ul id="subsubMenuVis1" class=" dropdown-menu  nav navbar-nav menu_nav ">
                <li class="nav-item"><a class="nav-link1" href="{{ route('hospitalWristbandPrinter', ['id'=>1]) }}">HC 100</a></li>
               </ul>
              </li>

                <li class="nav-item"><a class="nav-link" href="{{ route('bioMiniFingerPrint') }}">BIO Mini Finger Print(SUPREMA)</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('printingSupplies') }}">Printing Supplies</a></li>
              </ul>
            </li>
            <li class="nav-item"><a class="nav-link" href="{{route('gallery')}}">Gallery</a></li>
            <li class="nav-item submenu dropdown">
              <a href="{{route('topNavOpen', ['id'=>4])}}" class="nav-link dropdown-toggle"  role="button" aria-haspopup="true"
                aria-expanded="false">About Us</a>
              <ul class="dropdown-menu nav navbar-nav menu_nav">
              <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">About Malika</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('sector')}}">Sectors We Serve</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ route('ourpartners') }}">Our Partners</a></li>
              </ul>
            </li>
           <li class="nav-item"><a class="nav-link" href="{{route('contactUs')}}" >Contact Us</a></li>
           <li class="nav-item"><a class="nav-link" href="{{route('downloads')}}" >Downloads</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>

</header>
