<div class="container">

      <div class="row">
        <div class="col-lg-3  col-md-6 col-sm-6">
          <div class="single-footer-widget">
            <h6>About Company</h6>
            <p>
              Malika Incorporate Pvt.Ltd. was found in 2018 with an objective to serve its Corporate Clients with various IT Solutions. Since then it has been evolving with new ideas and concepts in Information Technology.
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="single-footer-widget">
            <h6>Navigation Links</h6>
            <div class="row">
              <div class="col">
                <ul>
                  <li><a href="{{route('homePage')}}">Home</a></li>
                  <li><a href="{{route('topNavOpen', ['id'=>1])}}">Solution</a></li>
                  <li><a href="{{route('topNavOpen', ['id'=>2])}}">Product</a></li>
                  <li><a href="{{route('topNavOpen', ['id'=>4])}}">About Us</a></li>
                </ul>
              </div>
              <div class="col">
                <ul>
                  <li><a href="{{route('gallery')}}">Gallery</a></li>
                  <li><a href="{{route('gallery')}}">Contact Us</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- ================ contact section start ================= -->
<div class="col-md-3 col-lg-3 mb-4 mb-md-0">
          <div class="single-footer-widget">
            <h6>Location</h6>
            <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <p style="font-size: 12px">ओम् शान्ति मार्ग, Kathmandu 44600, Nepal</p>
              <p style="margin-top: -12px">Koteshwor, Om Shanti Marg</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-headphone"></i></span>
            <div class="media-body">


              <p style="font-size: 12px"><a href="tel:454545654">+977 1-4154316</a></p>
              <p style="margin-top: -12px">Sun to Fri 9:30am to 5:30pm</p>
            </div>
          </div>
        </div></div>
    <div class="col-lg-3  col-md-12 col-sm-3">
        <div class="single-footer-widget">
  <section class="">
    <div class="container">
      <div class="d-none d-sm-block pb-4">
        <div id="map" style="height: 220px;"></div>
        <script>
          function initMap() {
            var uluru = {lat: 27.714512, lng: 85.324226};
            var grayStyles = [
              {
                featureType: "all",
                stylers: [
                  { saturation: -90 },
                  { lightness: 50 }
                ]
              },
              {elementType: 'labels.text.fill', stylers: [{color: '#A3A3A3'}]}
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: 27.714512, lng: 85.324226},
              zoom: 9,
              styles: grayStyles,
              scrollwheel:  false
            });
          }

        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBU7hmguA0viLT8kKTVFQFb6G6LiT31v8c&callback=initMap"></script>

      </div>

       </div>
       </div>
        <!-- ================ contact section end ================= -->

      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row align-items-center">
          <p class="col-lg-8 col-sm-12 footer-text m-0 text-center text-lg-left">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Malika Incorporate Pvt. Ltd.
</p>
          <div class="col-lg-4 col-sm-12 footer-social text-center text-lg-right">
            <a href="#"><i class="fab fa-facebook-f"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-dribbble"></i></a>
            <a href="#"><i class="fab fa-behance"></i></a>
          </div>
        </div>
      </div>
    </div>
