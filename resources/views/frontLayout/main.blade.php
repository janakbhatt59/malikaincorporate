<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta name="description" content="@yield('page_description')" />
  <meta name="author" content="Malika incorporate pvt ltd"/>
  <title>@yield('page_title')</title>
  @include('frontLayout.style')
  @yield('custom_css')
</head>
<body>
    @include('frontLayout.header')

         @yield('content')

  <footer class="footer-area container-flex">
    @include('frontLayout.footer')
  </footer>
  <!-- ================ End footer Area ================= -->

  <script src="{{ asset('vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('vendors/Magnific-Popup/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('js/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('js/mail-script.js') }}"></script>
  <script src="{{ asset('js/main.js') }}"></script>
  <script src="{{ asset('js/slider.js') }}"></script>
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('js/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('js/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('js/js/aos.js')}}"></script>
  <script src="{{asset('js/js/main.js')}}"></script>
  

 @yield('custom_script')
</body>
</html>
