						<aside class="single_sidebar_widget search_widget">
								<div class="input-group">
										<input type="text" class="form-control" placeholder="Search Posts">
										<span class="input-group-btn">
												<button class="btn btn-default" type="button">
														<i class="lnr lnr-magnifier"></i>
												</button>
										</span>
								</div>
								<!-- /input-group -->
								<div class="br"></div>
						</aside>
						<aside class="single_sidebar_widget author_widget">
								<img class="author_img rounded-circle" src="jack.jpg" alt="Photo">
								<h4>Jackobian King</h4>
								<p>Senior writer</p>
								<div class="social_icon">
          <a href="#">
              <i class="fab fa-facebook-f"></i>
          </a>
          <a href="#">
              <i class="fab fa-twitter"></i>
          </a>
          <a href="#">
              <i class="fab fa-github"></i>
          </a>
          <a href="#">
            <i class="fab fa-behance"></i>
          </a>
      </div>

								<div class="br"></div>
						</aside>
						<aside class="single_sidebar_widget popular_post_widget">
								<h3 class="widget_title">Latest Posts</h3>
								<div class="media post_item">
										<img src="img/blog/popular-post/post1.jpg" alt="post">
										<div class="media-body">
												<a href="rightPostCol.php">
														<h3>Space The Final Frontier</h3>
												</a>
												<p>02 Hours ago</p>
										</div>
								</div>
								<div class="media post_item">
										<img src="img/blog/popular-post/post2.jpg" alt="post">
										<div class="media-body">
												<a href="rightPostCol.php">
														<h3>The Amazing Hubble</h3>
												</a>
												<p>02 Hours ago</p>
										</div>
								</div>
								<div class="media post_item">
										<img src="img/blog/popular-post/post3.jpg" alt="post">
										<div class="media-body">
												<a href="rightPostCol.php">
														<h3>Astronomy Or Astrology</h3>
												</a>
												<p>03 Hours ago</p>
										</div>
								</div>
								<div class="media post_item">
										<img src="img/blog/popular-post/post4.jpg" alt="post">
										<div class="media-body">
												<a href="rightPostCol.php">
														<h3>Asteroids telescope</h3>
												</a>
												<p>01 Hours ago</p>
										</div>
								</div>
								<div class="br"></div>
                        </aside>
                        <!-- Comment section start -->
                        <aside class="single_sidebar_widget popular_post_widget">
								<h3 class="widget_title">Latest Comments</h3>
								<div class="media post_item">
										<img src="img/blog/popular-post/post1.jpg" alt="post">
										<div class="media-body">
												<a href="rightPostCol.php">
														<h3>Space The Final Frontier</h3>
												</a>
												<p>02 Hours ago</p>
										</div>
								</div>
								<div class="media post_item">
										<img src="img/blog/popular-post/post2.jpg" alt="post">
										<div class="media-body">
												<a href="rightPostCol.php">
														<h3>The Amazing Hubble</h3>
												</a>
												<p>02 Hours ago</p>
										</div>
								</div>
								<div class="media post_item">
										<img src="img/blog/popular-post/post3.jpg" alt="post">
										<div class="media-body">
												<a href="rightPostCol.php">
														<h3>Astronomy Or Astrology</h3>
												</a>
												<p>03 Hours ago</p>
										</div>
								</div>
								<div class="media post_item">
										<img src="img/blog/popular-post/post4.jpg" alt="post">
										<div class="media-body">
												<a href="rightPostCol.php">
														<h3>Asteroids telescope</h3>
												</a>
												<p>01 Hours ago</p>
										</div>
								</div>
								<div class="br"></div>
                        </aside>
                         <aside class="single_sidebar_widget ads_widget">
								<a href="#">
										<img class="img-fluid" src="img/blog/add.jpg" alt="">
								</a>
								<div class="br"></div>
                         </aside> -->
                         <!--============ ad area close ==========-->
                         <!--============ Post Catgories start ==========-->
						<aside class="single_sidebar_widget post_category_widget">
								<h4 class="widget_title">Post Catgories</h4>
								<ul class="list cat-list">
										<li>
												<a href="#" class="d-flex justify-content-between">
														<p>ID and access control cards</p>
												</a>
										</li>
										<li>
												<a href="#" class="d-flex justify-content-between">
														<p>Team</p>
												</a>
                                        </li>
                                        <li>
												<a href="#" class="d-flex justify-content-between">
														<p>Uncategorized</p>
												</a>
										</li>
								</ul>
								<div class="br"></div>
                        </aside> 
                         <!--============ Catgories end ==========-->
                        
						<aside class="single-sidebar-widget newsletter_widget">
								<h4 class="widget_title">Newsletter</h4>
								<p>
										Here, I focus on a range of items and features that we use in life without giving them a second thought.
								</p>
								<div class="form-group d-flex flex-row">
										<div class="input-group">
												<div class="input-group-prepend">
														<div class="input-group-text">
																<i class="fa fa-envelope" aria-hidden="true"></i>
														</div>
												</div>
												<input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email address" onfocus="this.placeholder = ''"
														onblur="this.placeholder = 'Enter email'">
										</div>
										<a href="#" class="bbtns">Subcribe</a>
								</div>
								<p class="text-bottom">You can unsubscribe at any time</p>
								<div class="br"></div>
						</aside>