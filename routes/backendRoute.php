<?php

    Route::prefix('admin')->group(function () {
	Route::get('/dashboard', 'admin\adminHomeController@dashboard')->name('adminDashboard');
	Route::get('/company', 'admin\company\companyDetailsController@index')->name('companyList');

    Route::get('/blog','admin\company\blogController@index')->name('viewBlog');
    Route::get('/blog/create','admin\company\blogController@create')->name('createBlog');
    Route::post('/store/Blog','admin\company\blogController@store')->name('storeBlog');

    Route::get('/edit/{id}/blog','admin\company\blogController@edit')->name('editBlog');
    Route::post('/update/{id}/blog','admin\company\blogController@update')->name('updateBlog');
    Route::get('/delete/{id}/blog','admin\company\blogController@destroy')->name('deleteBlog');

    // lcontent
    Route::get('/lcontent','admin\company\lcontentController@index')->name('indexlContent');
    Route::get('/lcontent/create','admin\company\lcontentController@create')->name('createlcontent');
    Route::post('/lcontent/store','admin\company\lcontentController@store')->name('storelContent');

    Route::get('/edit/content/{id}','admin\company\lcontentController@edit')->name('editcontent');
    Route::post('/update/content/{id}','admin\company\lcontentController@update')->name('updatecontent');
    Route::get('/delete/content/{id}','admin\company\lcontentController@destroy')->name('deletecontent');


    // navOpen
    Route::get('/navOpen','admin\company\navOpenController@index')->name('indexNavOpen');
    Route::get('/navOpen/create','admin\company\navOpenController@create')->name('createNavOpen');
    Route::post('/navOpen/store','admin\company\navOpenController@store')->name('storeNavOpen');


    Route::get('/edit/{id}/content','admin\company\navOpenController@edit')->name('editNavOpen');
    Route::post('/update/{id}/content','admin\company\navOpenController@update')->name('updateNavOpen');
    Route::get('/delete/{id}/content','admin\company\navOpenController@destroy')->name('deleteNavOpen');


    //Gallery
    Route::get('/gallery','admin\company\galleryController@index')->name('viewPhoto');
    Route::get('/gallery/addphoto','admin\company\galleryController@create')->name('addPhoto');
    Route::post('gallery/store','admin\company\galleryController@store')->name('storePhoto');

    Route::get('gallery/{id}/edit','admin\company\galleryController@edit')->name('editPhoto');
    Route::post('gallery/{id}/update','admin\company\galleryController@update')->name('updatePhoto');
    Route::get('gallery/{id}/delete','admin\company\galleryController@destroy')->name('deletePhoto');

    //companyDetails
    Route::get('/details','admin\company\companyDetails\detailsController@index')->name('viewdetails');
    Route::get('/details/create','admin\company\companyDetails\detailsController@create')->name('adddetails');
    Route::post('details/store','admin\company\companyDetails\detailsController@store')->name('storedetails');

    Route::get('details/{id}/edit','admin\company\companyDetails\detailsController@edit')->name('editdetails');
    Route::post('details/{id}/update','admin\company\companyDetails\detailsController@update')->name('updatedetails');
    Route::get('details/{id}/delete','admin\company\companyDetails\detailsController@destroy')->name('deletedetails');

    // printingSupplier
    Route::get('/supplier','admin\company\printingController@index')->name('viewsupplier');
    Route::get('/supplier/create','admin\company\printingController@create')->name('addsupplier');
    Route::post('supplier/store','admin\company\printingController@store')->name('storesupplier');

    Route::get('supplier/{id}/edit','admin\company\printingController@edit')->name('editsupplier');
    Route::post('supplier/{id}/update','admin\company\printingController@update')->name('updatesupplier');
    Route::get('supplier/{id}/delete','admin\company\printingController@destroy')->name('deletesupplier');

    //CardPrinter
    Route::get('/cardprinter','admin\company\Products\cardprinterController@index')->name('viewcardpinter');
    Route::get('/cardprinter/addproduct','admin\company\Products\cardprinterController@create')->name('addcardpinter');
    Route::post('cardprinter/store','admin\company\Products\cardprinterController@store')->name('storecardpinter');

    Route::get('cardprinter/{id}/edit','admin\company\Products\cardprinterController@edit')->name('editcardpinter');
    Route::post('cardprinter/{id}/update','admin\company\Products\cardprinterController@update')->name('updatecardpinter');
    Route::get('cardprinter/{id}/delete','admin\company\Products\cardprinterController@destroy')->name('deletecardpinter');


    //Mobile
    Route::get('/mobile','admin\company\Products\mobileController@index')->name('viewmobile');
    Route::get('/mobile/addproduct','admin\company\Products\mobileController@create')->name('addmobile');
    Route::post('mobile/store','admin\company\Products\mobileController@store')->name('storemobile');

    Route::get('mobile/{id}/edit','admin\company\Products\mobileController@edit')->name('editmobile');
    Route::post('mobile/{id}/update','admin\company\Products\mobileController@update')->name('updatemobile');
    Route::get('mobile/{id}/delete','admin\company\Products\mobileController@destroy')->name('deletemobile');


    //BarcodeScanner
    Route::get('/barcodescanner','admin\company\Products\barcodeScannerController@index')->name('viewbarcodescanner');
    Route::get('/barcodescanner/addproduct','admin\company\Products\barcodeScannerController@create')->name('addbarcodescanner');
    Route::post('barcodescanner/store','admin\company\Products\barcodeScannerController@store')->name('storebarcodescanner');

    Route::get('barcodescanner/{id}/edit','admin\company\Products\barcodeScannerController@edit')->name('editbarcodescanner');
    Route::post('barcodescanner/{id}/update','admin\company\Products\barcodeScannerController@update')->name('updatebarcodescanner');
    Route::get('barcodescanner/{id}/delete','admin\company\Products\barcodeScannerController@destroy')->name('deletebarcodescanner');

    //bio-miniFingerPrint
    Route::get('/finger','admin\company\Products\fingerController@index')->name('viewfinger');
    Route::get('/finger/addproduct','admin\company\Products\fingerController@create')->name('addfinger');
    Route::post('/finger/store','admin\company\Products\fingerController@store')->name('storefinger');

    Route::get('finger/{id}/edit','admin\company\Products\fingerController@edit')->name('editfinger');
    Route::post('finger/{id}/update','admin\company\Products\fingerController@update')->name('updatefinger');
    Route::get('finger/{id}/delete','admin\company\Products\fingerController@destroy')->name('deletefinger');

     //bioMetricDevice
     Route::get('/bio','admin\company\Products\bioController@index')->name('viewbio');
     Route::get('/bio/addproduct','admin\company\Products\bioController@create')->name('addbio');
     Route::post('/bio/store','admin\company\Products\bioController@store')->name('storebio');

     Route::get('bio/{id}/edit','admin\company\Products\bioController@edit')->name('editbio');
     Route::post('bio/{id}/update','admin\company\Products\bioController@update')->name('updatebio');
     Route::get('bio/{id}/delete','admin\company\Products\bioController@destroy')->name('deletebio');

     //hospitalPrinter
     Route::get('/hospital','admin\company\Products\hospitalController@index')->name('viewhospital');
     Route::get('/hospital/addproduct','admin\company\Products\hospitalController@create')->name('addhospital');
     Route::post('hospital/store','admin\company\Products\hospitalController@store')->name('storehospital');

     Route::get('hospital/{id}/edit','admin\company\Products\hospitalController@edit')->name('edithospital');
     Route::post('hospital/{id}/update','admin\company\Products\hospitalController@update')->name('updatehospital');
     Route::get('hospital/{id}/delete','admin\company\Products\hospitalController@destroy')->name('deletehospital');

    //IndustrialLevelPrinter
    Route::get('/industriallevelprinter','admin\company\Products\industrialLevelPrinterController@index')->name('viewindustrialLevelPrinter');
    Route::get('/industriallevelprinter/addproduct','admin\company\Products\industrialLevelPrinterController@create')->name('addindustrialLevelPrinter');
    Route::post('industriallevelprinter/store','admin\company\Products\industrialLevelPrinterController@store')->name('storeindustrialLevelPrinter');

    Route::get('industriallevelprinter/{id}/edit','admin\company\Products\industrialLevelPrinterController@edit')->name('editindustrialLevelPrinter');
    Route::post('industriallevelprinter/{id}/update','admin\company\Products\industrialLevelPrinterController@update')->name('updateindustrialLevelPrinter');
    Route::get('industriallevelprinter/{id}/delete','admin\company\Products\industrialLevelPrinterController@destroy')->name('deleteindustrialLevelPrinter');

    //Mobile/TicketPrinter
    Route::get('/mobtickprinter','admin\company\Products\mobileTicketPrinterController@index')->name('viewmobtickprinter');
    Route::get('/mobtickprinter/addproduct','admin\company\Products\mobileTicketPrinterController@create')->name('addmobtickprinter');
    Route::post('mobtickprinter/store','admin\company\Products\mobileTicketPrinterController@store')->name('storemobtickprinter');

    Route::get('mobtickprinter/{id}/edit','admin\company\Products\mobileTicketPrinterController@edit')->name('editmobtickprinter');
    Route::post('mobtickprinter/{id}/update','admin\company\Products\mobileTicketPrinterController@update')->name('updatemobtickprinter');
    Route::get('mobtickprinter/{id}/delete','admin\company\Products\mobileTicketPrinterController@destroy')->name('deletemobtickprinter');


     //BarcodeLevelPrinter
     Route::get('/barcode','admin\company\Products\barcodePrinterController@index')->name('viewbarcodeprinter');
     Route::get('/barcode/addproduct','admin\company\Products\barcodePrinterController@create')->name('addbarcodeprinter');
     Route::post('barcode/store','admin\company\Products\barcodePrinterController@store')->name('storebarcodeprinter');

     Route::get('barcode/{id}/edit','admin\company\Products\barcodePrinterController@edit')->name('editbarcodeprinter');
     Route::post('barcode/{id}/update','admin\company\Products\barcodePrinterController@update')->name('updatebarcodeprinter');
     Route::get('barcode/{id}/delete','admin\company\Products\barcodePrinterController@destroy')->name('deletebarcodeprinter');

    //Barcode Level Printer
    Route::get('/aboutus','admin\company\aboutController@index')->name('viewabout');
    Route::get('/aboutus/add','admin\company\aboutController@create')->name('addabout');
    Route::post('aboutus/store','admin\company\aboutController@store')->name('storeabout');

    Route::get('aboutus/{id}/edit','admin\company\aboutController@edit')->name('editabout');
    Route::post('aboutus/{id}/update','admin\company\aboutController@update')->name('updateabout');
    Route::get('aboutus/{id}/delete','admin\company\aboutController@destroy')->name('deleteabout');

    //Sector
    Route::get('/sector','admin\company\sectorsController@index')->name('viewsector');
    Route::get('/sector/add','admin\company\sectorsController@create')->name('addsector');
    Route::post('sector/store','admin\company\sectorsController@store')->name('storesector');

    Route::get('sector/{id}/edit','admin\company\sectorsController@edit')->name('editsector');
    Route::post('sector/{id}/update','admin\company\sectorsController@update')->name('updatesector');
    Route::get('sector/{id}/delete','admin\company\sectorsController@destroy')->name('deletesector');

    //ZXP3 Ribbons
    Route::get('/zxp3ribbon','admin\company\Ribbons\zxp3RibbonController@index')->name('viewzxp3ribbon');
    Route::get('/zxp3ribbon/add','admin\company\Ribbons\zxp3RibbonController@create')->name('addzxp3ribbon');
    Route::post('zxp3ribbon/store','admin\company\Ribbons\zxp3RibbonController@store')->name('storezxp3ribbon');
    Route::get('zxp3ribbon/{id}/edit','admin\company\Ribbons\zxp3RibbonController@edit')->name('editzxp3ribbon');
    Route::post('zxp3ribbon/{id}/update','admin\company\Ribbons\zxp3RibbonController@update')->name('updatezxp3ribbon');
    Route::get('zxp3ribbon/{id}/delete','admin\company\Ribbons\zxp3RibbonController@destroy')->name('deletezxp3ribbon');


    //ZXP7 Ribbons
    Route::get('/zxp7ribbon','admin\company\Ribbons\zxp7RibbonController@index')->name('viewzxp7ribbon');
    Route::get('/zxp7ribbon/add','admin\company\Ribbons\zxp7RibbonController@create')->name('addzxp7ribbon');
    Route::post('zxp7ribbon/store','admin\company\Ribbons\zxp7RibbonController@store')->name('storezxp7ribbon');
    Route::get('zxp7ribbon/{id}/edit','admin\company\Ribbons\zxp7RibbonController@edit')->name('editzxp7ribbon');
    Route::post('zxp7ribbon/{id}/update','admin\company\Ribbons\zxp7RibbonController@update')->name('updatezxp7ribbon');
    Route::get('zxp7ribbon/{id}/delete','admin\company\Ribbons\zxp7RibbonController@destroy')->name('deletezxp7ribbon');

    //ZXP9 Ribbons
    Route::get('/zxp9ribbon','admin\company\Ribbons\zxp9RibbonController@index')->name('viewzxp9ribbon');
    Route::get('/zxp9ribbon/add','admin\company\Ribbons\zxp9RibbonController@create')->name('addzxp9ribbon');
    Route::post('zxp9ribbon/store','admin\company\Ribbons\zxp9RibbonController@store')->name('storezxp9ribbon');
    Route::get('zxp9ribbon/{id}/edit','admin\company\Ribbons\zxp9RibbonController@edit')->name('editzxp9ribbon');
    Route::post('zxp9ribbon/{id}/update','admin\company\Ribbons\zxp9RibbonController@update')->name('updatezxp9ribbon');
    Route::get('zxp9ribbon/{id}/delete','admin\company\Ribbons\zxp9RibbonController@destroy')->name('deletezxp9ribbon');

    //OurTeam
    Route::get('/team','admin\company\ourteam\teamContoller@index')->name('viewteam');
    Route::get('/team/add','admin\company\ourteam\teamContoller@create')->name('addteam');
    Route::post('team/store','admin\company\ourteam\teamContoller@store')->name('storeteam');

    Route::get('team/{id}/edit','admin\company\ourteam\teamContoller@edit')->name('editteam');
    Route::post('team/{id}/update','admin\company\ourteam\teamContoller@update')->name('updateteam');
    Route::get('team/{id}/delete','admin\company\ourteam\teamContoller@destroy')->name('deleteteam');


// NEW
    //Slider
    Route::get('/slider/index', 'admin\company\slider\sliderController@index')->name('sliderindex');
    Route::post('/slider/save', 'admin\company\slider\sliderController@store')->name('slidersave');

    Route::get('/slider/{id}/edit/', 'admin\company\slider\sliderController@edit')->name('slideredit');
    Route::post('slider/{id}/update', 'admin\company\slider\sliderController@update')->name('sliderupdate');
    Route::get('/slider/delete/{id}', 'admin\company\slider\sliderController@destroy')->name('sliderdelete');


   /*product*/
   Route::get('/products/index', 'admin\company\product\productController@index')->name('sisadmin.products.index');

   Route::get('/products/create', 'admin\company\product\productController@create')->name('sisadmin.products.create');
   Route::post('/products/store', 'admin\company\product\productController@store')->name('sisadmin.products.store');

   Route::get('/products/{products}/edit', 'admin\company\product\productController@edit')->name('sisadmin.products.edit');

   Route::get('/products/{products}', 'admin\company\product\productController@show')->name('sisadmin.products.show');

   Route::post('/products/{products}', 'admin\company\product\productController@update')->name('sisadmin.products.update');

   Route::get('/products/delete/{id}', 'admin\company\product\productController@destroy')->name('sisadmin.products.delete');
   /*product status*/
   Route::get('product/changeProductStatus', 'admin\company\product\productController@changeProductStatus')->name('sisadmin.product.changeProductStatus');

   Route::get('product/makeFeaturedProduct', 'admin\company\product\productController@makeFeaturedProduct')->name('sisadmin.product.makeFeaturedProduct');


   Route::get('product/appreance', 'admin\company\product\productController@makeAppreanceProduct')->name('sisadmin.product.appreance');

   /*--------------subscribe comment --------------------------------*/
   Route::get('product/admin/supportForm', 'admin\company\product\productController@supportForm')->name('sisadmin.product.adminSupportForm');

   Route::get('/product/productComment', 'admin\company\product\productController@productComment')->name('sisadmin.product.productComment');

   Route::get('comment/delete/{id}', 'admin\company\product\productController@commentDelete')->name('sisadmin.comment.delete');

   Route::get('admin/subscribe', 'admin\company\product\productController@userSubscribe')->name('sisadmin.admin.subscribe');


     /*-----------------------brand--------------------------------*/
     Route::get('/brand/index', 'admin\company\brand\brandController@index')->name('sisadmin.brand.index');
     Route::post('/brand/store', 'admin\company\brand\brandController@store')->name('sisadmin.brand.store');

     Route::get('brand/destroy/{id}', 'admin\company\brand\brandController@destroy')->name('sisadmin.brand.destroy');

     Route::get('brand/{brand}/edit', 'admin\company\brand\brandController@edit')->name('sisadmin.brand.edit');
     Route::post('brand/{id}/update', 'admin\company\brand\brandController@update')->name('sisadmin.brand.update');

        /*categories*/
    Route::get('/categories/index', 'admin\company\categories\CategoriesController@index')->name('sisadmin.categories.index');
    Route::get('/categories/create', 'admin\company\categories\CategoriesController@create')->name('sisadmin.categories.create');
    Route::post('/categories/store', 'admin\company\categories\CategoriesController@store')->name('sisadmin.categories.store');
    Route::get('/categories/{id}/edit', 'admin\company\categories\CategoriesController@edit')->name('sisadmin.categories.edit');
    Route::get('/categories/show', 'admin\company\categories\CategoriesController@show')->name('sisadmin.categories.show');
    Route::post('categories/{id}/update', 'admin\company\categories\CategoriesController@update')->name('sisadmin.categories.update');

    Route::get('/categories/{id}', 'admin\company\categories\CategoriesController@destroy')->name('sisadmin.categories.delete');

    /*subcategories*/
    Route::get('/sub_categories/index', 'admin\company\categories\SubcategoryController@index')->name('sisadmin.subcategory.index');

    Route::post('/sub_categories/store', 'admin\company\categories\SubcategoryController@store')->name('sisadmin.subcategory.store');

    Route::get('/sub_categories/delete', 'admin\company\categories\SubcategoryController@destroy')->name('sisadmin.subcategory.delete');

    Route::get('/sub_categories/getSubCategory/{id}', 'admin\company\categories\SubcategoryController@getSubCategory')->name('sisadmin.sub_categories.getSubCategory');

    //Downloads
    Route::get('/downloads/index','admin\company\download\downloadsController@index')->name('downloadIndex');
    Route::post('/downloads/store','admin\company\download\downloadsController@store')->name('downloadStore');
    Route::get('/downloads/{id}/edit','admin\company\download\downloadsController@edit')->name('downloadEdit');
    Route::get('/downloads/{id}/delete','admin\company\download\downloadsController@destroy')->name('downloadDelete');
});
