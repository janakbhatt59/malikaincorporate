<?php
    Route::get('/', 'front\homeController@home')->name('homePage');
    Route::get('/smartCity/{id}', 'front\homeController@withSidebar')->name('smartCity');

    Route::get('/navOpen/{id}', 'admin\topNavController@navOpen')->name('topNavOpen');

    Route::get('/ContactUs','front\homeController@contact')->name('contact');

    Route::get('/Gallery','front\homeController@gallery')->name('gallery');

    Route::get('/contactUs', 'admin\contactUsController@contactUs')->name('contactUs');
    Route::post('/contactUs/msg', 'admin\contactUsController@contactUsPost')->name('contactUsPost');

    // Route::post('/contactUs/post',['as'=>'contactUs.store','uses'=>'admin\contactUsController@contactUsPost']);


    //buy products
    Route::get('/buyProducts', 'front\homeController@product')->name('product');

    //company details
    Route::get('/companyDetails', 'front\homeController@details')->name('details');

    // productList
    Route::get('/productLists/{id}', 'front\homeController@moreinfo')->name('productList');
    Route::get('/productLists', 'front\homeController@productsLists')->name('productsLists');
    Route::get('/productLists/{id}/moreinfo', 'front\homeController@moreinfo')->name('moreinfo');

    //Aboutus
    Route::get('/AboutUs','front\homeController@about')->name('about');
    Route::get('/Sectors','front\homeController@sector')->name('sector');

    // Downloads
    Route::get('/downloads','front\homeController@download')->name('downloads');

    //Our partners
    Route::get('/OurPartners',function(){
        return view('page.ourParterns');
    })->name('ourpartners');

    Route::get('/productLists/{id}', 'front\homeController@moreinfo')
                                        ->name('productList');
    Route::get('/productLists', 'front\homeController@productsLists')
                                        ->name('productsLists');
    Route::get('/productLists/moreinfo/{id}', 'front\homeController@moreinfo')
                                        ->name('moreinfo');
    //biometric
    Route::get('/biometric', 'front\homeController@biometrics')->name('biometrics');
    Route::get('/biometric/moreinfo/{id}', 'front\homeController@biometricInfo')
                                        ->name('biometric');
    Route::get('/biometric/{id}/moreinfo', 'front\homeController@biometricInfo')
                                        ->name('biometricInfo');
    // card Printers
    Route::get('/indLevPrinter', 'front\homeController@IPrinters')->name('IPrinters');
    Route::get('/indLevPrinter/moreinfo/{id}', 'front\homeController@IPrinterInfo')
                                        ->name('IPrinter');
    Route::get('/indLevPrinter/{id}/moreinfo', 'front\homeController@IPrinterInfo')
                                        ->name('IPrinterInfo');
   //barcodeScanner
    Route::get('/barcodeScanner', 'front\homeController@barcodeScanners')
                                                ->name('barcodeScanners');
    Route::get('/barcodeScanner/{id}', 'front\homeController@barcodeScannerInfo')
                                                ->name('barcodeScanner');
    Route::get('/barcodeScanner/moreinfo/{id}', 'front\homeController@barcodeScannerInfo')
                                        ->name('barcodeScannerInfo');
   //mobPrinters
    Route::get('/mobPrinter', 'front\homeController@mobPrinters')
                                        ->name('mobPrinters');
    Route::get('/mobPrinter/{id}', 'front\homeController@mobPrinterInfo')
                                        ->name('mobPrinter');
    Route::get('/mobPrinter/moreinfo/{id}', 'front\homeController@mobPrinterInfo')
                                        ->name('mobPrinterInfo');
   //mobile
    Route::get('/mobile', 'front\homeController@mobiles')
                                        ->name('mobiles');
    Route::get('/mobile/{id}', 'front\homeController@mobileInfo')
                                        ->name('mobile');
    Route::get('/mobile/moreinfo/{id}', 'front\homeController@mobileInfo')
                                        ->name('mobileInfo');
   //barcodeLevelPrinters
    Route::get('/barcodeLevelPrinter', 'front\homeController@barcodeLevelPrinters')
                                        ->name('barcodeLevelPrinters');
    Route::get('/barcodeLevelPrinter/{id}', 'front\homeController@barcodeLevelPrinterInfo')
                                        ->name('barcodeLevelPrinter');
    Route::get('/barcodeLevelPrinter/moreinfo/{id}', 'front\homeController@barcodeLevelPrinterInfo')
                                        ->name('barcodeLevelPrinterInfo');
   //hospitalWristbandPrinters
    Route::get('/hospitalWristbandPrinter', 'front\homeController@hospitalWristbandPrinters')
                                        ->name('hospitalWristbandPrinters');
    Route::get('/hospitalWristbandPrinter/{id}', 'front\homeController@hospitalWristbandPrinterInfo')
                                        ->name('hospitalWristbandPrinter');
    Route::get('/hospitalWristbandPrinter/moreinfo/{id}', 'front\homeController@hospitalWristbandPrinterInfo')
                                        ->name('hospitalWristbandPrinterInfo');
    //bioMiniFingerPrint
    Route::get('/bioMiniFingerPrint', 'front\homeController@bioMiniFingerPrint')
                                        ->name('bioMiniFingerPrint');
//   Route::get('/bioMiniFingerPrint/moreinfo/{id}', 'front\homeController@hospitalWristbandPrinterInfo')
//                                         ->name('bioMiniFingerPrintInfo');
//     Route::get('/bioMiniFingerPrint/{id}', 'front\homeController@hospitalWristbandPrinterInfo')
//                                         ->name('bioMiniFingerPrint');

    //getInTouch
    Route::get('/product/getInTouch', 'front\homeController@getInTouch')->name('getInTouch');

    //Printing Supplies
    Route::get('/printingSupplies', 'front\homeController@printingSupplies')->name('printingSupplies');
    Route::get('/printingSuppliesShow','front\homeController@viewProducts')->name('viewPrintingSupplies');
