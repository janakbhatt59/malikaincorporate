<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFingersTable extends Migration
{
   
    public function up()
    {
        Schema::create('fingers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('heading')->nullable();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->string('bullet1')->nullable();
            $table->string('bullet2')->nullable();
            $table->string('bullet3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fingers');
    }
}
