<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jobSummary');
            $table->string('jobDescription1');
            $table->string('jobDescription2');
            $table->string('jobDescription3');
            $table->string('jobDescription4');
            $table->string('jobDescription5');
            $table->string('jobDescription6');
            $table->string('jobDescription7');
            $table->string('jobDescription8');
            $table->string('jobDescription9');
            $table->string('jobDescription10');
            $table->string('jobSpecification1');
            $table->string('jobSpecification2');
            $table->string('jobSpecification3');
            $table->string('jobSpecification4');
            $table->string('jobSpecification5');
            $table->string('jobSpecification6');
            $table->string('jobSpecification7');
            $table->string('jobSpecification8');
            $table->string('jobSpecification9');
            $table->string('jobSpecification10');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers');
    }
}
