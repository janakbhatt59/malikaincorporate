<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('product_slug');
            $table->text('description')->nullable();
            $table->integer('categories_id');
            $table->string('featured_img');
            $table->string('images')->nullable();
            $table->string('discount')->nullable();
            $table->integer('brands_id')->nullable();
            $table->string('price');
            $table->integer('sub_categories_id')->nullable();
            $table->string('author_manufactural_name')->nullable();

            $table->boolean('status')->default(1);
            $table->boolean('soft_delete')->default(1);
            $table->text('vedio_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
