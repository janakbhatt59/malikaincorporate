<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavOpensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nav_opens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image1')->nullable();
            $table->string('heading1')->nullable();
            $table->string('description1')->nullable();
            $table->string('image2')->nullable();
            $table->string('heading2')->nullable();
            $table->string('description2')->nullable();
            $table->string('image3')->nullable();
            $table->string('heading3')->nullable();
            $table->string('description3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nav_opens');
    }
}
