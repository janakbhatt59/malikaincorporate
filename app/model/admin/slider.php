<?php

namespace App\model\admin;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    protected $table = 'sliders';
    protected $fillable = ['img_path', 'call_action', 'img_caption'];
}
