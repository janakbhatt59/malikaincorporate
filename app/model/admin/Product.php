<?php

namespace App\model\admin;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name','product_slug', 'categories_id', 'description',
     'price', 'discount', 'brands_id', 'featured_img','status','softdelete', 'vedio_link', 'author_manufactural_name',];

    public function categories()
    {
      return $this->belongsTo(category::class);
    }

    public function brands(){
    	return $this->belongsTo(Brand::class);
    }

    public function comments(){
    	return $this->belongsTo(Comment::class);
    }

    /*subcategories*/
    public function subcategories(){
        return $this->belongsTo(SubCategory::class, 'sub_categories_id');
    }
}
