<?php

namespace App\model\admin;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
    protected $fillable = ['fileName','ImageName'];
}
