<?php

namespace App\Http\Controllers\front;

use App\model\admin\leftContent;
use App\model\admin\companyDetails\details;
use App\model\admin\navOpen;
use App\desc\description;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\gallery;
use App\Products\cardprinter;
use  App\admin\sector;
use App\admin\about;
use App\Products\bio;
use App\Products\industrialprinter;
use App\Products\barcode;
use App\Products\barcodeScanner;
use App\Products\mobile;
use App\Products\mobtickprinter;
use App\Products\hospital;
use App\Products\finger;
use App\model\admin\printing;
use App\model\admin\Ribbons\zxp3ribbons;
use App\model\admin\Ribbons\zxp7ribbons;
use App\model\admin\Ribbons\zxp9ribbons;
use App\model\admin\HomeSlider;

class homeController extends Controller
{
    public $viewPath = 'page';

    // public function __construct()
    // {
    //         $this->middleware('auth')->except('home', 'aboutus', 'withSidebar','contact','gallery','product',
    //         'biometricstore','productList','moreinfo','productsLists','biometrics','biometricsInfo');

    // }

    public function home(){
     $page['page_title']       = config('siteContent.companyName') . 'Home';
     $page['page_description'] = 'Malika Incorporate';

    //  $desc = DB::table('description')->get();
     /*data extraction*/
     $homeSlider = HomeSlider::all();
     return  view($this->viewPath . '.home', compact(['page','homeSlider']));
    }

    //-------------------------------productLists
    public function productsLists(){


     $page['page_title']          = config('siteContent.companyName') . 'Our products';
     $page['page_description']    = 'malika incorporate this is the desceiption about our products';
     $productsList                = cardprinter::all();

     return  view($this->viewPath . '.productsList', compact(['page','productsList']));
 }
    public function moreinfo($id){

         $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
         $page['page_description']   = 'malika incorporate this is the detailed description about our products';
         $productInfo  = cardPrinter::findOrFail($id);
         return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
        }

    //-------------------------------mobile
        public function mobileInfo($id){



         $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
         $page['page_description']   = 'malika incorporate this is the detailed description about our products';
         $productInfo  = mobile::findOrFail($id);
         return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
        }


       public function mobiles(){


        $page['page_title']          = config('siteContent.companyName') . 'Our products';
        $page['page_description']    = 'malika incorporate this is the desceiption about our products';
        $productsList                = mobile::all();

        return  view($this->viewPath . '.mobiles', compact(['page','productsList']));
    }
    //-------------------------------mobPrinters
        public function mobPrinterInfo($id){



         $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
         $page['page_description']   = 'malika incorporate this is the detailed description about our products';
         $productInfo  = mobtickprinter::findOrFail($id);
         return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
        }



       public function mobPrinters(){
        $page['page_title']          = config('siteContent.companyName') . 'Our products';
        $page['page_description']    = 'malika incorporate this is the desceiption about our products';
        $productsList                = mobtickprinter::all();

        return  view($this->viewPath . '.mobPrinters', compact(['page','productsList']));
    }
    //-------------------------------barcodeLevelPrinters
        public function barcodeLevelPrinterInfo($id){

         $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
         $page['page_description']   = 'malika incorporate this is the detailed description about our products';
         $productInfo  = barcode::findOrFail($id);
         return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
        }


       public function barcodeLevelPrinters(){


        $page['page_title']          = config('siteContent.companyName') . 'Our products';
        $page['page_description']    = 'malika incorporate this is the desceiption about our products';
        $productsList = barcode::all();


        return  view($this->viewPath . '.barcodeLevelPrinters', compact(['page','productsList']));
    }

    //------------------------end product list
    //-------------------------------productLists

    //------------------------start biometric


        public function biometricInfo($id){

        $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
        $page['page_description']   = 'malika incorporate this is the detailed description about our products';
        $productInfo =  bio::findOrFail($id);
        return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
    }

       public function biometrics(){


        $page['page_title']          = config('siteContent.companyName') . 'Our products';
        $page['page_description']    = 'malika incorporate this is the desceiption about our products';
        $productsList                = bio::all();

        return  view($this->viewPath . '.biometrics', compact(['page','productsList']));
    }

    //-----------end biometric

    //----------------start iprinters
      public function IPrinters(){


        $page['page_title']          = config('siteContent.companyName') . 'Our products';
        $page['page_description']    = 'malika incorporate this is the desceiption about our products';
        $productsList                = industrialprinter::all();
        return  view($this->viewPath . '.IPrinters', compact(['page','productsList']));
    }
          public function IPrinterInfo($id){

        $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
        $page['page_description']   = 'malika incorporate this is the detailed description about our products';
        $productInfo  = industrialprinter::findOrFail($id);
        return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
    }
    //---------------------end iprinter
      public function bioMiniFingerPrint(){
        $page['page_title']          = config('siteContent.companyName') . 'Our products';
        $page['page_description']    = 'malika incorporate this is the desceiption about our products';
        $productsList                = finger::all();

        return  view($this->viewPath . '.bioMiniFingerPrint', compact(['page','productsList']));
    }
public function bioMiniFingerPrintInfo($id){

        $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
        $page['page_description']   = 'malika incorporate this is the detailed description about our products';
        $productInfo  = finger::findOrFail($id);
        return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
    }
    //---------------------start barcodeScanner

 public function barcodeScanners(){
        $page['page_title']          = config('siteContent.companyName') . 'Our products';
        $page['page_description']    = 'malika incorporate this is the desceiption about our products';
        $productsList                = barcodeScanner::all();

        return  view($this->viewPath . '.barcodeScanner', compact(['page','productsList']));
    }
          public function barcodeScannerInfo($id){

        $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
        $page['page_description']   = 'malika incorporate this is the detailed description about our products';
        $productInfo  = barcodeScanner::findOrFail($id);
        return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
    }
    //---------------------end barcodeScanner
    //---------------------start hospitalWristbandPrinters

 public function hospitalWristbandPrinters(){
        $page['page_title']          = config('siteContent.companyName') . 'Our products';
        $page['page_description']    = 'malika incorporate this is the desceiption about our products';
        $productsList                = hospital::all();

        return  view($this->viewPath . '.hospitalWristbandPrinter', compact(['page','productsList']));
    }
          public function hospitalWristbandPrinterInfo($id){

        $page['page_title']         = config('siteContent.companyName') . 'Our products detailed information';
        $page['page_description']   = 'malika incorporate this is the detailed description about our products';
        $productInfo  = hospital::findOrFail($id);
        return  view($this->viewPath . '.moreInfoProduct',compact(['page','productInfo']));
    }
    //---------------------end barcodeScanner


    public function product(){
    //  $desc = DB::table('description')->get();
     /*data extraction*/
     return  view($this->viewPath . '.buyProducts');
    }



    public function withSidebar($id){

        $content=new leftContent();

        $page['page_title']       = config('siteContent.companyName') . 'Smart City';
        $page['page_description'] = 'Malika Incorporate';
        $content                  = leftContent::findOrFail($id);

        return  view($this->viewPath . '.withSidebar', compact(['page','content']));
    }

    public function contact(){
     $page['page_title']       = config('siteContent.companyName') . 'Contact Us';
     $page['page_description'] = 'Malika Incorporate Contact us';

    //  $desc = DB::table('description')->get();
     /*data extraction*/
     return  view($this->viewPath . '.contact', compact(['page']));
    }

    public function download(){
        $page['page_title']       = config('siteContent.companyName') . 'Downloads';
        $page['page_description'] = 'Malika Incorporate Downloads';

     return  view($this->viewPath . '.downloads', compact(['page']));
    }

    public function gallery(){
        $page['page_title']       = config('siteContent.companyName') . 'Gallery';
        $page['page_description'] = 'Malika Incorporate Gallery';

        $gallery = gallery::get();

        return  view($this->viewPath . '.gallery', compact(['page','gallery']));
    }

    public function about(){
        $page['page_title']       = config('siteContent.companyName') . 'About Us';
        $page['page_description'] = 'About Us';

        $about = about::get();

        return  view($this->viewPath . '.about', compact(['page','about']));
    }


    public function sector(){
        $page['page_title']       = config('siteContent.companyName') . 'Sectors we Serve';
        $page['page_description'] = 'Sectors we Serve';

        $sector = sector::get();

        return  view($this->viewPath . '.sector', compact(['page','sector']));
    }





    public function details(){
        $page['page_title']       = config('siteContent.companyName') . 'Company Details';
        $page['page_description'] = 'malika incorporate Company Details';

        $details = details::get();

        return  view($this->viewPath . '.detailsLayout', compact(['page','details']));
    }

    public function getInTouch(){
         $page['page_title']       = config('siteContent.companyName') . 'get in touch with us';
        $page['page_description'] = 'malika incorporate get in touch with us. Product inquery Form  ';
        return view($this->viewPath. '.getInTouch');
    }

    public function printingSupplies(){
        $page['page_title']       = config('siteContent.companyName') . 'Printing Supplies';
        $page['page_description'] = 'Malika Incorporate Printing Supplies';

        $printing = printing::get();
        return view($this->viewPath.'.printingSupplies',compact(['page','printing']));

    }

    public function viewProducts(){
        $page['page_title']       = config('siteContent.companyName') . 'Printing Supplies';
        $page['page_description'] = 'Malika Incorporate Printing Supplies';

        $zxp3ribbons = zxp3ribbons::get();
        $zxp7ribbons = zxp7ribbons::get();
        $zxp9ribbons = zxp9ribbons::get();
        return view($this->viewPath.'.ribbons',compact(['page','zxp3ribbons','zxp9ribbons','zxp7ribbons']));

    }

}
