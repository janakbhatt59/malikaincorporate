<?php

namespace App\Http\Controllers\admin;

use App\model\admin\navOpen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class topNavController extends Controller
{
    public $viewPath = 'page';

       public function navOpen($id){
        $navOpen=new navOpen();

        $page['page_title']       = config('siteContent.companyName') . 'Nav Content';
        $page['page_description'] = 'malika incorporate nav description';
        $navOpen                  = navOpen::findOrFail($id);

        return  view($this->viewPath . '.navOpen', compact(['page','navOpen']));
    }
}
