<?php

namespace App\Http\Controllers\admin\company\download;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\Download;


class downloadsController extends Controller
{
    public $viewPath = 'page.download';
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'Downloads';
        $page['page_description'] = 'add downloads to malika incorporate';
        $downloads=Download::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.index',compact(['page','downloads']));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $download = new Download();
        $download->name=$request->name;
        if($request->hasFile('download')){
            $doc=$request->file('download');
            $input=time().'.'.$doc->getClientOriginalExtension();
            $destinationPath='download';
            $download->download=$destinationPath .'/'. $input;
            $doc->move($destinationPath, $input);
            }

    //    if ($request->hasFile('download')) {
    //     $download->name=$request->name;
    //     $download = uniqid() . $request->download->getClientOriginalName() . '.' . $request->download->getClientOriginalExtension();

    //     $download->move(public_path('download') . $download);
    //    }
        // if ($request->hasFile('download')) {
        //     $download = $request->file('download');
        //     $filename = str_replace(' ', '', $request->name). '.' . $download->getClientOriginalExtension();
        //     $download->move('download',$filename);
        //     $download->download = 'download'.$filename;
        //  }


        $mySave=$download->save();

        if($mySave){
            return redirect()->route('downloadIndex')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit downloads Link';
        $page['page_description'] = 'Downloads details of malika incorporate';

        $download = Download::findOrFail($id);
        // if(!$details){
        //     return back()->withMessage('ID Not Found');
        // }
        // return view($this->viewPath.'.editdetails',compact(['details','page']));

        if($download) {
            return response()->json([
                'success'       => true,
                'id'            => $download->id,
                'name'      => $download->name,
                'download'   => $download->download,
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Unauthorized access!'
        ], 401);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $download = Download::findOrFail($id);

        $download->name=$request->name;
        if($request->hasFile('download')){
            $doc=$request->file('download');
            $input=time().'.'.$doc->getClientOriginalExtension();
            $destinationPath='download';
            $download->download=$destinationPath .'/'. $input;
            $doc->move($destinationPath, $input);
            }

            $myUpdate=$download->update();

            if($myUpdate){
                return redirect()->route('downloadIndex')->withMessage('Data Updated Successfully');
            }

            else{
                return back()->withMessage('Oops...Something went wrong. Data not Updated');
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $download       = Download::findOrFail($id);
        $downloadDelete = $download->delete();
        if($downloadDelete ){
            return redirect()->route('downloadIndex')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
