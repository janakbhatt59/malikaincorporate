<?php

namespace App\Http\Controllers\admin\company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\printing;
use Intervention\Image\Facades\Image;

class printingController extends Controller
{
    public $viewPath = 'adminPage.company.printingSupplier';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Supplier';
        $page['page_description'] = 'add supplier of malika incorporate';
        $supplier = printing::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexsupplier',compact(['page','supplier']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add supplier of malika incorporate';
         return view($this->viewPath.'.createsupplier',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'name'=>'required',
            'photo'=>'required',
            'desc'=>'required'

        ]);

        $supplier = new printing();

        $supplier->name=$request->name;
        $supplier->desc=$request->desc;
             
        if($request->hasFile('photo')){
            $image = $request->file('photo');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/supplier/' . $filename );
            $supplier->photo =  'assests/img/supplier/' . $filename;
        }

        $mySave=$supplier->save();

        if($mySave){
            return redirect()->route('viewsupplier')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit supplier';
        $page['page_description'] = 'Edit supplier of malika incorporate';

        $supplier =printing::findOrFail($id);
        if(!$supplier){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editsupplier',compact(['supplier','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'name'=>'required',
            'photo'=>'required',
            'desc'=>'required',
            
        ]);


        $supplier = printing::findOrFail($id);

        $supplier->name=$request->name;
        $supplier->desc=$request->desc;

        if($request->hasFile('photo')){
            $image = $request->file('photo');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/supplier/' . $filename );
            $supplier->photo =  'assests/img/supplier/' . $filename;
        }


        $myUpdate=$supplier->update();

        if($myUpdate){
            return redirect()->route('viewsupplier')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $supplier      = printing::findOrFail($id);
        $deletesupplier = $supplier->delete();
        if($deletesupplier ){
            return redirect()->route('viewsupplier')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
