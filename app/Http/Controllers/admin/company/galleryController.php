<?php

namespace App\Http\Controllers\admin\company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\gallery;
use Intervention\Image\Facades\Image;

class galleryController extends Controller
{
    public $viewPath = 'adminPage.company.Gallery';
    public function index()
    {
          $page['page_title']       = config('siteContent.companyName') . 'view gallery';
       $page['page_description'] = 'create gallery of malika incorporate';
       $gallery = gallery::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexGallery',compact(['page','gallery']));
    }

    public function create()
     {
         $page['page_title']       = config('siteContent.companyName') . 'create gallery';
       $page['page_description'] = 'create gallery of malika incorporate';
        return view($this->viewPath.'.createGallery',compact(['page']));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'fileName'=>'required',
            'ImageName'=>'required'
        ]);

        $gallery = new gallery();

        $gallery->fileName=$request->fileName;
       
        if($request->hasFile('ImageName')){
            $image = $request->file('ImageName');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Gallery/' . $filename );
            $gallery->ImageName =  'assests/img/Gallery/' . $filename;
        }
        $mySave=$gallery->save();

        if($mySave){
            return redirect()->route('viewPhoto')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

    }

    public function edit($id)
    {

         $page['page_title']       = config('siteContent.companyName') . 'edit gallery';
         $page['page_description'] = 'Edit gallery of malika incorporate';

         $gallery = Gallery::findOrFail($id);
         if(!$gallery){
             return back()->withMessage('ID Not Found');
         }
         return view($this->viewPath.'.editGallery',compact(['gallery','page']));
         
        
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'fileName'=>'required',
            'ImageName'=>'required'
        ]);

        $gallery = gallery::findOrFail($id);

        $gallery->fileName=$request->fileName;
       
        if($request->hasFile('ImageName')){
            $image = $request->file('ImageName');
            $filename =time().'.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Gallery/' . $filename );
            $gallery->ImageName =  'assests/img/Gallery/' . $filename;
        }
        
        $myUpdate=$gallery->update();

        if($myUpdate){
            return redirect()->route('viewPhoto')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

       
    }

    public function destroy($id)
    {
        $gallery       = gallery::findOrFail($id);
        $galleryDelete = $gallery->delete();
        if($galleryDelete){
            return redirect()->route('viewPhoto')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
