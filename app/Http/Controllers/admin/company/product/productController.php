<?php

namespace App\Http\Controllers\admin\company\product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\Product;
use App\model\admin\Brand;
use App\model\admin\Comment;
use App\model\admin\category;
use App\Http\Requests\admin\ProductsValidation;
use Intervention\Image\Facades\Image;
use File;

class productController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth:sisadmin');
    // }

    public function index()
    {
        $page['page_title']      = 'Mywebnepal:Product';
        $page['page_description'] = 'List of all product';

        $catId = category::select('id', 'name')->get();
        $data = Product::orderBy('created_at', 'desc')->paginate(10);
        $brand = Brand::select('id', 'name')->get();
        if ($data) {
            return view('page.products.index', compact(['page', 'data', 'catId', 'brand']));
        }else{
            return view('page.products.index', compact(['page']));
        }
    }
    protected function getProductUniqueId(){
        return uniqid();
    }
    public function store(ProductsValidation $request)
    {
        $slug = ($request->name);
        $time = date('Y-m-d');
        $save = new Product;
        $save->name = $request->name;
        $save->product_slug = $slug;
        $save->categories_id = $request->categories_id;
        $save->sub_categories_id  = $request->sub_categories_id;
        $save->description   = $request->description;
        $save->discount       = $request->discount;
        $save->price          = $request->price;
        $save->brands_id      = $request->brands_id;
        $save->vedio_link     = $request->vedio_link;


        if ($request->hasFile('featured_img')) {
           $imgFile = $request->file('featured_img');

           $filename = str_replace(' ', '', $request->name).$time. '.' . $imgFile->getClientOriginalExtension();

           $this->resizeImageAndUpload($imgFile, 400, 400, 'product/featured_image_sm/',$filename);

           $save->featured_img = 'product/featured_image_sm/'.$filename;
        }

        if ($files = $request->file('images')) {
            $images=[];
        foreach($files as $file){
           $filename = str_replace(' ', '', $request->name).$time . '.' . $file->getClientOriginalExtension();
           $path='product/product_image/'.$filename;
           array_push($images, [
               'imagePath' => $path
               ]);
            $this->resizeImageAndUpload($imgFile, 400, 400, 'product/product_image/', $filename);
           }
           $save->images = 'product/product_image/'.$filename;
        }

        $saveData = $save->save();
        if ($saveData) {
            return back()->withMessage('successfully inserted your product');
        }else{
            return back()->withMessage('Oops try it again')->withInput();
        }
    }

    public function resizeImageAndUpload($file, $sizeWidth, $sizeHeight, $path, $imageName){
     $upload = Image::make( $file )->resize( $sizeWidth, $sizeHeight )->save($path . $imageName );
     if ($upload) {
         return true;
     }else{
        return false;
     }

    }

    public function show(Product $products)
    {
        $page['page_title']      = 'Mywebnepal::singleProduct';
        $page['page_description'] = 'Product Details';
        return view('page.products.show', compact('products', 'page'));
    }

    public function edit(Product $products)
    {
        $page['page_title']      = 'MalikaIncorporate::Product edit';
        $page['page_description'] = 'Product edit ';
        $catId = category::select('id', 'name')->get();
        $brand = Brand::select('id', 'name')->get();
        // return $products;
        if ($products) {
            return view('page.products.edit', compact(['page', 'products', 'catId', 'brand']));
        }else{
            return back()->withMessage('Oops data is not found');
        }
    }

    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //        'categories_id' => 'required'
        //    ]);
        $save = Product::findOrFail($id);
        if ($save) {
            $time = date('Y-m-d');
            $save->name = $request->name;
            $slug = ($request->name);
            $save->product_slug = $slug;
            $save->categories_id = $request->categories_id;
            $save->sub_categories_id     = $request->sub_categories_id;
            $save->description           = $request->description;
            $save->discount          = $request->discount;
            $save->price                   = $request->price;
            $save->author_manufactural_name= $request->author_manufactural_name;
            $save->brands_id      = $request->brands_id;
            $save->vedio_link     = $request->vedio_link;

            if ($request->hasFile('featured_img')) {
                $imgFile = $request->file('featured_img');

                $filename = str_replace(' ', '', $request->name).$time. '.' . $imgFile->getClientOriginalExtension();

                $this->resizeImageAndUpload($imgFile, 400, 400, 'product/featured_image_sm/',$filename);

                $save->featured_img = 'product/featured_image_sm/'.$filename;
             }

             if ($files = $request->file('images')) {
                 $images=[];
             foreach($files as $file){
                $filename = str_replace(' ', '', $request->name).$time . '.' . $file->getClientOriginalExtension();
                $path='product/product_image/'.$filename;
                array_push($images, [
                    'imagePath' => $path
                    ]);
                 $this->resizeImageAndUpload($imgFile, 400, 400, 'product/product_image/', $filename);
                }
                $save->images = 'product/product_image/'.$filename;
             }

            $saveData = $save->update();

            if ($saveData) {
                return redirect()->route('sisadmin.products.index')->withMessage('successfully update');
            }else{
                return back()->withMessage('Oops sorry try it again');
            }
        }
    }
    public function destroy($id)
    {
        $product = Product::where('id', $id)->first();
        if ($product) {
            if (file_exists($product->featured_img)) {
                File::delete($product->featured_img);
               // unlink(asset($product->featured_img_sm));
            }

            if (file_exists($product->images)) {
                File::delete($product->images);
                // unlink(asset($product->product_image));
            }
           $product->delete();

           return response()->json([
             'success' => true,
             'message' => 'product delete'
            ], 200);

        }
        return response()->json([
             'success' => false,
             'message' => 'sorry product is not delete'
            ], 200);


    }

    public function changeProductStatus(Request $request){
      $id  = $request->id;
      $status = $request->status==1 ? 0 : 1;

      $product  = Product::findOrFail($id);
      if ($product) {
         $product->status = $status;
          $product->update();
          return response()->json([
             'success' => true,
             'message' => 'product status chnaged'
            ], 200);
      }else{
        return response()->json([
          'success' => false,
            'message' => 'product status could not changed'
        ], 401);
      }
    }
    public function makeFeaturedProduct(Request $request){
        $id  = $request->id;
      $status = $request->status==1 ? 0 : 1;

      $product  = Product::findOrFail($id);
      if ($product) {
         $product->featured_product = $status;
          $product->update();
          return response()->json([
             'success' => true,
             'message' => 'this product became featured product'
            ], 200);
      }else{
        return response()->json([
          'success' => false,
            'message' => 'Oops sorry this try it again'
        ], 401);
      }
    }

    public function makeAppreanceProduct(Request $request){
          $id  = $request->id;
        $status = $request->status==1 ? 0 : 1;

        $product  = Product::findOrFail($id);
        if ($product) {
           $product->appreance = $status;
            $product->update();
            return response()->json([
               'success' => true,
               'message' => 'this product became featured product'
              ], 200);
        }else{
          return response()->json([
            'success' => false,
              'message' => 'Oops sorry this try it again'
          ], 401);
        }
    }

    public function supportForm(){
     $sprtForm  = supportForm::orderBy('created_at', 'desc')->paginate(10);
     if ($sprtForm) {
         return view('admin.customer.supportForm', compact(['sprtForm']));
     }else{
        return view('admin.customer.supportForm');
     }

    }
    public function productComment(){
      $productComment  = Comment::orderBy('created_at', 'desc')->paginate(10);
      if ($productComment) {
          return view('admin.customer.productComment', compact(['productComment']));
      }else{
         return view('admin.customer.productComment');
      }
    }
    public function commentDelete($id){
     $del = Comment::findOrFail($id);
     if ($del) {
         $del->delete();
         return response()->json([
             'success' => true,
             'message' => 'successfully delete'
            ], 200);
     }else{
          return response()->json([
             'success' => false,
             'message' => 'Oops sorry try it again'
            ], 200);
     }
    }
    /*comment status enable or disable*/
    public function changeCommentStatus($id){

    }
    /*subscribe */
    public function userSubscribe(){
      $subData  = subscribe::orderBy('created_at', 'desc')->paginate();
        return view('admin.customer.subscribe', compact(['subData']));
    }
}

