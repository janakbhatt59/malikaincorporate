<?php

namespace App\Http\Controllers\admin\company\Ribbons;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\Ribbons\zxp7ribbons;
use Intervention\Image\Facades\Image;

class zxp7RibbonController extends Controller
{
    
    public $viewPath = 'adminPage.company.Ribbons.ZXP7';
    public function index()
    {
       $page['page_title']       = config('siteContent.companyName') . 'View Ribbons';
       $page['page_description'] = 'View Ribbons of ZXP 7 ';
       $zxp7ribbons = zxp7ribbons::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexZxp7',compact(['page','zxp7ribbons']));
    }

    public function create()
     {
       $page['page_title']       = config('siteContent.companyName') . 'Add Ribbons';
       $page['page_description'] = 'Add Ribbons of ZXP 7 ';
        return view($this->viewPath.'.createZxp7',compact(['page']));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'brand'=>'required',
            'model'=>'required',
            'printCapacity'=>'required',
            'panelType'=>'required',
            'image'=>'required'
        ]);

        $zxp7ribbons = new zxp7ribbons();

        $zxp7ribbons->brand=$request->brand;
        $zxp7ribbons->model=$request->model;
        $zxp7ribbons->printCapacity=$request->printCapacity;
        $zxp7ribbons->panelType=$request->panelType;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Ribbons/' . $filename );
            $zxp7ribbons->image =  'assests/img/Ribbons/' . $filename;
        }
        $mySave=$zxp7ribbons->save();

        if($mySave){
            return redirect()->route('viewzxp7ribbon')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

    }

    public function edit($id)
    {

         $page['page_title']       = config('siteContent.companyName') . 'Edit Ribbons';
         $page['page_description'] = 'Edit Ribbons of ZXP 7';

         $zxp7ribbons = zxp7ribbons::findOrFail($id);
         if(!$zxp7ribbons){
             return back()->withMessage('ID Not Found');
         }
         return view($this->viewPath.'.editZxp7',compact(['zxp7ribbons','page']));
         
        
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'brand'=>'required',
            'model'=>'required',
            'printCapacity'=>'required',
            'panelType'=>'required',
            'image'=>'required'
        ]);

        $zxp7ribbons = zxp7ribbons::findOrFail($id);

        $zxp7ribbons->brand=$request->brand;
        $zxp7ribbons->model=$request->model;
        $zxp7ribbons->printCapacity=$request->printCapacity;
        $zxp7ribbons->panelType=$request->panelType;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time().'.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Ribbons/' . $filename );
            $zxp7ribbons->image =  'assests/img/Ribbons/' . $filename;
        }
        
        $myUpdate=$zxp7ribbons->update();

        if($myUpdate){
            return redirect()->route('viewzxp7ribbon')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

       
    }

    public function destroy($id)
    {
        $zxp7ribbons       = zxp7ribbons::findOrFail($id);
        $zxp7ribbonsDelete = $zxp7ribbons->delete();
        if($zxp7ribbonsDelete){
            return redirect()->route('viewzxp7ribbon')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
    
}
