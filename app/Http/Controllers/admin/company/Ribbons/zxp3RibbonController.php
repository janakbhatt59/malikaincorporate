<?php

namespace App\Http\Controllers\admin\company\Ribbons;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\Ribbons\zxp3ribbons;
use Intervention\Image\Facades\Image;

class zxp3RibbonController extends Controller
{
    public $viewPath = 'adminPage.company.Ribbons.ZXP3';
    public function index()
    {
       $page['page_title']       = config('siteContent.companyName') . 'View Ribbons';
       $page['page_description'] = 'View Ribbons of ZXP 3 ';
       $zxp3ribbons = zxp3ribbons::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexZxp3',compact(['page','zxp3ribbons']));
    }

    public function create()
     {
       $page['page_title']       = config('siteContent.companyName') . 'Add Ribbons';
       $page['page_description'] = 'Add Ribbons of ZXP 3 ';
        return view($this->viewPath.'.createZxp3',compact(['page']));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'brand'=>'required',
            'model'=>'required',
            'printCapacity'=>'required',
            'panelType'=>'required',
            'image'=>'required'
        ]);

        $zxp3ribbons = new zxp3ribbons();

        $zxp3ribbons->brand=$request->brand;
        $zxp3ribbons->model=$request->model;
        $zxp3ribbons->printCapacity=$request->printCapacity;
        $zxp3ribbons->panelType=$request->panelType;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Ribbons/' . $filename );
            $zxp3ribbons->image =  'assests/img/Ribbons/' . $filename;
        }
        $mySave=$zxp3ribbons->save();

        if($mySave){
            return redirect()->route('viewzxp3ribbon')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

    }

    public function edit($id)
    {

         $page['page_title']       = config('siteContent.companyName') . 'Edit Ribbons';
         $page['page_description'] = 'Edit Ribbons of ZXP 3';

         $zxp3ribbons = zxp3ribbons::findOrFail($id);
         if(!$zxp3ribbons){
             return back()->withMessage('ID Not Found');
         }
         return view($this->viewPath.'.editzXP3',compact(['zxp3ribbons','page']));
         
        
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'brand'=>'required',
            'model'=>'required',
            'printCapacity'=>'required',
            'panelType'=>'required',
            'image'=>'required'
        ]);

        $zxp3ribbons = zxp3ribbons::findOrFail($id);

        $zxp3ribbons->brand=$request->brand;
        $zxp3ribbons->model=$request->model;
        $zxp3ribbons->printCapacity=$request->printCapacity;
        $zxp3ribbons->panelType=$request->panelType;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time().'.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Ribbons/' . $filename );
            $zxp3ribbons->image =  'assests/img/Ribbons/' . $filename;
        } 
        
        $myUpdate=$zxp3ribbons->update();

        if($myUpdate){
            return redirect()->route('viewzxp3ribbon')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

       
    }

    public function destroy($id)
    {
        $zxp3ribbons       = zxp3ribbons::findOrFail($id);
        $zxp3ribbonsDelete = $zxp3ribbons->delete();
        if($zxp3ribbonsDelete){
            return redirect()->route('viewzxp3ribbon')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
    
}
