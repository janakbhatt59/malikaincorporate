<?php

namespace App\Http\Controllers\admin\company\Ribbons;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\model\admin\Ribbons\zxp9ribbons;

class zxp9RibbonController extends Controller
{
    public $viewPath = 'adminPage.company.Ribbons.ZXP9';
    public function index()
    {
       $page['page_title']       = config('siteContent.companyName') . 'View Ribbons';
       $page['page_description'] = 'View Ribbons of ZXP 7 ';
       $zxp9ribbons = zxp9ribbons::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexZxp9',compact(['page','zxp9ribbons']));
    }

    public function create()
     {
       $page['page_title']       = config('siteContent.companyName') . 'Add Ribbons';
       $page['page_description'] = 'Add Ribbons of ZXP 7 ';
        return view($this->viewPath.'.createZxp9',compact(['page']));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'brand'=>'required',
            'model'=>'required',
            'printCapacity'=>'required',
            'panelType'=>'required',
            'image'=>'required'
        ]);

        $zxp9ribbons = new zxp9ribbons();

        $zxp9ribbons->brand=$request->brand;
        $zxp9ribbons->model=$request->model;
        $zxp9ribbons->printCapacity=$request->printCapacity;
        $zxp9ribbons->panelType=$request->panelType;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Ribbons/' . $filename );
            $zxp9ribbons->image =  'assests/img/Ribbons/' . $filename;
        }
        $mySave=$zxp9ribbons->save();

        if($mySave){
            return redirect()->route('viewzxp9ribbon')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

    }

    public function edit($id)
    {

         $page['page_title']       = config('siteContent.companyName') . 'Edit Ribbons';
         $page['page_description'] = 'Edit Ribbons of ZXP 7';

         $zxp9ribbons = zxp9ribbons::findOrFail($id);
         if(!$zxp9ribbons){
             return back()->withMessage('ID Not Found');
         }
         return view($this->viewPath.'.editZxp9',compact(['zxp9ribbons','page']));
         
        
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'brand'=>'required',
            'model'=>'required',
            'printCapacity'=>'required',
            'panelType'=>'required',
            'image'=>'required'
        ]);

        $zxp9ribbons = zxp7ribbons::findOrFail($id);

        $zxp9ribbons->brand=$request->brand;
        $zxp9ribbons->model=$request->model;
        $zxp9ribbons->printCapacity=$request->printCapacity;
        $zxp9ribbons->panelType=$request->panelType;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time().'.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Ribbons/' . $filename );
            $zxp9ribbons->image =  'assests/img/Ribbons/' . $filename;
        }
        
        $myUpdate=$zxp9ribbons->update();

        if($myUpdate){
            return redirect()->route('viewzxp9ribbon')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }

       
    }

    public function destroy($id)
    {
        $zxp9ribbons       = zxp9ribbons::findOrFail($id);
        $zxp9ribbonsDelete = $zxp9ribbons->delete();
        if($zxp9ribbonsDelete){
            return redirect()->route('viewzxp9ribbon')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
