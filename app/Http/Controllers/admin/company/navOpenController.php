<?php

namespace App\Http\Controllers\admin\company;

use App\model\admin\navOpen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;


class navOpenController extends Controller
{
        public $viewPath='adminPage.company.navOpen';
    public function index()
    {
       $page['page_title']       = config('siteContent.companyName') . 'view navigation content list';
       $page['page_description'] = 'create nav content malika incorporate';
       $navOpen = navOpen::orderby('created_at','ASC')->paginate(7);

       return view($this->viewPath.'.indexNavOpen',compact(['page','navOpen']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */    public function create()
    {
          $page['page_title']       = config('siteContent.companyName') . 'create Content';
          $page['page_description'] = 'create Content of malika incorporate';

          return view($this->viewPath.'.createNavOpen',compact(['page']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'heading1'        =>'required',
             'image1'          =>'required',
            // 'desc1'         =>'required',
            // 'heading2'        =>'required',
            // 'desc2'         =>'required',
            // 'heading3'        =>'required',
            // 'desc3'         =>'required',
        ]);


        $navOpen = new navOpen();
        $navOpen->heading1      =  $request->heading1;
        $navOpen->description1  =  $request->desc1;
        $navOpen->heading2      =  $request->heading2;
        $navOpen->description2  =  $request->desc2;
        $navOpen->heading3      =  $request->heading3;
        $navOpen->description3  =  $request->desc3;




        if($request->hasFile('image1')){
            $image1 = $request->file('image1');
            $filename =$request->heading1. '.' . $image1->getClientOriginalExtension();
            Image::make( $image1 )->save('assests/img/navOpen' . $filename );
            $navOpen->image1 =  'assests/img/navOpen' . $filename;
            }




        if($request->hasFile('image2')){
            $image2 = $request->file('image2');
            $filename =$request->heading2. '.' . $image2->getClientOriginalExtension();
            Image::make( $image2 )->save('assests/img/navOpen' . $filename );
             $navOpen->image2 =  'assests/img/navOpen' . $filename;
            }


            if($request->hasFile('image3')){
            $image3 = $request->file('image3');
            $filename =$request->heading3. '.' . $image3->getClientOriginalExtension();
            Image::make( $image3 )->save('assests/img/navOpen' . $filename );
            $navOpen->image3 =  'assests/img/navOpen' . $filename;
            }



         $mySave=$navOpen->save();


         if($mySave){
             return redirect()->route('indexNavOpen')->withMessage('Data Saved Successfully');
         }

         else{
             return back()->withMessage('Oops...Something went wrong. Data not saved');
         }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $page['page_title']       = config('siteContent.companyName') . 'edit Content';
         $page['page_description'] = 'Edit content of malika incorporate';
        $navOpen = navOpen::findOrFail($id);
        if(!$navOpen){
            return back()->withMessage('IdNotFound');
        }
        return view($this->viewPath.'.editNavOpen',compact(['navOpen','page']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            // 'heading1'        =>'required',
            // 'image1'          =>'required',
            // 'desc1'         =>'required',
            // 'heading2'        =>'required',
            // 'image2'          =>'required',
            // 'desc2'         =>'required',
            // 'heading3'        =>'required',
            // 'image3'          =>'required',
            // 'desc3'         =>'required',

        ]);

        $navOpen = navOpen::findOrFail($id);
        $navOpen->heading1      =  $request->heading1;
        $navOpen->description1  =  $request->desc1;
        $navOpen->heading2      =  $request->heading2;
        $navOpen->description2  =  $request->desc2;
        $navOpen->heading3      =  $request->heading3;
        $navOpen->description3  =  $request->desc3;


        if($request->hasFile('image1')){
            $image1 = $request->file('image1');
            $filename =$request->heading1. '.' . $image1->getClientOriginalExtension();
            Image::make( $image1 )->save('assests/img/navOpen' . $filename );
            $navOpen->image1 =  'assests/img/navOpen' . $filename;
            }




        if($request->hasFile('image2')){
            $image2 = $request->file('image2');
            $filename =$request->heading2. '.' . $image2->getClientOriginalExtension();
            Image::make( $image2 )->save('assests/img/navOpen' . $filename );
             $navOpen->image2 =  'assests/img/navOpen' . $filename;
            }


            if($request->hasFile('image3')){
            $image3 = $request->file('image3');
            $filename =$request->heading3. '.' . $image3->getClientOriginalExtension();
            Image::make( $image3 )->save('assests/img/navOpen' . $filename );
            $navOpen->image3 =  'assests/img/navOpen' . $filename;
            }
         $myUpdate=$navOpen->update();
         if($myUpdate){
             return redirect()->route('indexNavOpen')->withMessage('Data updated Successfully');
         }

         else{
             return back()->withMessage('Oops...Something went wrong. Data not updated');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $navOpen       = navOpen::findOrFail($id);
        $navOpenDelete = $navOpen->delete();
        if($navOpenDelete){
            return redirect()->route('indexNavOpen')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
