<?php

namespace App\Http\Controllers\admin\company\companyDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\companyDetails\details;
use Intervention\Image\Facades\Image;

class detailsController extends Controller
{
    public $viewPath = 'adminPage.company.companyDetails';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Details';
        $page['page_description'] = 'add products of malika incorporate';
        $details = details::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexdetails',compact(['page','details']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createdetails',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'CompanyName'=>'required',
            'phon'=>'required',
            'email'=>'required',
            'address'=>'required',
            'logo'=>'required',

        ]);

        $details = new details();

        $details->CompanyName=$request->CompanyName;
        $details->phon=$request->phon;
        $details->email=$request->email;
        $details->address=$request->address;
             
        if($request->hasFile('logo')){
            $image = $request->file('logo');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Details/' . $filename );
            $details->logo =  'assests/img/Details/' . $filename;
        }

        $mySave=$details->save();

        if($mySave){
            return redirect()->route('viewdetails')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit details';
        $page['page_description'] = 'Edit details of malika incorporate';

        $details = details::findOrFail($id);
        if(!$details){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editdetails',compact(['details','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'CompanyName'=>'required',
            'phon'=>'required',
            'email'=>'required',
            'address'=>'required',
            'logo'=>'required',

        ]);

        $details = details::findOrFail($id);

        $details->CompanyName=$request->CompanyName;
        $details->phon=$request->phon;
        $details->email=$request->email;
        $details->address=$request->address;
        $details->logo=$request->logo;
       
        if($request->hasFile('logo')){
            $image = $request->file('logo');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Details/' . $filename );
            $details->logo =  'assests/img/Details/' . $filename;
        }


        $myUpdate=$details->update();

        if($myUpdate){
            return redirect()->route('viewdetails')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $details      = details::findOrFail($id);
        $deletedetails = $details->delete();
        if($deletedetails ){
            return redirect()->route('viewdetails')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
