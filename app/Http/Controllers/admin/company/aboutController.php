<?php

namespace App\Http\Controllers\admin\company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\admin\about;

class aboutController extends Controller
{
    public $viewPath = 'adminPage.company.AboutUs';

    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view About Us';
        $page['page_description'] = 'add content for About Us of malika incorporate';
        $about = about::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexAbout',compact(['page','about']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create About Us';
        $page['page_description'] = 'add content for About Us of malika incorporate';
         return view($this->viewPath.'.createAbout',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'description'=>'required'
        ]);

        $about = new about();

        $about->heading=$request->heading;
        $about->description=$request->description;

        $mySave=$about->save();

        if($mySave){
            return redirect()->route('viewabout')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit about us+';
        $page['page_description'] = 'Edit content of about us of malika incorporate';

        $about = about::findOrFail($id);
        if(!$about){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editAbout',compact(['about','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'description'=>'required'

        ]);

        $about = about::findOrFail($id);

        $about->heading=$request->heading;
        $about->description=$request->description;


        $myUpdate=$about->update();

        if($myUpdate){
            return redirect()->route('viewabout')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $about       = about::findOrFail($id);
        $aboutDelete = $about->delete();
        if($aboutDelete ){
            return redirect()->route('viewabout')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
