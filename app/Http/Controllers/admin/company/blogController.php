<?php

namespace App\Http\Controllers\admin\company;

use App\model\admin\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class blogController extends Controller
{
    public $viewPath = 'adminPage.company';
    public function index()
    {
          $page['page_title']       = config('siteContent.companyName') . 'view blog list';
       $page['page_description'] = 'create blog of malika incorporate';
       $blogs = blog::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.blogList',compact(['page','blogs']));
    }

    public function create()
     {
         $page['page_title']       = config('siteContent.companyName') . 'create blog';
       $page['page_description'] = 'create blog of malika incorporate';
        return view($this->viewPath.'.createBlog',compact(['page']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'title'       =>'required',
            'image'       =>'required',
            'date_from'   =>'required',
            'date_to'     =>'required',
            'desc' =>'required',
        ]);

        $blog = new blog();
        $blog->title       = $request->title;
        $blog->date_from   = $request->date_from;
        $blog->date_to     = $request->date_to;
        $blog->description = $request->desc;

        if($request->hasFile('image')){
            $image=$request->file('image');
            $input=time().'.'.$image->getClientOriginalExtension();
            $destinationPath='assests/img/blogImages';
            $blog->image    = $destinationPath .'/'. $input;
            $image->move($destinationPath, $input);
            }

         $mySave=$blog->save();
         if($mySave){
             return redirect()->route('viewBlog')->withMessage('Data Saved Successfully');
         }

         else{
             return back()->withMessage('Oops...Something went wrong. Data not saved');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $page['page_title']       = config('siteContent.companyName') . 'edit blog';
         $page['page_description'] = 'Edit blog of malika incorporate';
        $blog = blog::findOrFail($id);
        if(!$blog){
            return back()->withMessage('IdNotFound');
        }
        return view($this->viewPath.'.editBlog',compact(['blog','page']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'title'       =>'required',
            'image'       =>'required',
            'date_from'   =>'required',
            'date_to'     =>'required',
            'desc'        =>'required',
        ]);

        $blog = blog::findOrFail($id);
        $blog->title       = $request->title;
        $blog->date_from   = $request->date_from;
        $blog->date_to     = $request->date_to;
        $blog->description = $request->desc;

        if($request->hasFile('image')){
            $image=$request->file('image');
            $input=time().'.'.$image->getClientOriginalExtension();
            $destinationPath='assests/img/blogImages';
            $blog->image    = $destinationPath .'/'. $input;
            $image->move($destinationPath, $input);
            }

         $myUpdate=$blog->update();
         if($myUpdate){
             return redirect()->route('viewBlog')->withMessage('Data updated Successfully');
         }

         else{
             return back()->withMessage('Oops...Something went wrong. Data not updated');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $blog       = blog::findOrFail($id);
        $blogDelete = $blog->delete();
        if($blogDelete){
            return redirect()->route('viewBlog')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
