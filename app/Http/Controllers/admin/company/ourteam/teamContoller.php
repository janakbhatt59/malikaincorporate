<?php

namespace App\Http\Controllers\admin\company\ourteam;

use App\model\admin\team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class teamContoller extends Controller
{
    public $viewPath = 'adminPage.company.OurTeam';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view team';
        $page['page_description'] = 'add team of malika incorporate';
        $team = team::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexteam',compact(['page','team']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create team';
        $page['page_description'] = 'add team of malika incorporate';
         return view($this->viewPath.'.createteam',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'photo'=>'required',
            'name'=>'required',
            'post'=>'required',

        ]);

        $team = new team();

        $team->photo=$request->photo;
        $team->name=$request->name;
        $team->post=$request->post;
             
        if($request->hasFile('photo')){
            $image = $request->file('photo');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            image::make( $image )->save('assests/img/team/' . $filename );
            $team->photo =  'assests/img/team/' . $filename;
        }

        $mySave=$team->save();

        if($mySave){
            return redirect()->route('viewteam')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit team';
        $page['page_description'] = 'Edit team of malika incorporate';

        $team = team::findOrFail($id);
        if(!$team){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editteam',compact(['team','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'photo'=>'required',
            'name'=>'required',
            'post'=>'required',

        ]);

        $team = team::findOrFail($id);

        $team->photo=$request->photo;
        $team->name=$request->name;
        $team->post=$request->post;
       
        if($request->hasFile('photo')){
            $image = $request->file('photo');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            image::make( $image )->save('assests/img/team/' . $filename );
            $team->photo =  'assests/img/team/' . $filename;
        }


        $myUpdate=$team->update();

        if($myUpdate){
            return redirect()->route('viewteam')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $team      = team::findOrFail($id);
        $deleteteam = $team->delete();
        if($deleteteam ){
            return redirect()->route('viewteam')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
