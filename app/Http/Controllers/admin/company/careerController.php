<?php

namespace App\Http\Controllers\admin\company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\career;


class careerController extends Controller
{
    public $viewPath = 'adminPage.company.careers';
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view careers List';
        $page['page_description'] = 'List of careers of malika incorporate';
        $careers = career::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.careersList',compact(['page','careers']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $page['page_title']       = config('siteContent.companyName') . 'create careers';
        $page['page_description'] = 'create careers of malika incorporate';
        return view($this->viewPath.'.createCareers',compact(['page']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $careers = new career();
        $careers->jobSummary         =  $request->jobSummary;

        $careers->jobDescription1    =  $request->jobDescription1;
        $careers->jobDescription2    =  $request->jobDescription2;
        $careers->jobDescription3    =  $request->jobDescription3;
        $careers->jobDescription4    =  $request->jobDescription4;
        $careers->jobDescription5    =  $request->jobDescription5;
        $careers->jobDescription6    =  $request->jobDescription6;
        $careers->jobDescription7    =  $request->jobDescription7;
        $careers->jobDescription8    =  $request->jobDescription8;
        $careers->jobDescription9    =  $request->jobDescription9;
        $careers->jobDescription10   =  $request->jobDescription10;

        $careers->jobSpecification1    =  $request->jobSpecification1;
        $careers->jobSpecification2    =  $request->jobSpecification2;
        $careers->jobSpecification3    =  $request->jobSpecification3;
        $careers->jobSpecification4    =  $request->jobSpecification4;
        $careers->jobSpecification5    =  $request->jobSpecification5;
        $careers->jobSpecification6    =  $request->jobSpecification6;
        $careers->jobSpecification7    =  $request->jobSpecification7;
        $careers->jobSpecification8    =  $request->jobSpecification8;
        $careers->jobSpecification9    =  $request->jobSpecification9;
        $careers->jobSpecification10   =  $request->jobSpecification10;

         $mySave=$careers->save();


         if($mySave){
             return redirect()->route('indexCareers')->withMessage('Data Saved Successfully');
         }

         else{
             return back()->withMessage('Oops...Something went wrong. Data not saved');
         }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit careers';
        $page['page_description'] = 'edit careers of malika incorporate';

        $careers = careers::findOrFail($id);
        if(!$careers){
            return back()->withMessage('IdNotFound');
        }
        return view($this->viewPath.'.editCareers',compact(['careers','page']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $careers = career::findOrFail($id);

        $careers->jobSummery         =  $request->jobSummery;

        $careers->jobDescription1    =  $request->jobDescription1;
        $careers->jobDescription2    =  $request->jobDescription2;
        $careers->jobDescription3    =  $request->jobDescription3;
        $careers->jobDescription4    =  $request->jobDescription4;
        $careers->jobDescription5    =  $request->jobDescription5;
        $careers->jobDescription6    =  $request->jobDescription6;
        $careers->jobDescription7    =  $request->jobDescription7;
        $careers->jobDescription8    =  $request->jobDescription8;
        $careers->jobDescription9    =  $request->jobDescription9;
        $careers->jobDescription10   =  $request->jobDescription10;

        $careers->jobSpecification1    =  $request->jobSpecification1;
        $careers->jobSpecification2    =  $request->jobSpecification2;
        $careers->jobSpecification3    =  $request->jobSpecification3;
        $careers->jobSpecification4    =  $request->jobSpecification4;
        $careers->jobSpecification5    =  $request->jobSpecification5;
        $careers->jobSpecification6    =  $request->jobSpecification6;
        $careers->jobSpecification7    =  $request->jobSpecification7;
        $careers->jobSpecification8    =  $request->jobSpecification8;
        $careers->jobSpecification9    =  $request->jobSpecification9;
        $careers->jobSpecification10   =  $request->jobSpecification10;

         $myUpdate=$careers->update();
         if($myUpdate){
             return redirect()->route('indexcareers')->withMessage('Data updated Successfully');
         }

         else{
             return back()->withMessage('Oops...Something went wrong. Data not updated');
         }
    }


    public function destroy($id)
    {
        $careers       = career::findOrFail($id);
        $careersDelete = $career->delete();
        if($careersDelete){
            return redirect()->route('indexcareers')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
