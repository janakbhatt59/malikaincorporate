<?php

namespace App\Http\Controllers\admin\company;

use App\model\admin\leftContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class lcontentController extends Controller
{
    public $viewPath='adminPage.company.lContent';
    public function index()
    {
       $page['page_title']       = config('siteContent.companyName') . 'view blog list';
       $page['page_description'] = 'create blog of malika incorporate';
       $content = leftContent::orderby('created_at','DESC')->paginate(7);

       return view($this->viewPath.'.indexLcontent',compact(['page','content']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $page['page_title']       = config('siteContent.companyName') . 'create Content';
          $page['page_description'] = 'create Content of malika incorporate';

          return view($this->viewPath.'.lcontentCreate',compact(['page']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'heading'        =>'required',
            'image'          =>'required',
            'desc'    =>'required',
        ]);

        $content = new leftContent();
        $content->heading      =  $request->heading;
        $content->description  =  $request->desc;

        if($request->hasFile('image')){
            $image=$request->file('image');
            $input=time().'.'.$image->getClientOriginalExtension();
            $destinationPath='assests/img/lcontent';
            $content->image    = $destinationPath .'/'. $input;
            $image->move($destinationPath, $input);
            }

         $mySave=$content->save();
         if($mySave){
             return redirect()->route('indexlContent')->withMessage('Data Saved Successfully');
         }

         else{
             return back()->withMessage('Oops...Something went wrong. Data not saved');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $page['page_title']       = config('siteContent.companyName') . 'edit Content';
         $page['page_description'] = 'Edit content of malika incorporate';
        $content = leftContent::findOrFail($id);
        if(!$content){
            return back()->withMessage('IdNotFound');
        }
        return view($this->viewPath.'.editContent',compact(['content','page']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'heading'       =>'required',
            'image'         =>'required',
            'desc'          =>'required',
        ]);

        $content = leftContent::findOrFail($id);
        $content->heading       = $request->heading;
        $content->description   = $request->desc;

        if($request->hasFile('image')){
            $image=$request->file('image');
            $input=time().'.'.$image->getClientOriginalExtension();
            $destinationPath='assests/img/lcontent';
            $content->image    = $destinationPath .'/'. $input;
            $image->move($destinationPath, $input);
            }

         $myUpdate=$content->update();
         if($myUpdate){
             return redirect()->route('indexlContent')->withMessage('Data updated Successfully');
         }

         else{
             return back()->withMessage('Oops...Something went wrong. Data not updated');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $content       = leftContent::findOrFail($id);
        $contentDelete = $content->delete();
        if($contentDelete){
            return redirect()->route('indexlContent')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
