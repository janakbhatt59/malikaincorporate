<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products\hospital;
use Intervention\Image\Facades\Image;

class hospitalController extends Controller
{
    public $viewPath = 'adminPage.company.Products.hospital';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $hospital = hospital::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexhospital',compact(['page','hospital']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createhospital',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);


        $hospital = new hospital();

        $hospital->heading=$request->heading;
        $hospital->description=$request->description;
        $hospital->bullet1=$request->bullet1;
        $hospital->bullet2=$request->bullet2;
        $hospital->bullet3=$request->bullet3;
       


        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Hospital/' . $filename );
            $hospital->image =  'assests/img/Hospital/' . $filename;
        }

        $mySave=$hospital->save();

        if($mySave){
            return redirect()->route('viewhospital')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit hospital';
        $page['page_description'] = 'Edit hospital of malika incorporate';

        $hospital = hospital::findOrFail($id);
        if(!$hospital){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.edithospital',compact(['hospital','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $hospital = hospital::findOrFail($id);

        $hospital->heading=$request->heading;
        $hospital->description=$request->description;
        $hospital->bullet1=$request->bullet1;
        $hospital->bullet2=$request->bullet2;
        $hospital->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Hospital/' . $filename );
            $hospital->image =  'assests/img/Hospital/' . $filename;
        }

        $myUpdate=$hospital->update();

        if($myUpdate){
            return redirect()->route('viewhospital')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $hospital      = hospital::findOrFail($id);
        $deletehospital = $hospital->delete();
        if($deletehospital ){
            return redirect()->route('viewhospital')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
