<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products\finger;
use Intervention\Image\Facades\Image;

class fingerController extends Controller
{
    public $viewPath = 'adminPage.company.Products.finger';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $finger = finger::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexfinger',compact(['page','finger']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createfinger',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $finger = new finger();

        $finger->heading=$request->heading;
        $finger->description=$request->description;
        $finger->bullet1=$request->bullet1;
        $finger->bullet2=$request->bullet2;
        $finger->bullet3=$request->bullet3;
       

        
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Finger/' . $filename );
            $finger->image =  'assests/img/Finger/' . $filename;
        }

        $mySave=$finger->save();

        if($mySave){
            return redirect()->route('viewfinger')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit finger';
        $page['page_description'] = 'Edit finger of malika incorporate';

        $finger = finger::findOrFail($id);
        if(!$finger){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editfinger',compact(['finger','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $finger = finger::findOrFail($id);

        $finger->heading=$request->heading;
        $finger->description=$request->description;
        $finger->bullet1=$request->bullet1;
        $finger->bullet2=$request->bullet2;
        $finger->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Finger/' . $filename );
            $finger->image =  'assests/img/Finger/' . $filename;
        }

        $myUpdate=$finger->update();

        if($myUpdate){
            return redirect()->route('viewfinger')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $finger      = finger::findOrFail($id);
        $deletefinger = $finger->delete();
        if($deletefinger ){
            return redirect()->route('viewcardpinter')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
