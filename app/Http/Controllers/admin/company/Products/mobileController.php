<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Products\mobile;

class mobileController extends Controller
{
    public $viewPath = 'adminPage.company.Products.Mobile';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $mobile = mobile::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexMobile',compact(['page','mobile']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createMobile',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'
        ]);

        $mobile = new mobile();

        $mobile->heading=$request->heading;
        $mobile->description=$request->description;
        $mobile->bullet1=$request->bullet1;
        $mobile->bullet2=$request->bullet2;
        $mobile->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Mobile/' . $filename );
            $mobile->image =  'assests/img/Mobile/' . $filename;
        }

        $mySave=$mobile->save();

        if($mySave){
            return redirect()->route('viewmobile')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit mobile';
        $page['page_description'] = 'Edit cardprinter of malika incorporate';

        $mobile = mobile::findOrFail($id);
        if(!$mobile){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editMobile',compact(['mobile','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $mobile = mobile::findOrFail($id);

        $mobile->heading=$request->heading;
        $mobile->description=$request->description;
        $mobile->bullet1=$request->bullet1;
        $mobile->bullet2=$request->bullet2;
        $mobile->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Mobile/' . $filename );
            $mobile->image =  'assests/img/Mobile/' . $filename;
        }

        $myUpdate=$mobile->update();

        if($mobile){
            return redirect()->route('viewmobile')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $mobile       = mobile::findOrFail($id);
        $mobileDelete = $mobile->delete();
        if($mobileDelete ){
            return redirect()->route('viewmobile')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
