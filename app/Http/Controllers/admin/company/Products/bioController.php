<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products\bio;
use Intervention\Image\Facades\Image;


class bioController extends Controller
{
    public $viewPath = 'adminPage.company.Products.bio';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $bio = bio::orderby('created_at','DESC')->paginate(20);
        return view($this->viewPath.'.indexbio',compact(['page','bio']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createbio',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);


        $bio = new bio();

        $bio->heading=$request->heading;
        $bio->description=$request->description;
        $bio->bullet1=$request->bullet1;
        $bio->bullet2=$request->bullet2;
        $bio->bullet3=$request->bullet3;
       


        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Bio/' . $filename );
            $bio->image =  'assests/img/Bio/' . $filename;
        }

        $mySave=$bio->save();

        if($mySave){
            return redirect()->route('viewbio')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit bio';
        $page['page_description'] = 'Edit bio of malika incorporate';

        $bio = bio::findOrFail($id);
        if(!$bio){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editbio',compact(['bio','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $bio = bio::findOrFail($id);

        $bio->heading=$request->heading;
        $bio->description=$request->description;
        $bio->bullet1=$request->bullet1;
        $bio->bullet2=$request->bullet2;
        $bio->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Bio/' . $filename );
            $bio->image =  'assests/img/Bio/' . $filename;
        }

        $myUpdate=$bio->update();

        if($myUpdate){
            return redirect()->route('viewbio')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $bio      = bio::findOrFail($id);
        $deletebio = $bio->delete();
        if($deletebio ){
            return redirect()->route('viewbio')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
