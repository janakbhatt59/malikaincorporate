<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products\industrialprinter;
use Intervention\Image\Facades\Image;


class industrialLevelPrinterController extends Controller
{
    public $viewPath = 'adminPage.company.Products.IndustrialLevelPrinter';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $industrialprinter = industrialprinter::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexIndustrialPrinter',compact(['page','industrialprinter']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createIndustrialPrinter',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        
        $industrialprinter = new industrialprinter();

        $industrialprinter->heading=$request->heading;
        $industrialprinter->description=$request->description;
        $industrialprinter->bullet1=$request->bullet1;
        $industrialprinter->bullet2=$request->bullet2;
        $industrialprinter->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/IndustrialPrinter/' . $filename );
            $industrialprinter->image =  'assests/img/IndustrialPrinter/' . $filename;
        }

        $mySave=$industrialprinter->save();

        if($mySave){
            return redirect()->route('viewindustrialLevelPrinter')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit cardprinter';
        $page['page_description'] = 'Edit cardprinter of malika incorporate';

        $industrialprinter = industrialprinter::findOrFail($id);
        if(!$industrialprinter){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editIndustrialPrinter',compact(['industrialprinter','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'
        ]);


        $industrialprinter = industrialprinter::findOrFail($id);

        $industrialprinter->heading=$request->heading;
        $industrialprinter->description=$request->description;
        $industrialprinter->bullet1=$request->bullet1;
        $industrialprinter->bullet2=$request->bullet2;
        $industrialprinter->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/IndustrialPrinter/' . $filename );
            $industrialprinter->image =  'assests/img/IndustrialPrinter/' . $filename;
        }

        $myUpdate=$industrialprinter->update();

        if($myUpdate){
            return redirect()->route('viewindustrialLevelPrinter')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $industrialprinter       = industrialprinter::findOrFail($id);
        $industrialprinterDelete = $industrialprinter->delete();
        if($industrialprinterDelete ){
            return redirect()->route('viewindustrialLevelPrinter')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
