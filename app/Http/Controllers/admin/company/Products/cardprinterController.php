<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products\cardprinter;
use Intervention\Image\Facades\Image;

class cardprinterController extends Controller
{
    public $viewPath = 'adminPage.company.Products.CardPrinter';

    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $cardprinter = cardprinter::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexCardPrinter',compact(['page','cardprinter']));
    }


    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createCardPrinter',compact(['page']));
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $cardprinter = new cardprinter();

        $cardprinter->heading=$request->heading;
        $cardprinter->description=$request->description;
        $cardprinter->bullet1=$request->bullet1;
        $cardprinter->bullet2=$request->bullet2;
        $cardprinter->bullet3=$request->bullet3;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();

            Image::make( $image )->save('assests/img/CardPrinter/' . $filename );
            $cardprinter->image =  'assests/img/CardPrinter/' . $filename;
        }

        $mySave=$cardprinter->save();

        if($mySave){

            return redirect()->route('viewcardpinter')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }



    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit cardprinter';
        $page['page_description'] = 'Edit cardprinter of malika incorporate';

        $cardprinter = cardprinter::findOrFail($id);
        if(!$cardprinter){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editcardprinter',compact(['cardprinter','page']));
    }


    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $cardprinter = cardprinter::findOrFail($id);

        $cardprinter->heading=$request->heading;
        $cardprinter->description=$request->description;
        $cardprinter->bullet1=$request->bullet1;
        $cardprinter->bullet2=$request->bullet2;
        $cardprinter->bullet3=$request->bullet3;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/CardPrinter/' . $filename );
            $cardprinter->image =  'assests/img/CardPrinter/' . $filename;
        }

        $myUpdate=$cardprinter->update();

        if($myUpdate){
            return redirect()->route('viewcardpinter')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }


    public function destroy($id)
    {
        $cardprinter       = cardprinter::findOrFail($id);
        $cardprinterDelete = $cardprinter->delete();
        if($cardprinterDelete ){
            return redirect()->route('viewcardpinter')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
