<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use  App\Products\mobtickprinter;

class mobileTicketPrinterController extends Controller
{
    public $viewPath = 'adminPage.company.Products.Mob_Ticket';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $mobtickprinter = mobtickprinter::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexMobTicket',compact(['page','mobtickprinter']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createMobTicket',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'
        ]);

        
        $mobtickprinter = new mobtickprinter();

        $mobtickprinter->heading=$request->heading;
        $mobtickprinter->description=$request->description;
        $mobtickprinter->bullet1=$request->bullet1;
        $mobtickprinter->bullet2=$request->bullet2;
        $mobtickprinter->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Mob_Ticket/' . $filename );
            $mobtickprinter->image =  'assests/img/Mob_Ticket/' . $filename;
        }

        $mySave=$mobtickprinter->save();

        if($mySave){
            return redirect()->route('viewmobtickprinter')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit cardprinter';
        $page['page_description'] = 'Edit cardprinter of malika incorporate';

        $mobtickprinter = mobtickprinter::findOrFail($id);
        if(!$mobtickprinter){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editMobTicket',compact(['mobtickprinter','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'
        ]);


        $mobtickprinter = mobtickprinter::findOrFail($id);

        $mobtickprinter->heading=$request->heading;
        $mobtickprinter->description=$request->description;
        $mobtickprinter->bullet1=$request->bullet1;
        $mobtickprinter->bullet2=$request->bullet2;
        $mobtickprinter->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/Mob_Ticket/' . $filename );
            $mobtickprinter->image =  'assests/img/Mob_Ticket/' . $filename;
        }

        $myUpdate=$mobtickprinter->update();

        if($myUpdate){
            return redirect()->route('viewmobtickprinter')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $mobtickprinter       = mobtickprinter::findOrFail($id);
        $mobtickprinterDelete = $mobtickprinter->delete();
        if($mobtickprinterDelete ){
            return redirect()->route('viewmobtickprinter')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
