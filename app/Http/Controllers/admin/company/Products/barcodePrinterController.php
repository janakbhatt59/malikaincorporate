<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Products\barcode;

class barcodePrinterController extends Controller
{
    public $viewPath = 'adminPage.company.Products.BarcodePrinter';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $barcode = barcode::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexBarcodePrinter',compact(['page','barcode']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createBarcodePrinter',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'
        ]);

        $barcode = new barcode();

        $barcode->heading=$request->heading;
        $barcode->description=$request->description;
        $barcode->bullet1=$request->bullet1;
        $barcode->bullet2=$request->bullet2;
        $barcode->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/BarcodePrinter/' . $filename );
            $barcode->image =  'assests/img/BarcodePrinter/' . $filename;
        }

        $mySave=$barcode->save();

        if($mySave){
            return redirect()->route('viewbarcodeprinter')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit product';
        $page['page_description'] = 'Edit product of malika incorporate';

        $barcode = barcode::findOrFail($id);
        if(!$barcode){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editBarcodePrinter',compact(['barcode','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $barcode = barcode::findOrFail($id);

        $barcode->heading=$request->heading;
        $barcode->description=$request->description;
        $barcode->bullet1=$request->bullet1;
        $barcode->bullet2=$request->bullet2;
        $barcode->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/BarcodePrinter/' . $filename );
            $barcode->image =  'assests/img/BarcodePrinter/' . $filename;
        }

        $myUpdate=$barcode->update();

        if($myUpdate){
            return redirect()->route('viewbarcodeprinter')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $barcode       = barcode::findOrFail($id);
        $barcodeDelete = $barcode->delete();
        if($barcodeDelete ){
            return redirect()->route('viewbarcodeprinter')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
