<?php

namespace App\Http\Controllers\admin\company\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Products\barcodeScanner;

class barcodeScannerController extends Controller
{
    public $viewPath = 'adminPage.company.Products.BarcodeScanner';
    
    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Products';
        $page['page_description'] = 'add produtcts of malika incorporate';
        $barcodeScanner = barcodeScanner::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexBarcodeScanner',compact(['page','barcodeScanner']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create Products';
        $page['page_description'] = 'add produtctsof malika incorporate';
         return view($this->viewPath.'.createBarcodeScanner',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $barcodeScanner = new barcodeScanner();

        $barcodeScanner->heading=$request->heading;
        $barcodeScanner->description=$request->description;
        $barcodeScanner->bullet1=$request->bullet1;
        $barcodeScanner->bullet2=$request->bullet2;
        $barcodeScanner->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/BarcodeScanner/' . $filename );
            $barcodeScanner->image =  'assests/img/BarcodeScanner/' . $filename;
        }

        $mySave=$barcodeScanner->save();

        if($mySave){
            return redirect()->route('viewbarcodescanner')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit barcodescanner';
        $page['page_description'] = 'Edit cardprinter of malika incorporate';

        $barcodeScanner = barcodeScanner::findOrFail($id);
        if(!$barcodeScanner){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editbarcodescanner',compact(['barcodeScanner','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',
            'image'=>'required',
            'description'=>'required'

        ]);

        $barcodeScanner = barcodeScanner::findOrFail($id);

        $barcodeScanner->heading=$request->heading;
        $barcodeScanner->description=$request->description;
        $barcodeScanner->bullet1=$request->bullet1;
        $barcodeScanner->bullet2=$request->bullet2;
        $barcodeScanner->bullet3=$request->bullet3;
       
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename =time(). '.' . $image->getClientOriginalExtension();
            Image::make( $image )->save('assests/img/BarcodeScanner/' . $filename );
            $barcodeScanner->image =  'assests/img/BarcodeScanner/' . $filename;
        }

        $myUpdate=$barcodeScanner->update();

        if($myUpdate){
            return redirect()->route('viewbarcodescanner')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $barcodeScanner       = barcodeScanner::findOrFail($id);
        $barcodeScannerDelete = $barcodeScanner->delete();
        if($barcodeScannerDelete ){
            return redirect()->route('viewbarcodescanner')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
