<?php

namespace App\Http\Controllers\admin\company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\admin\sector;

class sectorsController extends Controller
{
    public $viewPath = 'adminPage.company.AboutUs.Sectors';

    public function index()
    {
        $page['page_title']       = config('siteContent.companyName') . 'view Sectors we Serve';
        $page['page_description'] = 'add content for Sectors we Serve of malika incorporate';
        $sector = sector::orderby('created_at','DESC')->paginate(7);
        return view($this->viewPath.'.indexSector',compact(['page','sector']));
    }

    
    public function create()
    {
        $page['page_title']       = config('siteContent.companyName') . 'create content for Sectors we Serve';
        $page['page_description'] = 'add content for Sectors we Serve of malika incorporate';
         return view($this->viewPath.'.createSector',compact(['page']));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'heading'=>'required',
        ]);

        $sector = new sector();

        $sector->heading=$request->heading;
        $sector->description=$request->description;
        $sector->product=$request->product;

        $mySave=$sector->save();

        if($mySave){
            return redirect()->route('viewsector')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

    
  
    public function edit($id)
    {
        $page['page_title']       = config('siteContent.companyName') . 'edit Sectors we Serve';
        $page['page_description'] = 'Edit content of Sectors we Serve of malika incorporate';

        $sector = sector::findOrFail($id);
        if(!$sector){
            return back()->withMessage('ID Not Found');
        }
        return view($this->viewPath.'.editSector',compact(['sector','page']));
    }

  
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'heading'=>'required',

        ]);

        $sector = sector::findOrFail($id);

        $sector->heading=$request->heading;
        $sector->description=$request->description;
        $sector->product=$request->product;


        $myUpdate=$sector->update();

        if($myUpdate){
            return redirect()->route('viewsector')->withMessage('Data Saved Successfully');
        }

        else{
            return back()->withMessage('Oops...Something went wrong. Data not saved');
        }
    }

 
    public function destroy($id)
    {
        $sector       = sector::findOrFail($id);
        $sectorDelete = $sector->delete();
        if($sectorDelete ){
            return redirect()->route('viewsector')->withMessage('Successfully Deleted');
        }
        else{
            return back()->withMessage('Oops...something went wrong. Delete Unsuccessful');
        }
    }
}
