<?php

namespace App\Http\Controllers\admin;

use App\admin\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class contactUsController extends Controller
{
  public function contactUs()
  {
      return view('page.contact');
  }
  public function contactUsPost(Request $request)
  {
      $this->validate($request, [
       
      ]);
   
      $data =array(
           'name' => $request->name,
           'email' => $request->email,
           'subject' => $request->subject,
           'bodymessage' => $request->message
      );
      Mail::send('emails.contact', $data, function($message) use ($data){

       $message->from($data['email']);
       $message->to('jackobian59@gmail.com');
       $message->subject($data['subject']);
      });
      return back()->with('success','Thanks for contacting us!');
  }
}
