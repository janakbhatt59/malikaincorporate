<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class adminHomeController extends Controller
{
	public $viewPath = 'adminPage';
    public function dashboard(){
		$page['page_title'] = config('siteContent.companyName');
		$page['page_description'] = config('siteContent.companyName') . 'dashboard';

		return view($this->viewPath . '.adminIndex', compact(['page']));
    }
}
