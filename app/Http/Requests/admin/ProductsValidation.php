<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:150',
            'categories_id'       =>'required',
            'price'               => 'required',
            'featured_img'        => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'discount'             => 'required',
            'description'          => 'required|min:10',
        ];
    }
}
